# 1.列举 str、list、dict、set 中的常用方法（每种至少5个），并标注是否有返回值。
# str 常用方法 upper/lower(有返回值)  、strip(有返回值) 、isdigit(有返回值) 、
# replace(有返回值) 、startswith/endswith(有返回值)
# join(有返回值) 、encode(有返回值) 、split(有返回值) 、rjust(有返回值).
#list 常用方法有 append(无) extend(无) insert(无) remove(无) pop(有)
# del(无) clear(无) reverse(无) sort(无)
# dict常用使用方法有 update无 get（有返回值） keys（有返回值） values（有返回值） items（有返回值） pop（有返回值）
# set add无 discard无 update无 intersection有 union有
# difference有  symmertric_difference有
# 2.列举你了解的常见内置函数 【面试题】。
# 输入输出：print input  强制转换：dict list tuple int str bool set
# 数学相关：abs float max min sum divmod 进制转换：bin oct int hex
# 其他：len open range id type
# 3.看代码分析结果
# Alex的女朋友***和大家都是好朋友
# def func(arg):
#     return arg.replace('苍老师', '***')
#
#
# def run():
#     msg = "Alex的女朋友苍老师和大家都是好朋友"
#     result = func(msg)
#     print(result)
#
#
# run()
# 4.看代码分析结果
# Alex的女朋友***和大家都是好朋友 None
# def func(arg):
#     return arg.replace('苍老师', '***')
#
#
# def run():
#     msg = "Alex的女朋友苍老师和大家都是好朋友"
#     result = func(msg)
#     print(result)
#
# data = run()
# print(data)
# 5.看代码分析结果
# None ['绕不死你']
# DATA_LIST = []
#
#
# def func(arg):
#     return DATA_LIST.insert(0, arg)
#
#
# data = func('绕不死你')
# print(data)
# print(DATA_LIST)
# 6.看代码分析结果
# 你好呀 \n 好你妹呀\n  你好呀 \n 好你妹呀\n  你好呀 \n 好你妹呀
# def func():
#     print('你好呀')
#     return '好你妹呀'
#
#
# func_list = [func, func, func]
#
# for item in func_list:
#     val = item()
#     print(val)
# 7.看代码分析结果
# 你好呀 \n 好你妹呀\n  你好呀 \n 好你妹呀\n  你好呀 \n 好你妹呀
# def func():
#     print('你好呀')
#     return '好你妹呀'
#
#
# func_list = [func, func, func]
#
# for i in range(len(func_list)):
#     val = func_list[i]()
#     print(val)
# 8.看代码写结果
# 你好不好 \n 好你妹呀\n 你好不好 \n 好你妹呀\n 你好不好 \n 好你妹呀
# tips = "啦啦啦啦"
#
#
# def func():
#     print(tips)
#     return '好你妹呀'
#
#
# func_list = [func, func, func]
#
# tips = '你好不好'
#
# for i in range(len(func_list)):
#     val = func_list[i]()
#     print(val)
# 9.看代码写结果
# 烧饼豆饼
# def func():
#     return '烧饼'
#
#
# def bar():
#     return '豆饼'
#
#
# def base(a1, a2):
#     return a1() + a2()
#
#
# result = base(func, bar)
# print(result)
# 10.看代码写结果
# 烧饼豆饼
# def func():
#     return '烧饼'
#
#
# def bar():
#     return '豆饼'
#
#
# def base(a1, a2):
#     return a1 + a2
#
#
# result = base(func(), bar())
# print(result)
# 11.看代码写结果
# 100    66    小
# v1 = lambda :100
# print(v1())
#
# v2 = lambda vals: max(vals) + min(vals)
# print(v2([11,22,33,44,55]))
#
# v3 = lambda vals: '大' if max(vals)>5 else '小'
# print(v3([1,2,3,4]))
# 12.看代码写结果
# 20   110  110
# def func():
#     num = 10
#     v4 = [lambda :num+10,lambda :num+100,lambda :num+100,]
#     for item in v4:
#         print(item())
# func()
# 13.看代码写结果
# 0/n2/n3/n4/n5/n6/n7/n8/n9/n9
# for item in range(10):
#     print(item)
#
# print(item)
# 14.看代码写结果
# 9
# def func():
#     for item in range(10):
#         pass
#     print(item)
# func()
# 15.看代码写结果
# 9
# item = '老男孩'
# def func():
#     item = 'alex'
#     def inner():
#         print(item)
#     for item in range(10):
#         pass
#     inner()
# func()
# 16.看代码写结果
# 109 109
# def func():
#     for num in range(10):
#         pass
#     v4 = [lambda :num+10,lambda :num+100,lambda :num+100,]
#     result1 = v4[1]()
#     result2 = v4[2]()
#     print(result1,result2)
# func()
# 17.写代码实现
# 二进制转换成十进制：v = '0b1111011'
# v = '0b1111011'
# print(int(v,base=2))
# 十进制转换成二进制：v = 18
# v = 18
# print(bin(v))
# 八进制转换成十进制：v = '011'
# v = '011'
# print(int(v,base=8))
# 十进制转换成八进制：v = 30
# v = 30
# print(oct(v))
# 十六进制转换成十进制：v = '0x12'
# v = '0x12'
# print(int(v,base=16))
# 十进制转换成十六进制：v = 87
# v = 87
# print(hex(v))
# 18.请编写一个函数实现将IP地址转换成一个整数。
# 167971084
# ip = '10.3.9.12'
# val = ip.split('.')
# v=[]
# for i in val:
#     v.append(bin(int(i))[2:].rjust(8,'0'))
# v1 = ''.join(v)
# print(v1)
# print(int(v1,base=2))
