from bin import starts
def encrypt_md5(arg):
    """
    md5加密
    :param arg:
    :return:
    """
    m = starts.hashlib.md5()
    m.update(arg.encode('utf-8'))
    return m.hexdigest()