from lib import user_info,md5
def login():
    """
    用户登录
    :return:
    """
    print('用户登录')
    while True:
        user = input('请输入用户名(N返回上一级)：')
        if user.upper() == 'N':
            return
        pwd = input('请输入密码：')
        if user not in user_info.USER_DICT:
            print('用户名不存在')
            continue

        encrypt_password = user_info.USER_DICT.get(user)
        if md5.encrypt_md5(pwd) != encrypt_password:
            print('密码错误')
            continue

        print('登录成功')
        global CURRENT_USER
        user_info.CURRENT_USER = user
        return