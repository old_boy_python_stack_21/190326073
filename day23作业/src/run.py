from src import login,register,news_sys
from lib import news
def run():
    """
    主函数
    :return:
    """
    print('=================== 系统首页 ===================')
    func_dict = {'1': register.register, '2': login.login, '3': news_sys.article_list}
    while True:
        print('1.注册；2.登录；3.文章列表')
        choice = input('请选择序号：')
        if choice.upper() == 'N':
            return
        func = func_dict.get(choice)
        if not func:
            print('序号选择错误')
            continue
        func()