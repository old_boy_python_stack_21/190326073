from lib import user_info,md5
def register():
    """
    用户注册
    :return:
    """
    print('用户注册')
    while True:
        user = input('请输入用户名(N返回上一级)：')
        if user.upper() == 'N':
            return
        pwd = input('请输入密码：')
        if user in user_info.USER_DICT:
            print('用户已经存在，请重新输入。')
            continue
        user_info.USER_DICT[user] = md5.encrypt_md5(pwd)
        print('%s 注册成功' % user)
        print(user_info.USER_DICT)