# 1、程序从flag a执行到falg b的时间大致是多少秒？
# import threading
# import time
# def _wait():
# 	time.sleep(60)
# # flag a
# t = threading.Thread(target=_wait,daemon = False)
# t.start()
# # flag b
# 由于守护线程daemon =False  ，守护线程未开启
# 2.程序从flag a执行到falg b的时间大致是多少秒？
# import threading
# import time
# def _wait():
#     time.sleep(60)
# # flag a
# t = threading.Thread(target=_wait,daemon = True)
# t.start()
# # flag b
# 大概0秒，因为线程开启了守护线程，守护线程会随着其他非守护线程的结束而结束，所以是先运行
# flag a 之后开启线程，之后运行flag b，随着代码的结束，守护线程也随之结束。
# 3、程序从flag a执行到falg b的时间大致是多少秒？
# import threading
# import time
# def _wait():
#     print(1)
#     time.sleep(1)
# # flag a
# print(2)
# t = threading.Thread(target=_wait,daemon = True)
# print(3)
# t.start()
# t.join()
# print(4)
# flag b
# 60秒，因为线程开启了守护线程，守护线程会随着其他非守护线程的结束而结束，
# 所以是先运行flag a ，之后开启线程，但是线程开启了同步阻塞，直到线程运行完毕
# 之后运行flag b，随着代码的结束，守护线程也随之结束。
# 读程序，请确认执行到最后number是否一定为0
# import threading
# loop = int(1E7)
# def _add(loop:int = 1):
#     global number
#     for _ in range(loop):
#         number += 1
# def _sub(loop:int = 1):
#     global number
#     for _ in range(loop):
#         number -= 1
# number = 0
# ta = threading.Thread(target=_add,args=(loop,))
# ts = threading.Thread(target=_sub,args=(loop,))
# ta.start()
# # ta.join()
# ts.start()
# # ta.join()
# # ts.join()
# print(number)
# 不一定为0，因为开启的这种多线程都使用全局变量并且修改全局变量的时候，很有可能会
# 遇到时间片轮转机制，导致某部分代码未执行完毕就切换到了其他线程，产生了混乱。
# 5、读程序，请确认执行到最后number是否一定为0
# import threading
# loop = int(1E7)
# def _add(loop:int = 1):
# 	global number
# 	for _ in range(loop):
# 		number += 1
# def _sub(loop:int = 1):
# 	global number
# 	for _ in range(loop):
# 		number -= 1
# number = 0
# ta = threading.Thread(target=_add,args=(loop,))
# ts = threading.Thread(target=_sub,args=(loop,))
# ta.start()
# ta.join()
# ts.start()
# ts.join()
# print(number)
# 最终number肯定是0，因为这个是先执行某个线程执行完毕之后再执行另一个线程，中间不会
# 切换到其他线程，所以不会发生数据混乱使用了join同步阻塞。
# 7、读程序，请确认执行到最后number的长度是否一定为1
# import threading,time
# loop = int(10)
# def _add(loop:int = 1):
# 	global numbers
# 	for i in range(loop):
# 		numbers.append(0)
# def _sub(loop:int = 1):
# 	global numbers
# 	while not numbers:
# 		time.sleep(1E-8)
# 	numbers.pop()
# numbers = [0]
# ta = threading.Thread(target=_add,args=(loop,))
# ts = threading.Thread(target=_sub,args=(loop,))
# ta.start()
# ta.join()
# ts.start()
# ts.join()
# 不为1，因为执行——sub线程的时候只执行了一次pop
# 8、读程序，请确认执行到最后number的长度是否一定为1
# import threading,time
# loop = int(1E7)
# def _add(loop:int = 1):
# 	global numbers
# 	for _ in range(loop):
# 		numbers.append(0)
# def _sub(loop:int = 1):
# 	global numbers
# 	while not numbers:
# 		time.sleep(1E-8)
# 	numbers.pop()
# numbers = [0]
# ta = threading.Thread(target=_add,args=(loop,))
# ts = threading.Thread(target=_sub,args=(loop,))
# ta.start()
# ts.start()
# ta.join()
# ts.join()
# 不为1，因为只删除了一次0.
# 协程内容相关
# 1.什么是协程，常用的协程模块有哪些？
# 协程是指线程运行时遇到io操作时会产生cpu的空闲，所以就引出了用协程来切换线程的运行
# 来提高cpu的运行效率，减少io阻塞的低效。
# 常用的协程模块有原生的python：yield   asyncio  c语言完成的python模块：greenlet  gevent
# 2.协程中的join是用来做什么用的？它是如何发挥作用的？
# join是用来同步阻塞的，执行时当创建的协程任务遇到io阻塞，切换到另一个任务开始异步执行，
# 当join的协程执行的时间到，全部结束。
# 3.使用协程实现并发的tcp server端
'''greenlet'''
'''
import socket
from greenlet import greenlet
import asyncio
sk = socket.socket()
sk.bind(('127.0.0.1',6666))
sk.listen()
def rec():
    conn,addr = sk.accept()
    msg = conn.recv(1024).decode('utf-8')
    print(msg)
def sed():
    conn, addr = sk.accept()
    conn.send(b'hello')
g1 = greenlet(rec)
g2 = greenlet(sed)
while True:
    try:
        g1.switch()
        g2.switch()
    except EOFError:
        break
'''
'''gevent'''
'''
import socket
import gevent
sk = socket.socket()
sk.bind(('127.0.0.1',6666))
sk.listen()
def rec():
    conn,addr = sk.accept()
    msg = conn.recv(1024).decode('utf-8')
    print(msg)
def sed():
    conn, addr = sk.accept()
    conn.send(b'hello')
g1 = gevent.spawn(rec)
g2 = gevent.spawn(sed)
g1.join()
g2.join()
'''
'''asyncio'''
'''
import socket
import asyncio
sk = socket.socket()
sk.bind(('127.0.0.1',6666))
sk.listen()
def rec():
    conn,addr = sk.accept()
    msg = conn.recv(1024).decode('utf-8')
    print(msg)
    conn.send(b'hello')
loop = asyncio.get_event_loop()
wait_obj = asyncio.wait(rec())
loop.run_until_complete(wait_obj)
'''
# 4、在一个列表中有多个url，请使用协程访问所有url，将对应的网页内容写入文件保存
# import requests
# import asyncio
# async def get():
#     v = ['https://blog.csdn.net/chengqiuming/article/details/80573288',
#          'http://www.sohu.com/','http://www.yyets.com/',
#          'http://www.baidu.com/']
#     m = []
#     for url in v:
#         task =asyncio.ensure_future(write_ret(url))
#         m.append(task)
#     for ret in asyncio.as_completed(m):
#         res = await ret
#         print(res)
# async def write_ret(url):
#     xet = requests.get(url)
#     xet = xet.text
#     with open('webinfo',mode='a',encoding='utf-8') as f:
#         f.write(xet)
#     return ('写入%s成功'%url)
#
# loop = asyncio.get_event_loop()
# loop.run_until_complete(get())
# 综合
# 1.进程和线程的区别
# 进程：是指运行中的程序，是计算机中的最小资源分配单位，在操作系统中有唯一的pid
# 进程的创建、切换和销毁开销都比较大。线程：线程是进程中的一部分，每一个进程中至少有
# 一个线程，线程是计算机中能被cpu执行的最小调度单位，线程的创建、切换及销毁相对较小。
# 2.进程池和线程池的优势和特点？
# 进程池:预先的开启固定个数的进程数，当任务来临的时候，直接交给已经开好的进程，让这个进程
# 去执行就可以了，节省了进程，进程的开启关闭切换都需要时间，并且减轻了操作系统跳读的负担
# 特点是：开销大，进程池的任务个数限制了我们程序的并发数。
# 线程池：预先开启固定个数的线程，任务来临之后在线程池中循环执行，但线程不能并行，只能
# 并发00
# 3.线程和协程的异同?
# 相同点：都不能利用多核，不同：线程切换需要操作系统，开销大，操作系统不可控，会增加操作系统
# 的负担，协程的切换时需要python代码，开销小，用户操作可控，不会增加操作系统的压力
# 4.请简述一下互斥锁和递归锁的异同？
# 互斥锁：互斥锁在一个线程中多次acquire会死锁，发生死锁时可以把多把互斥锁编程一把递归锁。
# 递归锁：可以快速解决问题，但是效率较差
# 5、请列举一个python中数据安全的数据类型？
# Queue  socket pickle Lock
# 6.Python中如何使用线程池和进程池
# 线程池：
'''
from concurrent.futures import ThreadPoolExecutor
def func(i):
    print('start', os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
    return '%s * %s'%(i,os.getpid())
tp = ThreadPoolExecutor(20)
ret_l = []
for i in range(10):
    ret = tp.submit(func,i)
    ret_l.append(ret)
tp.shutdown()
print('main')
for ret in ret_l:
    print('------>',ret.result())'''
# 进程池
''''
import os
import time
import random
from concurrent.futures import ProcessPoolExecutor
def func():
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())
if __name__ == '__main__':
    p = ProcessPoolExecutor(5)
    for i in range(10):
        p.submit(func)
    p.shutdown()
    print('main:',os.getpid())
'''
# 7.简述 进程、线程、协程的区别 以及应用场景？
# 进程：运行中的程序。线程：线程是进程的一部分，每个进程中至少有一个线程，不能利用多核
# 协程：负责任务遇到io操作的时候协调切换任务，让cpu高效的利用。
# 8.什么是并行，什么是并发？
# 并行是程序同时运行分别不同cpu运行，互补干扰。并发：程序轮流切换运行交替在一个cpu执行，
# 当一个程序运行完或时间片时间到了
# 再切换到另一个程序继续运行。