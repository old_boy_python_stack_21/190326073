# 1.大文件阅读线程
import time
from threading import Lock
from concurrent.futures import ThreadPoolExecutor
# def readline(num,lock):
#     with lock:
#         v = []
#         for i in range(num):
#             a = f.readline().strip()
#             if not a:break
#             v.append(a)
#         time.sleep(1)
#         return v
# def prid(v):
#     for i in v.result():
#         print(i)
# lock = Lock()
# f = open('webinfo',mode='r',encoding='utf-8')
# tp = ThreadPoolExecutor(10)
# ret_l = []
# for i in range(20):
#     ret = tp.submit(readline,10,lock)
#     ret.add_done_callback(prid)
# tp.shutdown()
# for ret in ret_l:
#     for i in ret.result():
#         print(i)
# 2.socket udp发送格式回复时间
# import socket,time
# import struct
# from datetime import datetime
# sk = socket.socket()
# sk.bind(('127.0.0.1',6666))
# sk.listen()
# conn,addr = sk.accept()
# while True:
#     datelen = conn.recv(4)
#     size = struct.unpack('i',datelen)[0]
#     msg = conn.recv(size).decode('utf-8')
#     ntime = datetime.now()
#     ntime = ntime.strftime(msg).encode('utf-8')
#     nlen = struct.pack('i',len(ntime))
#     conn.send(nlen)
#     conn.send(ntime)
#     time.sleep(1)
# conn.close()
# sk.close()
# 3.请总结进程、线程、协程的区别。和应用场景。
# 进程：开销大，数据隔离，可以利用多核，数据不安全，操作系统级别
# 线程：开销中，数据共享，cpython解释器下不能利用多核，数据不安全，操作系统控制。
# 协程：开销校 数据共享，不能利用多核，数据安全，用户数据控制。
# 用线程和协程完成爬虫任务；