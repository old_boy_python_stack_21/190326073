# -*- coding: utf-8 -*-
# 1.多线程实现socket server端并发
# import socket
# import struct
# from threading import Thread,Lock
# sk = socket.socket()
# sk.bind(('192.168.12.60',6666))
# sk.listen()
# lock = Lock()
# def server():
#         conn,addr = sk.accept()
#         len_size = conn.recv(4)
#         size = struct.unpack('i',len_size)[0]
#         msg = conn.recv(size)
#         print(msg.decode('utf-8'))
#         msg_s = msg.decode('utf-8')+'server端回复'
#         a_len = struct.pack('i', len(msg_s))
#         conn.send(a_len)
#         conn.send(msg_s.encode('gbk'))
#         # conn.close()
#         # sk.close()
# for i in range(10):
#     Thread(target=server).start()
# 2.使用多线程请求网页并把网页内容写到文件内
# 第一种不适用生产者消费者模型
# import requests
# from concurrent.futures import ThreadPoolExecutor
# url_lst = ['https://www.baidu.com',
#            'https://www.cnblogs.com',
#            'https://www.douban.com',
#            'https://www.tencent.com',
#            'https://www.iqiyi.com']
# def get_url(i):
#     res =requests.get(i)
#     res = i[12:] + '\n' + res.text
#     name = i[12:]+'.txt'
#     with open(name,mode='w',encoding='gb18030') as f:
#         f.write(res)
#         print('写入成功%s'%i)
# tp = ThreadPoolExecutor(5)
# for i in url_lst:
#     ret = tp.submit(get_url,i)
#     print('%s获得资源成功'%i)
# 第二种使用生产者消费者模型
# import requests
# from multiprocessing import Process,Queue
# from concurrent.futures import ThreadPoolExecutor
# url_lst = ['https://www.baidu.com',
#            'https://www.cnblogs.com',
#            'https://www.tencent.com',
#            'https://www.iqiyi.com']
# def producer(i,q):
#     res =requests.get(i)
#     res = i[12:] + '\n' + res.text
#     name = i[12:]+'.txt'
#     q.put(res)
#     q.put(name)
# def consumer(q):
#     while True:
#         print(0)
#         res = q.get()
#         print(1)
#         name = q.get()
#         print(2)
#         if not name:
#             print('全部完成')
#             break
#         with open(name,mode='w',encoding='gb18030') as f:
#             f.write(res)
#             print('写入成功%s'%i)
# tp = ThreadPoolExecutor(5)
# q = Queue(5)
# p_ls = []
# for i in url_lst:
#     ret = tp.submit(producer,i,q)
#     m = tp.submit(consumer,q)
#     print('%s获得资源成功'%i)
# 7.什么是死锁现象，如何产生，如何解决
# 死锁是由于线程或者进程中有多把锁，多把锁在交替使用的时候产生了运行矛盾，
# 导致无法正常继续运行产生了阻塞，解决方案：使用递归锁把多把互斥锁编程成一把递归锁，
# 可以快速的解决问题，但是运行效率差，而且递归锁在多把使用的时候也可能会发生死锁现象。
# 优化代码逻辑，可以使用互斥锁解决问题，效率相对较好，解决问题的效率相对较低。
# 8.并发编程中有哪些锁，都有哪些特点？
# 互斥锁：在同一个线程中，不能连续多次acquire，必须release之后才可以。
# 递归锁：在一个线程中连续多次acquire不会死锁，解锁是也需要多次release解锁。
# 互斥锁：
# 9.cpython解释器下的线程是否数据安全？
# 不安全，由于线程中的数据共享，以及线程的并发机制和cpu时间片轮转机制，所以导致有可能
# 某线程未全部运行完毕，导致线程切换引起的数据混乱。
# 10.尝试：在多进程中开启多线程。在进程池中开启进程池
# from concurrent.futures import ThreadPoolExecutor
# import os,time,random
# def func(i):
#     print('外部开始', os.getpid())
#     time.sleep(random.randint(1,3))
#     print('外部结束', os.getpid())
#     def func(i):
#         print('内部开始', os.getpid())
#         time.sleep(random.randint(1, 3))
#         print('内部结束', os.getpid())
#         return '内部%s * %s'%(i,os.getpid())
#     tp = ThreadPoolExecutor(20)
#     ret_l = []
#     for i in range(10):
#         ret = tp.submit(func, i)
#         ret_l.append(ret)
#     tp.shutdown()
#     print('main')
#     for ret in ret_l:
#         print('------>', ret.result())
#     return '外部%s * %s'%(i,os.getpid())
#
# tp = ThreadPoolExecutor(20)
# ret_l = []
# for i in range(10):
#     ret = tp.submit(func,i)
#     ret_l.append(ret)
# tp.shutdown()
# print('main')
# for ret in ret_l:
#     print('------>',ret.result())