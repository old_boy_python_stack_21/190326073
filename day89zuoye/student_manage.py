from flask import Flask, render_template, request, session,redirect
from stu import bp
from index import bqi
from login import bql
from settings import DebugConfig,TextConfig


STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}
app = Flask(__name__)
app.secret_key = 'hehe'
app.config.from_object(DebugConfig)

app.register_blueprint(bp)
app.register_blueprint(bqi)
app.register_blueprint(bql)

@app.before_request
def checklogin():
    if request.path == '/login':
        return
    if not session.get('user'):
        return redirect('/login')


@app.errorhandler(404)
def error404(errorMessage):
    return render_template('error404.html')



if __name__ == '__main__':
    app.run()