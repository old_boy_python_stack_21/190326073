from flask import Blueprint,request,render_template

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

bp = Blueprint("app01",__name__)

@bp.route("/stu_list")
def stu_list():
    if request.method == 'GET':
        if request.args.to_dict():
            id = int(request.args.to_dict().get('id'))
            dic = STUDENT_DICT.get(id)
            dic['id'] = id
            return render_template('stu.html',dic=dic)
        return render_template('stu_list.html',stu = STUDENT_DICT)