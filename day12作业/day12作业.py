# 1.三元函数的格式及作用
# 简便快捷判断，为了解决简单的if else 情况
# a = 2
# b = 3
# v = a if a>b else b
# 2. 匿名函数 lambda表达式，没有直接的函数定义。
# 3.尽量多的默写内置函数。
# 输入输出：print/input 其他len/open/range/id/type
# 强制转换：dict/list/tuple/int/str/bool/set
# 数学相关：abs/float/max/min/sum/divmod
# 进制相关：bin/oct/int/hex 编码相关：chr ord
# 4.filter/map/reduce函数的作用分辨是什么
# filter多种数据类型通过过滤筛选获得想要的数据类型
# v = [1,2,3,4,'df',34,'kjh']
# def func(x):
#     if type(x) == int:
#         return True
#     return False
# result = filter(func,v)
# result = filter(lambda x:type(x) == int,v)
# print(list(result))
# reduce 是让一种数据类型进行循环元素，最终得到一个结果。
# import functools
# v = ['wo','shi','chuanqi']
# result = functools.reduce(lambda x,y:x+y,v)
# print(result)
# map循环每一个元组做统一处理，处理的结果保存到新列表内。
# v1 = [11,22,33,44]
# result = map(lambda x:x+100,v1)
# print(list(result))
# 5.看代码写结果
# def func(*args, **kwargs):
#     print(args, kwargs)

# a. 执行 func(12,3,*[11,22]) ，输出什么？#(12,3,11,22) {}
# b. 执行 func(('alex','武沛齐',),name='eric') #(('alex','武沛齐'),){name:'eric'}
# 6.看代码分析结果
# 22
# def func(arg):
#     return arg.pop(1)
#
# result = func([11,22,33,44])
# print(result)
# 7.看代码分析结果
# (9,9)
# func_list = []
# for i in range(10):
#     func_list.append(lambda :i)
#
# v1 = func_list[0]()
# v2 = func_list[5]()
# print(v1,v2)
# 8.看代码分析结果
# (11,10)
# func_list = []
# for i in range(10):
#     func_list.append(lambda x:x+i)
# v1 = func_list[0](2)
# v2 = func_list[5](1)
# print(v1,v2)
# 9.看代码分析结果
# 0\n 2\n 4\n 6\n 8\n 10\n 12\n 14\n 16\n 18
# func_list = []
#
# for i in range(10):
#     func_list.append(lambda x:x+i)
#
# for i in range(0,len(func_list)):
#     result = func_list[i](i)
#     print(result)
# 10.看代码分析结果
# f2 f1 None
# def f1():
#     print('f1')
#
# def f2():
#     print('f2')
#     return f1
#
# func = f2()
# result = func()
# print(result)
# 11.看代码分析结果【面试题】
# f2 f1 f3 None
# def f1():
#     print('f1')
#     return f3()
#
# def f2():
#     print('f2')
#     return f1
#
# def f3():
#     print('f3')
#
# func = f2()
# result = func()
# print(result)
# 12.看代码分析结果
# 景女神 None
# name = '景女神'
#
# def func():
#     def inner():
#         print(name)
#     return inner()
#
# v = func()
# print(v)
# 13.看代码分析结果
# 景女神 老男孩
# name = '景女神'
#
# def func():
#     def inner():
#         print(name)
#         return "老男孩"
#     return inner()
#
# v = func()
# print(v)
# 14.看代码分析结果
# 景女神 老男孩
# name = '景女神'
#
# def func():
#     def inner():
#         print(name)
#         return '老男孩'
#     return inner
#
# v = func()
# result = v()
# print(result)
# 15.看代码分析结果
# inner内存地址 inner内存地址
# def func():
#     name = '武沛齐'
#     def inner():
#         print(name)
#         return '老男孩'
#     return inner
#
# v1 = func()
# v2 = func()
# print(v1,v2)
# 16.看代码写结果
# inner内存地址 inner内存地址
# def func(name):
#     def inner():
#         print(name)
#         return '老男孩'
#     return inner
#
# v1 = func('金老板')
# v2 = func('alex')
# print(v1,v2)
# 17.看代码写结果
# None inner内存地址
# def func(name=None):
#     if not name:
#         name= '武沛齐'
#     def inner():
#         print(name)
#         return '老男孩'
#     return inner
#
# v1 = func()
# v2 = func('alex')
# print(v1,v2)
# 18.看代码写结果
# （武沛齐内存地址，alex内存地址，银角武沛齐，金角alex）
# def func(name):
#     v = lambda x:x+name
#     return v
#
# v1 = func('武沛齐')
# v2 = func('alex')
# v3 = v1('银角')
# v4 = v2('金角')
# print(v1,v2,v3,v4)
# 19.看代码写结果
# 9   [func,func,func,func,func,func,func,func,func,func]  (100,100)
# NUM = 100
# result = []
# for i in range(10):
#     func = lambda : NUM      # 注意：函数不执行，内部代码不会执行。
#     result.append(func)
#
# print(i)
# print(result)
# v1 = result[0]()
# v2 = result[9]()
# print(v1,v2)
# 20.看代码写结果
# 9  [func,func,func,func,func,func,func,func,func,func]  （9,9）
# result = []
# for i in range(10):
#     func = lambda : i      # 注意：函数不执行，内部代码不会执行。
#     result.append(func)
#
# print(i)
# print(result)
# v1 = result[0]()
# v2 = result[9]()
# print(v1,v2)
# 21.看代码分析结果【面试题】
# 9  [inner内存地址，、、、、]（10个）   0 None  9 None
# def func(num):
#     def inner():
#         print(num)
#     return inner
#
# result = []
# for i in range(10):
#     f = func(i)
#     result.append(f)
#
# print(i)
# print(result)
# v1 = result[0]()
# v2 = result[9]()
# print(v1,v2)
# 22.程序设计题
def login():
    print('*******欢迎使用xx购物商城*******\n1.商品管理\n2.会员管理(不可选，开发中)')
    login_list = [choice_1_list,choice_2_list]
    a = int(input('请输入要选择的数字:'))
    login_list[a-1]()
def choice_2_list():
    print('此功能开发中')
    return login()
def choice_1_list():
    print('*****欢迎使用xx购物商城【商品管理】*****\n'
                     '1.查看商品列表\n'
                     '2.根据关键字搜索制定商品\n'
                     '3.录入商品\n'
          '请选择（输入N返回上一级）'
                    )
    choice_map = ['查看商品列表','根据关键字搜索定制商品','录入商品','return']
    b = input('请输入要选择的序号：')
    if b == 'N':
        return login()
    else:
        b = int(b)
        choice_map[b-1]()
def shop_list():


login()



