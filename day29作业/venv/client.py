import socket,os,json,hashlib
def get_md5(name,pwd):
    obj = hashlib.md5(name.encode('gbk'))
    obj.update(pwd.encode('gbk'))
    pwd = obj.hexdigest()
    return pwd
sk = socket.socket()
sk.connect(('192.168.12.50',6666))
name = input('输入姓名：')
pwd = input('输入密码：')
dic = {'name':name,'pwd':get_md5(name,pwd)}
dic_info = json.dumps(dic)
sk.send(dic_info.encode('gbk'))
bkinfo = sk.recv(1024).decode('gbk')
print(bkinfo)
if bkinfo:
    while True:
        path = input('请输入要发送的文件路径：')
        if path.upper() == 'N':
            sk.send('用户已退出'.encode('gbk'))
            break
        if os.path.isabs(path):
            name = os.path.basename(path)
            size = os.path.getsize(path)
            dic = {'name':name,'size':size}
            dic_str = json.dumps(dic)
            sk.send(dic_str.encode('gbk'))
            with open(path,mode='rb') as f:
                content = f.read()
                sk.send(content)
                print('发送[%s]成功'%name)
        else:
            print('输入有误')
sk.close()