# 1.请将列表中的每个元素通过 "_" 链接起来
user = ['李少奇', '李启航', '渣渣辉']
result = '_'.join(user)
print(result)
#for i in users:
    #print(i)
    #result = '_'.join(i)
    #print(result)

#2.请将列表中的每个元素通过 "_" 链接起来。

users = ['李少奇', '李启航', 666, '渣渣辉']
for i in range(0,len(users)):
    users[i] = str(users[i])
v = '_'.join(users)
print(v)

# 3.请将元组 v1 = (11,22,33) 中的所有元素追加到列表 v2 = [44,55,66] 中。
v1 = (11, 22, 33)
v2 = [44, 55, 66]
for k in v1:
    v2.append(k)
print(v2)
# 4.请将元组 v1 = (11,22,33,44,55,66,77,88,99) 中的所有偶数索引位置的元素 追加到列表 v2 = [44,55,66] 中。
v3 = (11, 22, 33, 44, 55, 66, 77, 88, 99)
v4 = [44, 55, 66]
v4.extend(v3[1::2])
print(v4)
# 5.将字典的键和值分别追加到 key_list 和 value_list 两个列表中
key_list = []
value_list = []
info = {'k1': 'v1', 'k2': 'v2', 'k3': 'v3'}
for k in info.keys():
    key_list.append(k)
print(key_list)
for v in info.values():
    value_list.append(v)
print(value_list)
# 6.字典dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
#请循环输出所有的key
for k1 in dic.keys():
    print(k1)
#请循环输出所有的value
for v5 in dic.values():
    print(v5)
#请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
dic['k4'] = 'v4'
print(dic)
#请在修改字典中 "k1" 对应的值为 "alex"，输出修改后的字典
dic['k1'] = 'alex'
print(dic)
#请在k3对应的值中追加一个元素 44，输出修改后的字典
dic['k3'].append(44)
print(dic)
#请在k3对应的值的第 1 个位置插入个元素 18，输出修改后的字典
dic['k3'].insert(0, 18)
#7.请循环打印k2对应的值中的每个元素。
info = {'k1':'v1','k2':[('alex'), ('wupeiqi'), ('oldboy')],}
for i1 in info['k2']:
    print(i1)
#8.有字符串"k: 1|k1:2|k2:3  |k3 :4" 处理成字典 {'k':1,'k1':2....}
info = {}
a = 'k: 1|k1:2|k2:3|k3 :4'
for i in a.split('|'):
    v1,v2 = i.split(':')
    info[v1] = v2
print(info)
#9.写代码有如下值 li= [11,22,33,44,55,66,77,88,99,90] ,将所有大于 66 的值保存至字典的第一个key对应的列表中，将小于 66 的值保存至第二个key对应的列表中。
li = [11, 22, 33, 44, 55, 66, 77, 88, 99, 90]
r = {'k1':[], 'k2':[]}
for i in li:
    if i < 66:
        r['k1'].append(i)
    if i >= 66:
        r['k2'].append(i)
print(r)
#10.输出商品列表，用户输入序号，显示用户选中的商品
goods = [
        {"name": "电脑", "price": 1999},
        {"name": "鼠标", "price": 10},
        {"name": "游艇", "price": 20},
        {"name": "美女", "price": 998}
    ]
count = 0
for item in goods:
    print(count+1, item['name'], item['price'])
    count += 1
while True:
    num = input('请输入要选择的商品序号：')
    num = num.strip()  #去除空格
    if not num.isdigit():
        print('请输入正确数字')
        continue
    num = int(num)
    if 0 < num < 5:
        print(num,goods[num-1]['name'],goods[num-1]['price'])
        break
    print('超出范围')

#11.看代码写结果
v = { 'users':9}