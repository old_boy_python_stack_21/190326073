# day05数据类型

## 今日内容

- 字典

### 作业题讲解和变换

### 内容回顾和补充

- int

  - py2/py3

  - 除法

  - 强制转换：

    - int("字符串") 【重要】
    - int(布尔值)

  - bool

    - 强制转换：

      - bool(整数) 	->bool(1)/..		->bool(0)

      - bool(字符串)     ->bool("xx")               ->bool("")

      - bool(列表)         ->bool(())                   ->bool(空元组)

        ```python
        v1 = bool(0)
        
        ```

        



