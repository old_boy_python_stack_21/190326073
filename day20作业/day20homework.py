# 1.面向对象的三大特性：封装、继承、多态
# 封装
# class Person:
#     def __init__(self,name):
#         self.name = name
# p = Person()
# p.name = 'alex'
# 基层
# class Fater:
#     def f1(self):
#         print('f1')
# class Son(Fater):
#     def f2(self):
#         self.f1()
#         print('f2')
# f = Son()
# f.f2()
# 多态
# class Fater:
#     def play(self):
#         print('打游戏')
# class Son(Fater):
#     def play(self):
#         print('学习')
# dad = Fater()
# dad.play()
# son = Son()
# son.play()
# 2.鸭子模型：同一操作作用于不同对象，可以有不同的解释产生不同的结果。
# 3.列举面向对象中的类成员和对象成员。
# 类成员：类属性、类方法。 对象成员：类、方法、可变对象
# 4.@methondclass和@staicmethond的区别
# @clasmethon是类方法，至少有一个cls参数 @staicmethond是静态方法，
# 参数无限制。两者执行都是：类.方法名()   对象.方法名()
# 5.python中的双下划线是表示私有类型，只有在本类中可执行。
# 6.看代码写结果
# class Base:
#     x = 1
# obj = Base()
# print(obj.x) #1
# obj.y = 123
# print(obj.y) #123
# obj.x = 123
# print(obj.x) #123
# print(Base.x) #1
# 7.看代码写结果
# class Parent:
#     x = 1
# class Child1(Parent):
#     pass
# class Child2(Parent):
#     pass
# print(Parent.x, Child1.x, Child2.x)  # 1 1 1
# Child2.x = 2
# print(Parent.x, Child1.x, Child2.x)  # 1 1 2
# Child1.x = 3
# print(Parent.x, Child1.x, Child2.x) # 1 3 2
# 8.看代码写结果
# class Foo(object):
#     n1 = '武沛齐'
#     n2 = '金老板'
#     def __init__(self):
#         self.n1 = '女神'
# obj = Foo()
# print(obj.n1) # 女神
# print(obj.n2) # 金老板
# 9.看代码写结果
# class Foo(object):
#     n1 = '武沛齐'
#     def __init__(self,name):
#         self.n2 = name
# obj = Foo('太白')
# print(obj.n1) #  武沛齐
# print(obj.n2)  #  太白
#
# print(Foo.n1)  #  武沛齐
# print(Foo.n2)  #  不能执行，foo内无n2，内部方法有但不可用
# 10.看代码写结果
# class Foo(object):
#     a1 = 1
#     __a2 = 2
#     def __init__(self, num):
#         self.num = num
#         self.__salary = 1000
#
#     def show_data(self):
#         print(self.num + self.a1)
# obj = Foo(666)
# print(obj.num)  #666
# print(obj.a1)  #1
# # print(obj.__salary)  #私有，只能内部使用
# # print(obj.__a2)  #私有，只能内部使用
# print(Foo.a1)  #1
# # print(Foo.__a2)  #私有，只能内部使用
# 11.看代码写结果
# class Foo(object):
#     a1 = 1
#
#     def __init__(self, num):
#         self.num = num
#
#     def show_data(self):
#         print(self.num + self.a1)
# obj1 = Foo(666)
# obj1 = Foo(999)
# print(obj1.num)  #999
# print(obj1.a1)  #1
#
# obj1.num = 18
# obj1.a1 = 99
#
# print(obj1.num)  #18
# print(obj1.a1)  #99
#
# # print(obj2.a1)  #未被定义
# # print(obj2.num)  #未被定义
# # print(obj2.num)  #未被定义
# print(Foo.a1)  #1
# print(obj1.a1)  #99
# 12.看代码写结果
# class Foo(object):
#
#     def f1(self):
#         return 999
#
#     def f2(self):
#         v = self.f1()
#         print('f2')
#         return v
#
#     def f3(self):
#         print('f3')
#         return self.f2()
#
#     def run(self):
#         result = self.f3()
#         print(result)
# obj = Foo()
# v1 = obj.run() # f3  f2
# print(v1) #999 None
# 13.看代码写结果
# class Foo(object):
#
#     def f1(self):
#         print('f1')
#
#     @staticmethod #静态方法
#     def f2():
#         print('f2')
#
# obj = Foo()
# # obj.f1() #f1
# # obj.f2() #f2
# #
# Foo.f1() #f1 缺少括号，对象可以调用方法
# Foo.f2() #f1
# 14.看代码写结果
# class Foo(object):
#     def f1(self):
#         print('f1')
#     @classmethod #类方法
#     def f2(cls):
#         print('f2')
# obj = Foo()
# obj.f1() #f1
# obj.f2() #f2

# Foo.f1() #对象可调用方法，加括号可调用输出f1
# Foo.f2() #f2
# 15.看代码写结果
# class Foo(object):
#     def f1(self):
#         print('f1')
#         self.f2()
#         self.f3()
#     @classmethod
#     def f2(cls):
#         print('f2')
#     @staticmethod
#     def f3():
#         print('f3')
# obj = Foo()
# obj.f1()#f1  f2  f3
# 16.看代码写结果
# class Base(object):
#     @classmethod
#     def f2(cls):
#           print('f2')
#     @staticmethod
#     def f3():
#           print('f3')
# class Foo(object):
#     def f1(self):
#         print('f1')
#         self.f2()
#         self.f3()
# obj = Foo()
# obj.f1() #f1 以下的不能调用，不在同一类中，而且也不在父类中。
# 17.看代码写结果
# class Foo(object):
#     def __init__(self, num):
#         self.num = num
# v1 = [Foo for i in range(10)]
# v2 = [Foo(5) for i in range(10)]
# v3 = [Foo(i) for i in range(10)]
# print(v1)  #[Foo,Foo,Foo,Foo,Foo,Foo,Foo,Foo,Foo,Foo]
# print(v2)  #[Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5),Foo(5)]
# print(v3)  #[Foo(0),Foo(1),Foo(2),Foo(3),Foo(4),Foo(5),Foo(6),Foo(7),Foo(8),Foo(9)]
# 18.看代码写结果
# class StarkConfig(object):
#     def __init__(self, num):
#         self.num = num
#     def changelist(self, request):
#         print(self.num, request)
# config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
# for item in config_obj_list:
#     print(item.num) # 1  2  3
# 19.看代码写结果
# class StarkConfig(object):
#     def __init__(self, num):
#         self.num = num
#
#     def changelist(self, request):
#         print(self.num, request)
# config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
# for item in config_obj_list:
#     item.changelist(666) # 1 666 、  2 666  、 3 666
# 20.看代码写结果
# class Department(object):
#     def __init__(self, title):
#         self.title = title
# class Person(object):
#     def __init__(self, name, age, depart):
#         self.name = name
#         self.age = age
#         self.depart = depart
# d1 = Department('人事部')
# d2 = Department('销售部')
# p1 = Person('武沛齐', 18, d1)
# p2 = Person('alex', 18, d1)
# p3 = Person('安安', 19, d2)
# print(p1.name) #武沛齐
# print(p2.age) #18
# print(p3.depart) #不输出，加.title后输出销售部
# print(p3.depart.title) #销售部
# 21.看代码写结果
# class Department(object):
#     def __init__(self, title):
#         self.title = title
# class Person(object):
#     def __init__(self, name, age, depart):
#         self.name = name
#         self.age = age
#         self.depart = depart
#     def message(self):
#         msg = "我是%s,年龄%s,属于%s" % (self.name, self.age, self.depart.title)
#         print(msg)
# d1 = Department('人事部')
# d2 = Department('销售部')
# p1 = Person('武沛齐', 18, d1)
# p2 = Person('alex', 18, d1)
# p1.message() #我是武沛齐,年龄18,属于人事部
# p2.message() #我是alex,年龄18,属于人事部
# 22.写代码
class School:
    def __init__(self,sc):
        self.sc = sc
class Course:
    def __init__(self, course1 = None,course2 = None,course3 = None):
        self.course1 = course1
        self.course2 = course2
        self.course3 = course3
class Grades(School,Course):
    def __init__(self, school,courise,classname, starttime,endtime,pepole_num):
        self.school = school
        self.courise = courise
        self.classname = classname
        self.starttime = starttime
        self.endtime = endtime
        self.pepole = pepole_num
    def messge(self):
        msg = '%s%s开设，开班为%s,' \
           '时间是%s-%s,有%s人' %(self.school,self.courise,self.classname,
            self.starttime,self.endtime,self.pepole)
        print(msg)
s1 = School('北京')
s2 = School('上海')
s3 = School('深圳')
# print(s1.sc)
c1 = Course('linux','python','go')
c2 = Course('linux','python')
c3 = Course('python')
g1 = Grades(s1.sc,c1.course1+' '+c1.course2 +' '+ c1.course3 ,'21期、22期','三月','十月','80人')
g1.messge()
g2 = Grades(s2.sc,c2.course2 ,'1期、2期','三月','十月','80人')
g2.messge()
g3 = Grades(s3.sc,c3.course3 ,'1期、2期','三月','十月','80人')
g3.messge()

