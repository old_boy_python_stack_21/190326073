# -*- coding: utf-8 -*-
# Generated by Django 1.11.21 on 2019-06-24 09:12
from __future__ import unicode_literals

import app01.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='phone',
            field=app01.models.MyCharField(default=11, max_length=11),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='person',
            name='birth',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
