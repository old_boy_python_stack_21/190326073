from django.db import models


class MyCharField(models.Field):
    """
    自定义的char类型的字段类
    """

    def __init__(self, max_length, *args, **kwargs):
        self.max_length = max_length
        super(MyCharField, self).__init__(max_length=max_length, *args, **kwargs)

    def db_type(self, connection):
        """
        限定生成数据库表的字段类型为char，长度为max_length指定的值
        """
        return 'char(%s)' % self.max_length

class Person(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,verbose_name='姓名',db_column='username',unique=True)
    age = models.IntegerField(default=18,verbose_name='年纪')
    # birth = models.DateTimeField(auto_now_add=True)
    birth = models.DateTimeField(auto_now=True)
    phone = MyCharField(max_length=11,null=True,blank=True)
    gender = models.BooleanField('性别',choices=((0,'女'),(1,'男')))
    def __str__(self):
        return '{}-{}'.format(self.pk,self.name)
    class Meta:
        db_table = 'person'#数据库表名
        verbose_name  = '个人信息'#admin中的表名
        verbose_name_plural = '个人信息info'#admin中显示名
        index_together = [('name','age')]
        unique_together = [('name','age')]#添加了约束，name跟age只能出现一次


class Publisher(models.Model):
    name = models.CharField(max_length=32,verbose_name='名称')
    def __str__(self):
        return self.name
class Book(models.Model):
    title = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=6,decimal_places=2)
    sale = models.IntegerField()
    kucun = models.IntegerField()
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE,related_name='books',related_query_name='book')
    def __str__(self):
        return self.title
class Author(models.Model):
    name = models.CharField(max_length=32,)
    books = models.ManyToManyField(Book,related_name='author')
    def __str__(self):
        return self.name
# Create your models here.
