import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','orm_prictice.settings')
import django

django.setup()
from app01 import models

#基于对象的查询
book_obj = models.Book.objects.get(title='桃花侠大战菊花怪')
#正向查询
# print(book_obj.pub)
#反向查询
pub_obj = models.Publisher.objects.get(pk=1)
# print(pub_obj.book_set.all())#类名小写_set 没有指定relate_name时
# print(pub_obj.book.all())#relate_name='book'从新定义了关联的name为book

#基于字段的查询
ret = models.Book.objects.filter(title='桃花侠大战菊花怪')
#查询老男孩出版的书
ret = models.Book.objects.filter(pub__name='清华出版社')

ret = models.Publisher.objects.filter(book__title='桃花侠大战菊花怪')
print(ret)