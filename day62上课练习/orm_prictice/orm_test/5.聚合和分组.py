import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','orm_prictice.settings')
import django

django.setup()

from app01 import models

from django.db.models import Max,Avg,Sum,Min,Count
#aggregate 聚合
# ret = models.Book.objects.all().aggregate(Max('price'))
# ret = models.Book.objects.all().aggregate(avg=Avg('price'),max=Max('price'))
# ret = models.Book.objects.filter(pk__gt=3).aggregate(avg=Avg('price'),max=Max('price'))
# print(ret)
#统计每一本书的的作者个数
# annotate 注释
# ret = models.Book.objects.annotate(count=Count('author'))
# for i in ret:
#     print(i.count,i.title)
#统计每个出版社最便宜的书
#方式一
# ret = models.Publisher.objects.annotate(Min('book__price')).values()
# for ret in ret:
#     print(ret)

#方式二

# ret = models.Book.objects.values('pub__name').annotate(Min('price'))
# for ret in ret:
#     print(ret)

# 统计不止一个作者的图书
# ret = models.Book.objects.annotate(count=Count('author')).filter(count__gt=1)
# print(ret)

# 查询各个作者出的书的总价
ret = models.Author.objects.annotate(Sum('books__price')).values()
print(ret)