import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','orm_prictice.settings')
import django

django.setup()
from app01 import models
#all()   ---QuerySet 对象列表
# ret = models.Person.objects.all()
# print(ret[1].name)
#get()   获取满足添加的一个数据
# ret = models.Person.objects.get(pk=5)
# print(ret)
#filter  过滤筛选
# ret = models.Person.objects.filter(gender=1)
# print(ret)
#exclude  什么什么之外 排除此选项
# ret = models.Person.objects.exclude(pk=4)
# print(ret)
#values()  拿到所有的字段和字段的值
#values('字段')  拿到指定的字段和字段的值
#valueslist()  拿到指定的字段的值
# ret = models.Person.objects.values('pid','name')
# ret = models.Person.objects.values_list('pid','name')
# for i in ret:
#     print(i)
#order_by()  排序  负号时降序，默认升序
ret = models.Person.objects.all().order_by('-age','pk')
print(ret)
#reverse()  必须排序好的才能反转
ret = models.Person.objects.all().order_by('pk').reverse()
print(ret)
#distinct 去重
ret = models.Person.objects.values('age').distinct()
print(ret)
#count() 计数
ret = models.Person.objects.values('age').count()
print(len(models.Person.objects.all()))
print(ret)
#first（） 取第一个元素 没有则None
#exists 查询数据是否存在
ret = models.Person.objects.filter(pk=3).exists()
print(ret)