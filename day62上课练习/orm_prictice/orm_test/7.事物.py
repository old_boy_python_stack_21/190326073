import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','orm_prictice.settings')
import django

django.setup()

from app01 import models

from django.db import transaction

with transaction.atomic():
    #进行一系列的原子性操作
    models.Publisher.objects.create(name='孙悟空')
    models.Publisher.objects.create(name='猪八戒')
try:
    with transaction.atomic():
    #进行一系列的原子性操作
        models.Publisher.objects.create(name='孙悟空')
        # int('sss')
        models.Publisher.objects.create(name='猪八戒')
except Exception as e:
    print(e)