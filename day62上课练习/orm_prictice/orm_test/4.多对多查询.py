import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','orm_prictice.settings')
import django

django.setup()

from app01 import models
#基于对象的正向查询
# mjj = models.Author.objects.get(pk=1)
# print(mjj.books.all())

#基于对象的反向查询
# book_obj = models.Book.objects.filter(title='嘻嘻哈哈').first()
# print(book_obj.author_set.all())
# print(book_obj.author.all())#定义了related_name=‘author’3
#查作者
# ret = models.Author.objects.filter(books__title='桃花豆豆啊')
# print(ret)
#查作者
# ret = models.Book.objects.filter(author__name='MJJ')
# print(ret)



#关系管理对象的方法
mjj = models.Author.objects.get(pk=1)
#all() 所关联的所有对象
print(mjj.books.all())
print(mjj)
# set 设置多对多的关系 []
# mjj.books.set([1,2,3])
mjj.books.set(models.Book.objects.filter(pk__in=[1,2,3]))

#add 添加多对多的关系 /(id,id)  (对象,对象)
mjj.books.add('4',5)

#remove 删除多对多的关系
# mjj.books.remove(4,5)
mjj.books.remove(*models.Book.objects.filter(pk__in=[4,5]))

#clear() 清除所有的多对多关系
# mjj.books.clear()

#create() 创建多对多关系
# obj = mjj.books.create(title='跟MJJ学前端',pub_id=1)
# print(obj)

# book_obj = models.Book.objects.get(pk=1)
# obj = book_obj.author.create(name='taibai')
# print(obj)


pub_obj = models.Publisher.objects.get(pk=1)
pub_obj.books.set(models.Book.objects.filter(pk__in=[4,5]))#不能用id
print(pub_obj.books.all())

# pub_obj.books.add(*models.Book.objects.filter(pk__in=[1,2]))
# pub_obj.books.remove(*models.Book.objects.filter(pk__in=[1,2]))
# pub_obj.books.clear()
pub_obj.books.create(title='用Python去养猪')







