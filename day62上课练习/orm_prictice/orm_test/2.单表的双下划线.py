import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','orm_prictice.settings')
import django

django.setup()
from app01 import models
#pk__gt greater than  大于
#pk__lt less than  大于
#pk__gte greater than  equal 大于等于
#pk__lte less than equal 大于等于
#pk__range[2,3]    两边都包含
#pk__in[2,3,6]    在列表内的
#name__contains  包含内容
#name__icontains  包含内容,忽略大小写
#name__startswith  以什么开头
#name__istartswith  以什么开头忽略大小写
#name__endswith  以什么结尾
#name__iendswith  以什么结尾忽略大小写
#birth__year='2019'  筛选年
#value__isnull=True or False  判断某个库的value是否为空的项
ret = models.Person.objects.filter(pk__gt=1)
ret = models.Person.objects.filter(name__contains='a')
ret = models.Person.objects.filter(birth__year='2019')
ret = models.Person.objects.filter(phone__isnull=True)
# ret = models.Person.objects.all()
print(ret)