from django.shortcuts import render,redirect,HttpResponse
from app01 import models

def index(request):
    return render(request,'index.html')

def login(request):
    if request.method == 'POST':
        name = request.POST['username']
        pwd = request.POST['password']
        print(name,pwd)
        if models.User.objects.filter(username=name,password=pwd):
            return redirect('/index/')
    return render(request,'login.html')
# Create your views here.
