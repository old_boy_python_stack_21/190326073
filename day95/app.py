from flask import Flask,request
# from setting import MongoDB
import os

from settings import AVATAR_PATH
app = Flask(__name__)
app.debug = True
app.secret_key = "****"


@app.route("/upload",methods=["POST"])
def upload():
    avatar = request.files.get("avatar")
    avatar_file_path = os.path.join(AVATAR_PATH,avatar.name)
    avatar.save(avatar_file_path)
    return {"code":0,"msg":"保存成功"}


@app.route("/reg",methods=["POST","GET"])
def reg():
    user_info = request.form.to_dict()
    MongoDB.usertest.insert_one(user_info)
    return {"code":0,"msg":"注册成功"}

@app.route("/login",methods=["POST","GET"])
def login():
    user_info = request.form.to_dict()
    user_dict = MongoDB.usertest.find_one(user_info)
    if user_dict:
        return {"code":0,"msg":"登录成功"}

if __name__ == '__main__':
    app.run("0.0.0.0",9527)