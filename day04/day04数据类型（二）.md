<<<<<<< HEAD
# day04数据类型（二）

## 今日内容

- 列表
- 元组

## 内容回顾和补充

1.计算机基础

- 硬件：CPU/内存/硬盘/主板/网卡

- 操作系统

  - Linux（免费/开源）
    - centos
    - Ubuntu
    - redhat
  - windows
  - mac

- 解释器/编译器

  - 补充：编译型语言和解释型语言？

    ​	#  编译型：代码写完后，编译器将其整体编译成一个文件交给计算机执行

    ​	#  解释型：写完代码后交给解释器，解释器会从上至下逐行解释，边解释边执行。

- 软件（应用程序）

2.环境的安装

- Python解释器
  - py2
  - py3
- 开发工具：pycharm（推荐）/文本编辑器

3.Python语法

​	1.解释器路径：hello.py

``` python
#!/usr/bin/env python
print('你好')
```

​	Linux系统应用：

- ​	赋予文件可执行权限
  - ./hello.py

2.编码

``` python
#!usr/bin/env python
#-*- coding:utf-8 -*-

print('你好')
```

​	1.编码种类

- ​	ascii
  - unicode
  - utf-8/utf-16
  - gbk/gb2312

2. 中文表示

   - utf-8:3字节
   - gbk:2字节

   3.Python默认解释器编码

   - py3:utf-8
   - py2:ascii

3. 输入输出

- py2:

  - 输入：raw_input
  - 输出：print""

- py3:

  - 输入：input
  - 输出：print()

  4.数据类型

​	1.int（转换）

- py2中有：int/long；py3中有int。	

- 强制转换：int("76")	

  2.bool

  - True/False
  - 特殊为False的其他类型：0和" "（空）

  3.str（字符串）

  - 独有功能
    - upper/lower
    - replace
    - strip/lstrip/rstrip
    - isidigit
    - split/rsplint
    - 补充：
      - startswith/endswith

  ``` python
  name = 'alex'
  # 判断是否al开头
  ···
  # 方式一
  flag = name.startswith('al')
  print(flag)
  ···
  ···
  # 方式二
  val = name[0:2]
  if vla == 'al':
      print('是以al开头')
  else：
  	print('不是')
  ···
  ```

  - format

    ``` python
    name = "我叫{0},年龄:{1}".format('老男孩'，73)
    print(name)
    ```

  - encode

    ``` python
    name = '李杰' #解释器读取到内存后，按照Unicode编码存储：8个字节。
    v1 = name.encode('utf-8')
    print(v1)# 用utf-8 每个汉字为3个字节
    v2 = name.encode('gbk')
    print(v2)#用bgk为每个汉字2个字节
    ```

  - join

    ``` python
    name = 'alex' #a_l_e_x
    result = "**".join(name)#循环每个元素，并在元素和元素之间假如连接符
    print(result)
    ```

  - 公共功能

    - 索引，获取一个字符。

    - 切片，获取一段字符串（子序列）。

    - 步长

      ``` python
      name = 'alex'
      #val = name[0:-1:2] 
      print(val)
      ```

    - 长度，获取字符长度。

    - for循环

      ``` python
      name = 'alex'
      for item in name:
          print(item)
      ```

      ``` python
      name = 'alex'
      for item in name:
          print(item)
          break
          print('123')
      ```

      ``` python
      for i in range(1,12):
      	if i == 7
          	pass
         	else:
          	print(i)
      ```

      注意：for和while的应用场景：有穷尽优先使用for，无穷尽用while

      5.变量

      6.注释

      7.条件语句

      8.循环语句：while+for+break+continue

      9.运算符

      10.字符串格式化

      - %s
      - %d
      - %%

      11.其他

      - markdown笔记

      - git

        - 本地：git软件【常用命令】
          - git status 查看文件的相关状态
          - git add .
          - git commit -m "提交记录"  进行添加和提交
          - git push origin master当前分支推送到origin主机的对应分支

        - 远程：码云/github（程序员交友平台）

        - 面试相关：

          ​	1.写出你常用的git命令。

          ​	2.你们公司是怎么用git做开发的？

          ​		1.在码云或github等代码托管的网站创建自己的仓库创建完成后码云会给一个地址。

          ​		2.自己写代码。。。

          ​		3.将代码提交到远程仓库

          - ​		初始化

            -  进入一个任意文件夹，如：D：\homework\

            - git init

            - git config "邮箱"

            - git config "姓名"

            - git remote add origin https://gitee.com/........

              注意：至此git已经将D：\homework\目录管理起来，以后此文件夹有任何变化，git都会检测到（使用git status命令可以查看状态）

          - 代码的收集并提交

            - git status
            - git add .
            - git commit -m "记录"
            - git push origin master 将本地D:\homework\下任何文件做操作
              - git status
              - git add .
              - git commit -m "记录"
              - git push origin master 将本地D：\homework\目录下的内容同步到库
            - 【避免】如果远程有本地没有的代码，必须先执行：【可能引发合并问题】
            - git push origin master
            - git status
            - git add .
            - git commit -m "记录"
            - git push origin master 将本地D:\homework\目录下的内容同步到 码云仓
              库。

=======
# day04数据类型（二）

## 今日内容

- 列表
- 元组

## 内容回顾和补充

1.计算机基础

- 硬件：CPU/内存/硬盘/主板/网卡

- 操作系统

  - Linux（免费/开源）
    - centos
    - Ubuntu
    - redhat
  - windows
  - mac

- 解释器/编译器

  - 补充：编译型语言和解释型语言？

    ​	#  编译型：代码写完后，编译器将其整体编译成一个文件交给计算机执行

    ​	#  解释型：写完代码后交给解释器，解释器会从上至下逐行解释，边解释边执行。

- 软件（应用程序）

2.环境的安装

- Python解释器
  - py2
  - py3
- 开发工具：pycharm（推荐）/文本编辑器

3.Python语法

​	1.解释器路径：hello.py

``` python
#!/usr/bin/env python
print('你好')
```

​	Linux系统应用：

- ​	赋予文件可执行权限
  - ./hello.py

2.编码

``` python
#!usr/bin/env python
#-*- coding:utf-8 -*-

print('你好')
```

​	1.编码种类

- ​	ascii
  - unicode
  - utf-8/utf-16
  - gbk/gb2312

2. 中文表示

   - utf-8:3字节
   - gbk:2字节

   3.Python默认解释器编码

   - py3:utf-8
   - py2:ascii

3. 输入输出

- py2:

  - 输入：raw_input
  - 输出：print""

- py3:

  - 输入：input
  - 输出：print()

  4.数据类型

​	1.int（转换）

- py2中有：int/long；py3中有int。	

- 强制转换：int("76")	

  2.bool

  - True/False
  - 特殊为False的其他类型：0和" "（空）

  3.str（字符串）

  - 独有功能
    - upper/lower
    - replace
    - strip/lstrip/rstrip
    - isidigit
    - split/rsplint
    - 补充：
      - startswith/endswith

  ``` python
  name = 'alex'
  # 判断是否al开头
  ···
  # 方式一
  flag = name.startswith('al')
  print(flag)
  ···
  ···
  # 方式二
  val = name[0:2]
  if vla == 'al':
      print('是以al开头')
  else：
  	print('不是')
  ···
  ```

  - format

    ``` python
    name = "我叫{0},年龄:{1}".format('老男孩'，73)
    print(name)
    ```

  - encode

    ``` python
    name = '李杰' #解释器读取到内存后，按照Unicode编码存储：8个字节。
    v1 = name.encode('utf-8')
    print(v1)# 用utf-8 每个汉字为3个字节
    v2 = name.encode('gbk')
    print(v2)#用bgk为每个汉字2个字节
    ```

  - join

    ``` python
    name = 'alex' #a_l_e_x
    result = "**".join(name)#循环每个元素，并在元素和元素之间假如连接符
    print(result)
    ```

  - 公共功能

    - 索引，获取一个字符。

    - 切片，获取一段字符串（子序列）。

    - 步长

      ``` python
      name = 'alex'
      #val = name[0:-1:2] 
      print(val)
      ```

    - 长度，获取字符长度。

    - for循环

      ``` python
      name = 'alex'
      for item in name:
          print(item)
      ```

      ``` python
      name = 'alex'
      for item in name:
          print(item)
          break
          print('123')
      ```

      ``` python
      for i in range(1,12):
      	if i == 7
          	pass
         	else:
          	print(i)
      ```

      注意：for和while的应用场景：有穷尽优先使用for，无穷尽用while

      5.变量

      6.注释

      7.条件语句

      8.循环语句：while+for+break+continue

      9.运算符

      10.字符串格式化

      - %s
      - %d
      - %%

      11.其他

      - markdown笔记

      - git

        - 本地：git软件【常用命令】
          - git status 查看文件的相关状态
          - git add .
          - git commit -m "提交记录"  进行添加和提交
          - git push origin master当前分支推送到origin主机的对应分支

        - 远程：码云/github（程序员交友平台）

        - 面试相关：

          ​	1.写出你常用的git命令。

          ​	2.你们公司是怎么用git做开发的？

          ​		1.在码云或github等代码托管的网站创建自己的仓库创建完成后码云会给一个地址。

          ​		2.自己写代码。。。

          ​		3.将代码提交到远程仓库

          - ​		初始化

            -  进入一个任意文件夹，如：D：\homework\

            - git init

            - git config "邮箱"

            - git config "姓名"

            - git remote add origin https://gitee.com/........

              注意：至此git已经将D：\homework\目录管理起来，以后此文件夹有任何变化，git都会检测到（使用git status命令可以查看状态）

          - 代码的收集并提交

            - git status
            - git add .
            - git commit -m "记录"
            - git push origin master 将本地D:\homework\下任何文件做操作
              - git status
              - git add .
              - git commit -m "记录"
              - git push origin master 将本地D：\homework\目录下的内容同步到库
            - 【避免】如果远程有本地没有的代码，必须先执行：【可能引发合并问题】
            - git push origin master
            - git status
            - git add .
            - git commit -m "记录"
            - git push origin master 将本地D:\homework\目录下的内容同步到 码云仓
              库。

>>>>>>> 230600839fac0e8063c5b221fbc3af29296937bf
