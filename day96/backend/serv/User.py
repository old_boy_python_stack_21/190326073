from flask import Blueprint, request, jsonify

from Config import MongoDB,RET
from bson.objectid import ObjectId

useragent = Blueprint("user",__name__)

@useragent.route("/reg",methods=["POST"])
def reg():
    user_info = request.form.to_dict()

    if MongoDB.Users.find_one({"username":user_info.get("username")}):
        RET["CODE"] = 1
        RET["MSG"] = "失败"
        return RET

    user_info["avatar"] = "mama.jpg" if user_info["gender"] == "1" else "baba.jpg"
    MongoDB.Users.insert_one(user_info)

    RET["CODE"] = 0
    RET["MSG"] = "注册成功"
    return RET

@useragent.route("/login",methods=["POST"])
def login():
    user_info = request.form.to_dict()
    user_dict = MongoDB.Users.find_one(user_info,{"password":0})
    user_dict["_id"] = str(user_dict.get("_id"))
    print(user_dict)

    RET["CODE"] = 0
    RET["DATA"] = user_dict
    return jsonify(RET)

@useragent.route("/auto_login",methods=["POST"])
def auto_login():
    user_id = request.form.to_dict()
    user_id["_id"] = ObjectId(user_id.get("_id"))
    user_info = MongoDB.Users.find_one(user_id,{"password":0})
    user_info["_id"] = str(user_info.get("_id"))
    RET["DATA"] = user_info
    return jsonify(RET)

@useragent.route("/scan_qr",methods=["POST"])
def scan_qr():
    qr_dict = request.form.to_dict()
    print(qr_dict,type(qr_dict))
    print(qr_dict["device_key"])
    dic = {"username":qr_dict["device_key"]}
    # 假如传来的字典中的device_key中对应的字典ID为0为绑定好友
    if MongoDB.Users.find_one(dic):
        RET["CODE"] = 0
        return jsonify(RET)
    if MongoDB.Content.find_one(dic):
        RET["CODE"] = 0
        return jsonify(RET)
    else:
        RET["CODE"] = 5
        print(5)
        RET["MSG"] = "扫描数据完成"
        return jsonify(RET)