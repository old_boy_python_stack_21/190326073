import json
import os

from flask import Blueprint, send_file
from Config import MongoDB,RET,COVER_PATH,MUSIC_PATH
from bson.objectid import ObjectId

content = Blueprint("content",__name__)

@content.route("/content_list",methods=["POST"])
def get_content():
    content_list = list(MongoDB.Content.find({}))
    for content in content_list:
        content["_id"] = str(content.get("_id"))
    RET["CODE"] = 0
    RET["msg"] = "获取歌曲列表成功"
    RET["data"] = content_list
    return RET


@content.route("/get_cover/<img>",methods=["GET"])
def get_cover(img):
    file_path = os.path.join(COVER_PATH,img)
    print(file_path)
    return send_file(file_path)

@content.route("/get_music/<song>",methods=["GET"])
def get_music(song):
    file_path = os.path.join(MUSIC_PATH,song)
    print(file_path)
    return send_file(file_path)