from flask import Flask,Blueprint
from serv.Content import content
from serv.User import useragent


app = Flask(__name__)
app.debug = True

app.register_blueprint(content)
app.register_blueprint(useragent)




if __name__ == '__main__':
    app.run("0.0.0.0",9527)