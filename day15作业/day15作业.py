# 1.sys.path.append('')的作用？
# 添加指定路径下的模块。
# 2.字符串如何进行反转。
# a = 'abcde'
# b = ''.join(reversed(a))
# print(b)
# c = a[-1::-1]
# print(c)
# 3.不用中间变量交换a,b的值。
# a = 3
# b = 8
# a,b = b,a
# print(a,b)
# 4.*args和**kwargs这俩参数是什么意思？我们为什么要用它。
# *args表示任何多个无名参数，他是一个元组。**kwargs表示关键字参数，他是一个字典。
# 5.函数的参数传递的是地址还是新值。
# 可以传递地址，也可以是新值。
# 6.看代码写结果
# my_dict = {'a':0,'b':1}
# def func(d):
#     d['a'] = 1
#     return d
#
# func(my_dict)
# my_dict['c'] = 2
# print(my_dict)#{'a':1,'b':1，'c':2}
# 7.什么事lambda表达式
# lambda表达式为表示简单的函数且为匿名函数
# 8.range和xrange有什么区别？
# py3下为rangge，执行时直接循环完，py2下为xrange，执行循环时才输出。
# 9.'1,2,3'如何变为['1','2','3']
# a = '1,2,3'
# print(a.split(','))
# 10.['1','2','3'] 如何变成 [1,2,3]
# a = ['1','2','3']
# info = []
# for i in a:
#     i = int(i)
#     info.append(i)
# print(info)
# 11.def f(a,b=[]) 这种写法有什么陷阱？
# 因为b为可变类型，所以b内的数据会随着假如下部数据执行修改时也会修改。
# def f(a, b=[]):
#     b.append(a)
#     return b
# f1 = f(1)
# f2 = f(2,[3])
# f3 = f(4)
# print(f1,f2,f3)
# 12.如何生成列表 [1,4,9,16,25,36,49,64,81,100] ，尽量用一行实现。
# print([i*i for i in range(1,11)])
# 13.python一行print出1~100偶数的列表, (列表推导式, filter均可)
# info = []
# for i in range(1,101):
#     if i%2 == 0:
#         i = str(i)
#         info.append(i)
# print(info)
# info = []
# def func():
#     for i in range(1,101):
#         if i%2==0:
#             info.append(i)
# func()
# print(info)
# b = [i for i in range(1,101) if i%2==0]
# print(b)
# 14.下面函数变为lambda表达式
# def func():
#     result = []
#     for i in range(10):
#         if i % 3 == 0:
#             result.append(i)
#     return result
# result = [i for i in range(10) if i%3==0]
# result = lambda :[i for i in range(10) if i%3==0]
# print(result())
# 15.如何用Python删除一个文件？
# import shutil
# import os
# path_floder = os.path.exists('E:\新建文件夹')
# if path_floder:
#     shutil.rmtree('E:\新建文件夹')
#     print('删除成功')
# else:
#     print('路径不存在')
# 16.如何对一个文件进行重命名？
# import os
# os.rename('新建文件夹','110120')
# 17.python如何生成随机数？
# import random
# def get_random_ocde(length=6):
#     data = []
#     for i in range(length):
#         v = random.randint(65,90)
#         data.append(chr(v))
#     return ''.join(data)
# code = get_random_ocde()
# print(code)
# 18.从0-99这100个数中随机取出10个数，要求不能重复，
# 可以自己设计数据结构。
# import random
# def func():
#     v = []
#     for i in range(100):
#         val = random.randint(0,100)
#         if val not in v:
#             v.append(val)
#             if len(v)==10:
#                 break
#     return v
# b = func()
# print(b)
# 19.用Python实现 9*9 乘法表 （两种方式）
# for i in range(1,10):
#     for j in range(1,i):
#         print('%d*%d=%d\t' %(j,i,i*j),end='')
#     print()
# for i in range(1,10):
#     for j in range(1,item+1):
#         if j == i:
#             print('%s*%s%s'%(i,j,j*i))
#         else:
#             print('%s*%s%s  '%(i,j,j*i),end='')
# 20.请给出下面代码片段的输出并阐述涉及的python相关机制。
# def dict_updater(k,v,dic={}):
#     dic[k] = v
#     print(dic)
# dict_updater('one',1)#{'one':1} 此处是执行时打印为此
# dict_updater('two',2)#{'one':1,'two':2}打印此时添加了此下内容
# dict_updater('three',3,{})#{'three':3} 新定义字典，与上字典不同
# 21.写一个装饰器出来。
# def wrapper(func):
#     def inner(*args,**kwargs):
#         func(*args,**kwargs)
#     return inner
# @wrapper
# def func():
#     print(123)
# func()
# 22.用装饰器给一个方法增加打印的功能
# def wrapper(func):
#     def inner(*args,**kwargs):
#         func(*args,**kwargs)
#     return inner
# @wrapper
# def func():
#     print(123)
# func()
# 23.as 请写出log实现(主要功能时打印函数名)
# import time
# time_now = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
# def log(arg):
#     def inner():
#         arg()
#     return inner
# @log
# def now():
#     print(time_now)
# now()
# 24.向指定地址发送请求，将获取到的值写入到文件中。
# import requests
# import json
# response = requests.get('https://www.luffycity.com/api/v1/course_sub/category/list/')
# v = json.loads(response.text)
# info = v.get('data')
# v1 = []
# for i in info:
#     name_list = i['name']
#     v1.append(name_list)
# with open('catelog.txt',mode='w',encoding='utf-8') as f:
#     f.write('，'.join(v1))
#     print('写入成功')
# 25.经常访问的技术网站和博客
# 博客园、github、csdn
# 26.请列举最近在关注的技术
# 人工智能、网络爬虫、数据分析、python开发
# 27.请列举你认为不错的技术书籍和最近在看的书（不限于技术）
# python基础
