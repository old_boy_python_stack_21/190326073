from django.shortcuts import render,redirect,HttpResponse
from app01 import models

def publisher_list(request):
    all_publishers = models.Publisher.objects.all().order_by('pk')
    return render(request,'publisher_list.html',{'all_publishers':all_publishers})
def add_publisher(request):
    error = ''
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        if models.Publisher.objects.filter(name=publisher_name):
            error = '此出版社名称已经存在'
        if not publisher_name:
            error = '输入不能为空'
        if not error:
            obj = models.Publisher.objects.create(name=publisher_name)
            return redirect('/publisher_list/')
    return render(request,'add_publisher.html',{'error':error})
def del_publisher(request):
    pk = request.GET.get('id')
    obj_list = models.Publisher.objects.filter(pk=pk)
    if not obj_list:
        return HttpResponse('要删除的数据不存在')
    obj_list.delete()
    return redirect('/publisher_list/')
def edit_publisher(request):
    error = ''
    pk = request.GET.get('id')
    obj_list = models.Publisher.objects.filter(pk=pk)
    if not obj_list:
        error = '要编辑的数据不存在'
    obj = obj_list[0]
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        if models.Publisher.objects.filter(name=publisher_name):
            error = '修改的名字已存在'
        if obj.name == publisher_name:
            error = '内容未做修改'
        if not publisher_name:
            error = '输入不能为空'
        if not error:
            obj.name = publisher_name
            obj.save()
            return redirect('/publisher_list/')
    return render(request,'edit_publisher.html',{'obj':obj,'error':error})
# Create your views here.
