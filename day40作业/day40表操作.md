create database day40;

use day40;

create table book(id int primary key auto_increment,书名 char(18),作者 char(18),出版社 char(18),价格 float(6,2),出版日期（publish_date） date);

insert into book(书名,作者,出版社,价格,出版日期（publish_date）) values('倚天屠龙记','egon','北京工业地雷出版社','70','2019-7-1'),('九阳神功','alex','人民音乐不好听出版社','5','2018-7-4')
,('九阴真经','yuan','北京工业地雷出版社','62','2017-7-12')
,('九阴白骨爪','jin','人民音乐不好听出版社','40','2019–8-7')
,('独孤九剑','alex','北京工业地雷出版社','12','2017-9-1') 
,('降龙十巴掌','egon','知识产权没有用出版社','20','2019-7-5') 
,('葵花宝典','yuan','知识产权没有用出版社','33','2019–8-2') ;

- 1查询egon写的所有书和价格

```python
select 书名,作者,价格 from book where 作者='egon';
```

- 2查询最贵的图书价格

```python
select 书名,max(价格) from book;
```

- 3求出所有图书的均价

```python
select 书名,avg(价格) from book;
```

- 4所有图书按照出版日期排序

```python
select * from book order by 出版日期（publish_date）;
```

- 5查询alex写的所有书的平均价格

```python
select 书名,作者,avg(价格) from book where 作者='alex'
```

- 6查询人民音乐不好听出版社的所有图书

```python
select 书名,出版社 from book where 出版社='人民音乐不好听出版社';
```

- 7查询人民音乐不好听出版社的alex写的所有图书和价格

```python
select 书名,作者,出版社,价格 from book where 出版社='人民音乐不好听出版社' and 作者='alex';
```

- 8查出出版社图书均价最高的作者

```python
select 作者,出版社,avg(价格) from book group by 出版社;
```

- 9找出最新出版的图书的作者和出版社

```python
select 作者,出版社,max(出版日期（publish_date）) from book;
```

- 10显示各出版社出版的所有图书

```python
select 书名,作者,出版社 from book group by 书名;
```

- 11查找价格最高的图书，并将它的价格修改为50元

```python
select 书名,max(价格)from book;
update book set 价格=50 where 书名='倚天屠龙记';
```

- 12删除价格最低的那本书对应的数据

```python
select 书名,min(价格) from book;
delete from book where 价格=5;
```

- 13将所有alex写的书作业修改成alexsb

```python
update book set 作者='alexsb' where 作者='alex';
```

- 14select year(出版日期（publish_date）) from book;
- 将所有2017年出版的图书从数据库中删除

```python
delet from book where year(出版日期（publish_date）) =2017;
select id,出版日期（publish_date） from book where year(出版日期（publish_date）) =2017;
delet from book where id=3 or id=5;
```

- 15有文件如下，请使用pymysql写代码讲文件中的数据写入数据库

```python
学python从开始到放弃|alex|人民大学出版社|50|2018-7-1
学mysql从开始到放弃|egon|机械工业出版社|60|2018-6-3
学html从开始到放弃|alex|机械工业出版社|20|2018-4-1
学css从开始到放弃|wusir|机械工业出版社|120|2018-5-2
学js从开始到放弃|wusir|机械工业出版社|100|2018-7-30
insert into book(书名,作者,出版社,价格,出版日期（publish_date）) values('学python从开始到放弃','alex','人民大学出版社','50','2018-7-1'),
('学mysql从开始到放弃','egon','机械工业出版社','60','2018-6-3')
,('学html从开始到放弃','alex','机械工业出版社','20','2018-4-1')
,('学css从开始到放弃','wusir','机械工业出版社','120','2018-5-2')
,('学js从开始到放弃','wusir','机械工业出版社','100','2018-7-30') ;
```

