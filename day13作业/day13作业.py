# 2.请为 func 函数编写一个装饰器，添加上装饰器后可以实现：
# 执行func时，先输入"before"，然后再执行func函数内部代码。
# def wrapper(func):
#     def b():
#         a = input('请输入内容')
#         return func() if a == "before" else None
#     return b
# @wrapper
# def func():
#     return 100+200
# val = func()
# print(val)
# 3.请为 func 函数编写一个装饰器，添加上装饰器后可以实现：
# 执行func时，先执行func函数内部代码，再输出 "after"
# def wrapper(func):
#     def inner():
#         print(func())
#         print('after')
#     return inner
# @wrapper
# def func():
#     return 100+200
# val = func()
# print(val)
# 4.请为以下所有函数编写一个装饰器，添加上装饰器后可以实现：
# 执行func时，先执行func函数内部代码，再输出 "after"
# def wrapper(arg):
#     def inner(*args,**kwargs):
#         print(arg(*args,**kwargs))
#         print('after')
#
#     return inner
# @wrapper
# def func(a1):
#     return a1 + '傻叉'
#
#
# @wrapper
# def base(a1,a2):
#     return a1+a2+ '傻缺'
#
#
# @wrapper
# def base1(a1,a2,a3,a4):
#     return a1+a2+a3+a4+'傻蛋'
#
#
# func('a')
# base('a','b')
# base1('a','b','c','d')
# 5.请为以下所有函数编写一个装饰器，添加上装饰器后可以实现：
# 将被装饰的函数执行5次，讲每次执行函数的结果按照顺序放到列表
# 中，最终返回列表。(为什么循环加到inner外不行？)
# def wrapper(func):
#     def inner():
#         for i in range(5):
#             func_list.append(func())
#     return inner
# func_list = []
#
# import random
# @wrapper
# def func():
#     return random.randint(1,4)
# result = func()
# print(func_list)
# print(result)# 执行5次，并将每次执行的结果追加到列表最终返回给result
# 6.请为以下函数编写一个装饰器，添加上装饰器后可以实现：
# 执行 read_userinfo 函时，先检查文件路径是否存在，如果存在则执行后，
# 如果不存在则 输入文件路径不存在，并且不再执行read_userinfo函数体中的内容，
# 再将 content 变量赋值给None。
# import os
# def func(arg):
#     def inner(*args):
#         result = os.path.exists('E:\刘小辉作业\day13作业')
#         read_userinfo(*args) if result else print('路径不存在')
#     return inner
# @func
# def read_userinfo(path):
#     file_obj = open(path,mode='r',encoding='utf-8')
#     data = file_obj.read()
#     file_obj.close()
#     return data
# # content = read_userinfo('/usr/bin/xxx/xxx')
# read_userinfo()
"""
温馨提示：如何查看一个路径是否存在？
import os
result = os.path.exists('路径地址')

# result为True，则表示路径存在。
# result为False，则表示路径不存在。
"""
# 7.
# CURRENT_USER_STATUS = False
# def wrapper(arg):
#     def inner():
#         if CURRENT_USER_STATUS:
#             return arg()
#         else:
#             print('请登录后查看')
#             return
#     return inner
# @wrapper
# def user_list():
#     """查看用户列表"""
#     for i in range(1, 100):
#         temp = "ID:%s 用户名：老男孩-%s" % (i, i,)
#         print(temp)
#
# def login():
#     """登录"""
#     print('欢迎登录')
#     while True:
#         username = input('请输入用户名（输入N退出）：')
#         if username == 'N':
#             print('退出登录')
#             return
#         password = input('请输入密码：')
#         if username == 'alex' and password == '123':
#             global CURRENT_USER_STATUS
#             CURRENT_USER_STATUS = True
#             print('登录成功')
#             return
#         print('用户名或密码错误，请重新登录。')
# def run():
#     func_list = [user_list, login]
#     while True:
#         print("""系统管理平台
#         1.查看用户列表；
#         2.登录""")
#         index = int(input('请选择：'))
#         if index >= 0 and index < len(func_list)+1:
#             func_list[index-1]()
#         else:
#             print('序号不存在，请重新选择。')
# run()
# 8.看代码写结果
# v = [lambda :x for x in range(10)]
# print(v) #十个lambda函数内存地址组成的列表
# print(v[0])#第一个lambda函数内存地址
# print(v[0]())# 9
# 10.看代码写结果
# data = [lambda x:x*i for i in range(10)] # 新浪微博面试题
# print(data)#十个lambda函数内存地址组成的列表
# print(data[0](2))#18
# print(data[0](2) == data[8](2))#True
# 11.使用推导式
# data_list = [11,22,33,'alex',455,'eric']
# new_data_list = [list(filter(lambda i:type(i) ==int,data_list))]  第一种
# new_data_list = [i for i in data_list if type(i) == int] 第二种
# print(new_data_list)
# 12.使用字典推导式，将列表构造成制定格式字典。
# data_list = [
#     (1,'alex',19),
#     (2,'老男',84),
#     (3,'老女',73)
# ]
# info_list = {i[0]:(i[1],i[2]) for i in data_list}
# print(info_list)

