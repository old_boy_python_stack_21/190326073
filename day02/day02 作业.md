1. 猜数字，设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确，然后退出循环。

   ```python
   while True:
      number = input('请输入数字')
      number = int(number)
      if number > 66:
          print('大了')
      elif number < 66:
          print('小了')
      else:
          print('正确')
          break
   ```

   

2. 在上一题的基础，设置：给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，如果三次之内没有猜测正确，则自动退出循环，并显示‘大笨蛋’。

   ```python
   count = 3
   while count > 0:
       number = input('请输入账号:')
       if number == "66":
           print('正确')
           break
       else:
           count -= 1
           if count !=0:
                  print("密码错误请重新输入！您还有%s次机会" %(count))
   else:
       print("'大笨蛋'")
   ```

   

3. 使用两种方法实现输出 1 2 3 4 5 6   8 9 10 。

   ```python
   ount = 1
   while count <=10:
       if count == 7:
         count = count + 1
         continue
       print(count)
       count = count + 1
   ```

   

4. 求1-100的所有数的和

   ```python
   count = 1
   total = 0
   while count < 101:
       total = total +count
       count += 1
   print(total)
   
   ```

   

5. 输出 1-100 内的所有奇数

   ```python
   count = 1
   while count <=100:
      count = count+2
      print(count)
   ```

   

6. 输出 1-100 内的所有偶数

   ```python
   count = 0
   while count <=100:
      count = count+2
      print(count)
   ```

   

7. 求1-2+3-4+5 ... 99的所有数的和

   ```py
   total = 0
   count = 1
   while count < 100:
       if count % 2 == 0:
           total = total - count  # total -= count
       else:
           total = total + count  # total += count
       count += 1
   print(total)
   ```

8. ⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化） 

   ```python
   # 方式一
   count = 2
   while count >= 0:
       user = input('请输入用户名:')
       pwd = input('请输入密码:')
       if user == 'alex' and pwd == 'wupeiqi':
           print('登录成功')
           break
       else:
           content = "用户名或密码错误，剩余%s次机会。" % (count,)
           count -= 1
    # 方式二
   count = 2
   while True:
       user = input('请输入用户名:')
       pwd = input('请输入密码:')
       if user == 'alex' and pwd == 'wupeiqi':
           print('登录成功')
           break
   
       content = "用户名或密码错误，剩余%s次机会。" % (count,)
       print(content)
       if count == 0:
           break
       count -= 1
   ```

   

9. 简述ASCII、Unicode、utf-8编码

   ascii用八位表示

   Unicode分两种ecs2(至少占两个字节)和ecs4(多个字节)

   utf-8最少是8位,三个字节一个中文

10. 简述位和字节的关系？

    八位等于一个字节

11. 猜年龄游戏 

    

12. 猜年龄游戏升级版
    要求：允许用户最多尝试3次，每尝试3次后，如果还没猜对，就问用户是否还想继续玩，如果回答Y，就继续让其猜3次，以此往复，如果回答N，就退出程序，如何猜对了，就直接退出。

13. 判断下列逻辑语句的True,False
    - 1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
    - not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6

14. 求出下列逻辑语句的值。
    - 8 or 3 and 4 or 2 and 0 or 9 and 7
    - 0 or 2 and 3 and 4 or 6 and 0 or 3

15. 下列结果是什么？
    - 6 or 2 > 1
    - 3 or 2 > 1
    - 0 or 5 < 4
    - 5 < 4 or 3
    - 2 > 1 or 6
    - 3 and 2 > 1
    - 0 and 3 > 1
    - 2 > 1 and 3
    - 3 > 1 and 0
    - 3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 > 2