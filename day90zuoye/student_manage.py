from flask import Flask, render_template, request, session,redirect
from stu import bp
from index import bqi
from login import bql
from settings import DebugConfig,TextConfig
from flask_session import Session
from redis import Redis


STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}
app = Flask(__name__)
app.secret_key = 'hehe'
app.config["SESSION_TYPE"] = "redis"
app.config["SESSION_REDIS"] = Redis(host="192.168.12.60",port=6379,db=10)
app.config.from_object(DebugConfig)
Session(app)

app.register_blueprint(bp)
app.register_blueprint(bqi)
app.register_blueprint(bql)

@app.before_request
def checklogin():
    print(session.get("user"))
    if request.path == '/login':
        return
    if not session.get('user'):
        return redirect('/login')


@app.errorhandler(404)
def error404(errorMessage):
    return render_template('error404.html')



if __name__ == '__main__':
    app.run()