from flask import Blueprint, request, render_template, session

from flask import views

bql = Blueprint('app03',__name__)


class Login(views.MethodView):
    def get(self):
        return render_template('login.html')

    def post(self):
        name = request.form.get("name")
        session['user'] = name
        return "login ok"

bql.add_url_rule("/login",endpoint=None,view_func=Login.as_view(name="login"))