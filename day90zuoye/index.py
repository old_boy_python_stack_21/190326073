from flask import Blueprint

from flask import views

bqi = Blueprint('app02',__name__)

# @bqi.route('/')
# def index():
#     return 'index'

class Home(views.MethodView):
    def get(self):
        return "home page"

bqi.add_url_rule("/",view_func=Home.as_view(name="home"))