from django.shortcuts import render,redirect,HttpResponse

from django.views import View
from app01 import models
# Create your views here.
class Login(View):
    def get(self,request,*args,**kwargs):
        return render(request,'login.html')
    def post(self,request,*args,**kwargs):
        username = request.POST.get('username')
        pwd = request.POST.get('pwd')
        print(username,pwd)
        return_url = request.GET.get('return_url')
        if username == 'hei' and pwd == '123':
            ret = redirect(return_url)
            ret.set_signed_cookie('is_login','123','mm')
            return ret
        return render(request,'login.html',{'error':'账户或密码错误'})


def login_required(func):
    def inner(request,*args,**kwargs):
        is_login = request.get_signed_cookie('is_login',salt='mm',default='')
        url = request.path_info
        if is_login != '123':
            return redirect('/login/?return_url={}'.format(url))
        ret = func(request,*args,**kwargs)
        return ret
    return inner


@login_required
def index(request):

    return render(request,'index.html')

def logout(request):
    ret = redirect('/login/')
    ret.delete_cookie('is_login')
    return ret