from django.template import Library
register = Library()

@register.inclusion_tag("menu.html")
def sql_list(num):

    return {'num':range(1,num+1)}