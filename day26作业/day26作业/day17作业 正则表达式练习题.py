# 正则表达式练习
# 1、匹配一篇英文文章的标题 类似 The Voice Of China
# import re
# name_title = 'The Voice Of China'
# ret = re.findall('[A-Z][a-z]*\s?',name_title)
# print(ret)
# 2、匹配一个网址
    # 类似 https://www.baidu.com http://www.cnblogs.com
# import re
# inp = 'https://www.baidu.com'
# ret = re.findall('[https://]{1}w{3}\.\w*\.(com|cn|org)',inp)
# print(ret)
# 3、匹配年月日日期 类似 2018-12-06 2018/12/06 2018.12.06
# import re
# inp = '2018-12-06'
# ret = re.findall('\d{4}(\.|\/|\s|\-){1}([1][0-2]|[1-9]){1}(\.|\/|\s|\-){1}([0][1-9]|[1]\d|[2]\d|[3][0-2]){1}',inp)
# print(ret)
# 4、匹配15位或者18位身份证号
# [1-9]\d{5}[1][9]\d{2}([0][1-9]|[1][0-2]){1}([0][1-9]|[1]\d|[2]\d|[3][0-2]){1}\d{3}(\d|x|X){1}
# 5、从lianjia.html中匹配出标题，户型和面积，结果如下：
# [('金台路交通部部委楼南北大三居带客厅   单位自持物业', '3室1厅', '91.22平米')
# , ('西山枫林 高楼层南向两居 户型方正 采光好', '2室1厅', '94.14平米')]
import requests,re
with open('E:/刘小辉作业/day26作业/day26作业/lianjia.html',mode='r',encoding='utf-8') as f:
    ret = f.read()
# print(ret)
pattern = '" data-sl="">(?P<name>.*?)</a>.*?</span>(?P<mj>.*?)<span class="divide">.*?'
par = re.compile(pattern,flags=re.S)
a = par.findall(ret)
for i in a:
    b = i.group('name')

# print(a)
# with open('list.txt',mode='w',encoding='utf-8') as f1:
#     for i in a:
#         f1.write(i)

# 6、1-2*((60-30+(-40/5)*(9-2*5/3+7/3*99/4*2998+10*568/14))-(-4*3)/(16-3*2))
# 从上面算式中匹配出最内层小括号以及小括号内的表达式


# 7、从类似9-2*5/3+7/3*99/4*2998+10*568/14的表达式中匹配出乘法或除法

# 8、通读博客，完成三级菜单
# http://www.cnblogs.com/Eva-J/articles/7205734.html

# 9、大作业：计算器