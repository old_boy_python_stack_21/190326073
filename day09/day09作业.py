#2.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回。
# def get_list(i):
#     a = i[1::2]
#     return a
# print(get_list([1,1,2,3,5,8,13,21,34]))
# 3.写函数，判断用户传入的一个对象（字符串或列表或元组任意）长度是否大于5，并返回真假。
# def num_list(a):
#     v = True if len(a) > 5 else False
#     return v
# print(num_list([1,1,2,3,5,8,13,21,34,]))
# 4.写函数，接收两个数字参数，返回比较大的那个数字。
# def info(a,b):
#     v = a if a > b else b
#     return v
# print(info(7,6))
# 5.写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。
# 用户通过输入这四个内容，然后将这四个内容传入到函数中，
# 此函数接收到这四个内容，将内容根据"*"拼接起来并追加到一个student_msg文件中。
# def info(name,gender,age,ed):
#     with open('student_msg.txt',mode='w',encoding='utf-8') as f:
#         a1 = [name,gender,age,ed]
#         a2 = '*'.join(a1)
#         f.write(a2)
#     print(a2)
# name = input('name:')
# gender = input('gender:')
# age = input('age:')
# ed = input('ed:')
# info(name,gender,age,ed)
# 6.写函数，在函数内部生成如下规则的列表  [1,1,2,3,5,8,13,21,34,55…]（斐波那契数列），并返回。
# 注意：函数可接收一个参数用于指定列表中元素最大不可以超过的范围。
# def info(num):
#     v = [1,1]
#     for i in range(2,num):
#         v.append(v[i-1]+v[i-2])
#     return v
# print(info(13))



# 7.写函数，验证用户名在文件 data.txt 中是否存在，如果存在则返回True，否则返回False。
# （函数有一个参数，用于接收用户输入的用户名）
# data.txt文件格式如下：
# 1|alex|123123
# 2|eric|rwerwe
# 3|wupeiqi|pppp


# def cc(a):
#     with open('data.txt', mode='r', encoding='gbk') as f:
#         data = f.read()
#         v = data.split('\n')
#     for i in v:
#         val = i.split('|')
#         b = False
#         if a in val:
#             b = True
#             break
#     return b
#
# a = input('输入内容：')
# print(cc(a))


