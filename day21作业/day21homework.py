# 1.列举你了解的面向对象中的特殊成员，并为每个写代码示例。
# init
# class Foo:
#     def __init__(self,name):
#         self.name = name
# f = Foo('alex')
# print(f.name)
# new
# class Foo(object):
#     def __init__(self,name):
#         self.name = name
#         print(1)
#     def __new__(cls, *args, **kwargs):
#         pass
#         print(*args, **kwargs)
#         return object.__new__(cls)
# Foo('alex')
# call
# class Foo:
#     def __call__(self, *args, **kwargs):
#         print(123)
#         print(*args)
# Foo()('110')
# getitem setitem delitem
# class Foo:
#     def __setitem__(self, key, value):
#         print('set')
#     def __getitem__(self, item):
#         print('get')
#     def __delitem__(self, key):
#         print('del')
# obj = Foo()
# obj['name'] = 'alex'
# val = obj['name']
# print(val)
# del obj['name']
# str
# class Foo:
#     def __str__(self):
#         print('打印了')
#         return 'alex'
# f = Foo()
# print(f)
# dict
# class Foo:
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
# f = Foo('alex',20)
# val = f.__dict__
# print(val)
# 上下文管理
# class Foo:
#     def __enter__(self):
#         self.wenzi = open('xiyouji',mode='a',encoding='utf-8')
#         return self.wenzi
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.wenzi.close()
# with Foo() as f:
#     f.write('haha')
# 两个对象相加
# class Foo:
#     def __add__(self, other):
#         return 1
# class Moo:
#     def __add__(self, other):
#         return 1
# a = Foo()
# b = Moo()
# print(a+b,a,b)
# 2.看代码写结果
# class Foo(object):
#     def __init__(self, age):
#         self.age = age
#     def display(self):
#         print(self.age)
# data_list = [Foo(8), Foo(9)]
# for item in data_list:
#     print(item.age, item.display()) #8  8 None 9 9 None
# 3.看代码写结果
# class Base(object):
#     def __init__(self, a1):
#         self.a1 = a1
#     def f2(self, args):
#         print(self.a1, args)
# class Foo(Base):
#     def f2(self, arg):
#         print('666')
# obj_list = [Base(1), Foo(2), Foo(3)]
# for obj in obj_list:
#     obj.f2(0) #1 0 666 666
# 4.看代码写结果
# class StarkConfig(object):
#     def __init__(self,num):
#         self.num = num
#     def changelist(self,request):
#         print(self.num,request)
# class RoleConfig(StarkConfig):
#     def changelist(self,request):
#         print('666')
# config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
# for item in config_obj_list:
#     print(item.num) #1  2  3
# 5.看代码写结果
# class StarkConfig(object):
#     def __init__(self,num):
#         self.num = num
#     def changelist(self,request):
#         print(self.num,request)
# class RoleConfig(StarkConfig):
#     pass
# config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
# for item in config_obj_list:
#     item.changelist(168) #1 168   2  168   3  168
# 6.看代码写结果
# class StarkConfig(object):
#     def __init__(self,num):
#         self.num = num
#     def changelist(self,request):
#         print(self.num,request)
# class RoleConfig(StarkConfig):
#     def changelist(self,request):
#         print(666,self.num)
# config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
# for item in config_obj_list:
#     item.changelist(168)  #1  168    2   168   666 3
# 7.看代码写结果
# class StarkConfig(object):
#     def __init__(self,num):
#         self.num = num
#     def changelist(self,request):
#         print(self.num,request)
#     def run(self):
#         self.changelist(999)
# class RoleConfig(StarkConfig):
#     def changelist(self,request):
#         print(666,self.num)
# config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
# config_obj_list[1].run()  #1  999
# config_obj_list[2].run()  #666 3
# 8.看代码写结果
# class StarkConfig(object):
#     def __init__(self,num):
#         self.num = num
#     def changelist(self,request):
#         print(self.num,request)
#     def run(self):
#         self.changelist(999)
# class RoleConfig(StarkConfig):
#     def changelist(self,request):
#         print(666,self.num)
# class AdminSite(object):
#     def __init__(self):
#         self._registry = {}
#     def register(self,k,v):
#         self._registry[k] = v
# site = AdminSite()
# print(len(site._registry)) #0
# site.register('range',666)
# site.register('shilei',438)
# print(len(site._registry))#2
# site.register('lyd',StarkConfig(19))
# site.register('yjl',StarkConfig(20))
# site.register('fgz',RoleConfig(33))
# print(len(site._registry))#5
# print(site._registry)#{'range':666,'shilei':438,'lyd':StarkConfig(19),'yjl':StarkConfig(20),'fgz':RoleConfig(33)}
# 9.看代码写结果
# class StarkConfig(object):
#     def __init__(self,num):
#         self.num = num
#     def changelist(self,request):
#         print(self.num,request)
#     def run(self):
#         self.changelist(999)
# class RoleConfig(StarkConfig):
#     def changelist(self,request):
#         print(666,self.num)
# class AdminSite(object):
#     def __init__(self):
#         self._registry = {}
#     def register(self,k,v):
#         self._registry[k] = v
# site = AdminSite()
# site.register('lyd',StarkConfig(19))
# site.register('yjl',StarkConfig(20))
# site.register('fgz',RoleConfig(33))
# print(len(site._registry)) # 3
# for k,row in site._registry.items():
#     row.changelist(5) #19  5 / 20  5 / 666 33
# 10.看代码写结果
# class StarkConfig(object):
#     def __init__(self,num):
#         self.num = num
#     def changelist(self,request):
#         print(self.num,request)
#     def run(self):
#         self.changelist(999)
# class RoleConfig(StarkConfig):
#     def changelist(self,request):
#         print(666,self.num)
# class AdminSite(object):
#     def __init__(self):
#         self._registry = {}
#     def register(self,k,v):
#         self._registry[k] = v
# site = AdminSite()
# site.register('lyd',StarkConfig(19))
# site.register('yjl',StarkConfig(20))
# site.register('fgz',RoleConfig(33))
# print(len(site._registry)) # 3
# for k,row in site._registry.items():
#     row.run() #19   999 /  20  999  / 666  33
# 11.看代码写结果
# class UserInfo(object):
#     pass
# class Department(object):
#     pass
# class StarkConfig(object):
#     def __init__(self,num):
#         self.num = num
#     def changelist(self,request):
#         print(self.num,request)
#     def run(self):
#         self.changelist(999)
# class RoleConfig(StarkConfig):
#     def changelist(self,request):
#         print(666,self.num)
# class AdminSite(object):
#     def __init__(self):
#         self._registry = {}
#     def register(self,k,v):
#         self._registry[k] = v(k)
# site = AdminSite()
# site.register(UserInfo,StarkConfig)
# site.register(Department,StarkConfig)
# print(len(site._registry)) #2
# for k,row in site._registry.items():
#     row.run()  #StarkConfig(UserInfo) 999 /  StarkConfig(Department) 999
# 12.看代码写结果
# class F3(object):
#     def f1(self):
#         ret = super().f1()
#         print(ret)
#         return 123
# class F2(object):
#     def f1(self):
#         print('123')
# class F1(F3, F2):
#     pass
# obj = F1()
# obj.f1()  #‘123’ None
# 13.看代码写结果
# class Base(object):
#     def __init__(self, name):
#         self.name = name
# class Foo(Base):
#     def __init__(self, name):
#         super().__init__(name)
#         self.name = "于大爷"
# obj1 = Foo()#需传参数
# print(obj1.name)
# obj2 = Base()#需传参数
# print(obj2.name)
# 14.看代码写结果
# class Base(object):
#     pass
# class Foo(Base):
#     pass
# obj = Foo()
# print(type(obj) == Foo)  #True
# print(type(obj) == Base)  #False
# print(isinstance(obj,Foo))  #True
# print(isinstance(obj,Base))  #True
# 15.看代码写结果
# class StarkConfig(object):
#     def __init__(self, num):
#         self.num = num
#     def __call__(self, *args, **kwargs):
#         print(self.num)
# class RoleConfig(StarkConfig):
#     def __call__(self, *args, **kwargs):
#         print(self.num)
# v1 = StarkConfig(1)
# v2 = RoleConfig(11)
# v1() #1
# v2() #11
# 16.看代码写结果
# class StarkConfig(object):
#     def __init__(self, num):
#         self.num = num
#     def run(self):
#         self()
#     def __call__(self, *args, **kwargs):
#         print(self.num)
# class RoleConfig(StarkConfig):
#     def __call__(self, *args, **kwargs):
#         print(345)
#     def __getitem__(self, item):
#         return self.num[item]
# v1 = RoleConfig('alex')
# v2 = StarkConfig("wupeiqi")
# print(v1[1])  #l  ##RoleConfig('alex')[1]
# print(v2[2])  #不能执行       ##StarkConfig("wupeiqi")[2]
# 17.补全代码
# class Context(object):
#     def __enter__(self):
#         self.obj = open('xiyouji',mode='a',encoding='gbk')
#         return self.obj
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.obj.close()
# with Context() as ctx:
#     ctx.write('题真多')
# 18.补全代码
# class Stack(object):
#     def __init__(self):
#         self.data_list = []
#
#     def push(self, val):
#         self.data_list.insert(3,val)
#
#     def pop(self):
#         self.data_list.pop()
# obj = Stack()
# # 调用push方法，将数据加入到data_list中。
# obj.push('alex')
# print(obj.data_list)
# obj.push('武沛齐')
# print(obj.data_list)
# obj.push('金老板')
# print(obj.data_list)
# # 调用pop讲数据从data_list获取并删掉，注意顺序(按照后进先出的格式)
# v1 = obj.pop()  # 金老板
# print(obj.data_list)
# v2 = obj.pop()  # 武沛齐
# print(obj.data_list)
# v3 = obj.pop()  # alex
# print(obj.data_list)
# 请补全Stack类中的push和pop方法，将obj的对象维护成 后进先出 的结构。
# 19.如何主动触发异常
# try:
#     print('123')
#     raise Exception('wenti')
# except Exception as e:
#     print(e)
# 20.看代码写结果
# def func(arg):
#     try:
#         int(arg)
#     except Exception as e:
#         print('异常')
#     finally:
#         print('哦')
# func('123') # 哦
# func('二货') # 异常  哦