# 1.使用面向对象实现先进先出。
# class Foo:
#     data = []
#     def apd(self,name):
#         self.data.append(name)
#     def pip_d(self):
#         a = self.data.pop(0)
# f = Foo()
# for i in range(3):
#     f.apd(i)
# print(f.data)
# f.pip_d()
# print(f.data)
# 2.使用面向对象实现先进后出。
# class Foo:
#     data = []
#     def apd(self,name):
#         self.data.append(name)
#     def pip_d(self):
#         a = self.data.pop()
# f = Foo()
# for i in range(3):
#     f.apd(i)
# print(f.data)
# f.pip_d()
# print(f.data)
# 3.如何实现可迭代对象
# 在类中实现__iter__方法并返回。
# 4.看代码写结果
# class Foo(object):
#
#     def __init__(self):
#         self.name = '武沛齐'
#         self.age = 100
#
#
# obj = Foo()
# setattr(Foo, 'email', 'wupeiqi@xx.com')
#
# v1 = getattr(obj, 'email')
# v2 = getattr(Foo, 'email')
#
# print(v1, v2) #wupeiqi@xx.com wupeiqi@xx.com
# 5.请补充代码（提：循环的列表过程中如果删除列表元素，
# 会影响后续的循环，推荐：可以尝试从后向前找）
# class Nlist:
#     li = ['李杰','女神','金鑫','武沛齐','李1']
#     def name_del(self):
#         name = input('请输入要删除的姓氏：')
#         for i in self.li:
#             if name in i:
#                 self.li.remove(i)
# n = Nlist()
# n.name_del()
# print(n.li )
# 6.有如下字典，请删除指定数据。
# class User(object):
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
# info = [User('武沛齐', 19), User('李杰', 73), User('景女神', 16)]
# name = input('请输入要删除的用户姓名：')
# for i in info:
#     if name in i.name:
#         info.remove(i)
# print(info)
# 7.
# class User(object):
#     def __init__(self, name, email, age):
#         self.name = name
#         self.email = email
#         self.age = age
#     def __str__(self):
#         return self.name
# class School(object):
#     def __init__(self):
#         # 员工字典，格式为：{"销售部": [用户对象,用户对象,] }
#         self.user_dict = {}
#     def invite(self, department, user_object):
#         if self.user_dict.get(department) == None:
#             self.user_dict[department] = []
#             self.user_dict[department].append(user_object)
#             print('新添加用户成功')
#         else:
#             for i in self.user_dict[department]:
#                 if self.name in i:
#                     print('用户已存在')
#                 self.user_dict[department].append(user_object)
#                 print('增加用户成功')
#     def dimission(self, username, department=None):
#         for m,i in self.user_dict.items():
#             for k in i:
#                 if username in k:
#                     i.remove(k)
#                     print('删除成功')
#     def run(self):
#         while True:
#             print('请输入要选择的序号：\n'
#                   '1.查看部门人员\n'
#                   '2.添加部门成员\n'
#                   '3.删除部门成员')
#             num = input('请输入要选择的序号(n退出)')
#             if type(num) == str:
#                 if num.upper() == 'N':
#                     break
#             if num.isdecimal():
#                 if num == '1':
#                     print(self.user_dict)
#                 if num == '2':
#                     name = input('请输入姓名：')
#                     email = input('请输入邮箱：')
#                     age = input('请输入年龄')
#                     i = User(name,email,age)
#                     user_object = [i.name, i.email, i.age]
#                     department = input('请输入部门')
#                     self.invite(department,user_object)
#                 if num == '3':
#                     # department = input('请输入要删除的人员部门(不确定可不输入)：')
#                     username = input('请输入要删除的人员姓名：')
#                     self.dimission(username, department=None)
#                 else:
#                     print('请输入1或2或3')
#             else:
#                 print('请输入数字')
#
# if __name__ == '__main__':
#     obj = School()
#     obj.run()
# 8.
MIDDLEWARE_CLASSES = [
    'utils.session.SessionMiddleware',
    'utils.auth.AuthMiddleware',
    'utils.csrf.CrsfMiddleware',
]

from wsgiref.simple_server import make_server

class View(object):
    def login(self):
        return '登陆'

    def logout(self):
        return '退出'

    def index(self):
        return '首页'


def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    obj = View()
    method_name = environ.get('PATH_INFO').strip('/')
    if not hasattr(obj,method_name):
        return ["".encode("utf-8"),]
    response = getattr(obj,method_name)()
    return [response.encode("utf-8")  ]


server = make_server('127.0.0.1', 8000, func)
server.serve_forever()