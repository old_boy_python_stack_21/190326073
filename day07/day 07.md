# day 07

## 今日内容

- 深浅拷贝(面试)
- 文件操作

## 内容回顾&补充

各数据类型下的使用功能成为：方法

1.内容回顾

- 计算机基础
- 编码
- 语法
  - if
  - while
  - for
- 数据类型
- range/type/id
- 运算符  and  or

2.面试题

1.公司线上的系统用的什么？centos

2.Python2跟Python3的区别

- 默认解释器编码
- 输入输出
- 整数的除法/int/int long

3.运算符

```python
v = 1 or 0 and 8 or 9
print(v)
```

4.is 和==的区别？

​	is比较数据内存地址的区别，==比较数据是否相同

5.列举Python的数据类型中都有哪些方法？

## 今日内容

### 1.深浅拷贝

- 浅拷贝：只拷贝第一层

- 深拷贝：拷贝嵌套层次中的所有可变类型

- 特殊情况

  ```python
  v1 = (1,2,3,5,6)
  import copy
  v2 = copy.copy(v1)
  print(id(v1),id(v2))
  v3 = copy.deepcopy(v1)
  print(id(v1),id(v3))
  #若元组内含有列表，地址id会改变
  
  ```



### 2.文件操作

- 打开
  - r，只能读。【**】
  - w，只能写，写之前清空【**】
  - a，只能追加【*】
  - r+
    - 读：默认从0的位置开始读，也可以通过seek调整光标的位置
    - 写：从光标所在的位置开始写。也可以从国seek调整光标的位置
  - w+
    - 读：默认光标永远在写入的最后或0的位置，可以通过seek调整光标位置
    - 写：先清空
  - a+ 
    - 读：默认光标在最后，也可以通过seek调整光标位置
    - 写：永远写到最后
- 操作
  - read
    - read
    - read(2)#字符
    - 
  - write
- 关闭

![1554354373537](C:\Users\xiaohui\AppData\Roaming\Typora\typora-user-images\1554354373537.png)

![1554354701849](C:\Users\xiaohui\AppData\Roaming\Typora\typora-user-images\1554354701849.png)



今日总结：

- 深浅拷贝
- 文件操作
  - 打开
  - 读写
  - 关闭
- 文件操作和数据类型的结合使用

作业

1.思维导图（文件。pdf）

2.数据类型。（单个文件，数据类型及其使用方法加案例。md）

3.从入学到现在所有的作业题做一遍。（单个文件。py）

4.今日作业（单个文件。py）