#1.看代码写结果
#v1 = [1,2,3,4,5]
# v2 = [v1,v1,v1]
#
# v1.append(6)
# print(v1)  [1,2,3,4,5,6]
# print(v2)  [[1,2,3,4,5,6],[1,2,3,4,5,6],[1,2,3,4,5,6]]
# 2.看代码写结果
# v1 = [1,2,3,4,5]
# v2 = [v1,v1,v1]
#
# v2[1][0] = 111
# v2[2][0] = 222
# print(v1)  #[222,2,3,4,5]
# print(v2)  #[[222,2,3,4,5],[222,2,3,4,5],[222,2,3,4,5]]
# 3.看代码写结果
# v1 = [1,2,3,4,5,6,7,8,9]
# v2 = {}
#
# for item in v1:
#     if item < 6:
#         continue #当小于6时continue
#     if 'k1' in v2:
#         v2['k1'].append(item) #{对键为'k1'对应得值做列表并循环添加
#     else:
#         v2['k1'] = [item ] #对字典v2进行定义，
# print(v2) #{'k1':[6,7,8,9]}
# 4.简述深浅拷贝
#浅拷贝：对数据进行拷贝，只拷贝第一层可变或不可变数据。 深拷贝：对数据嵌套层次中的可变类型数据进行拷贝。
#5.看代码写结果
# import copy
#
# v1 = "alex"
# v2 = copy.copy(v1)
# v3 = copy.deepcopy(v1)
#
# print(v1 is v2) #True
# print(v1 is v3) #True
# 6.看代码写结果
# import copy
#
# v1 = [1,2,3,4,5]
# v2 = copy.copy(v1)
# v3 = copy.deepcopy(v1)
#
# print(v1 is v2) #False
# print(v1 is v3) #False
# 7.看代码写结果
# import copy
#
# v1 = [1,2,3,4,5]
#
# v2 = copy.copy(v1)
# v3 = copy.deepcopy(v1)
#
# print(v1[0] is v2[0]) #True
# print(v1[0] is v3[0]) #True
# print(v2[0] is v3[0]) #True
# 8.看代码写结果
# import copy
#
# v1 = [1,2,3,4,5]
#
# v2 = copy.copy(v1)
# v3 = copy.deepcopy(v1)
#
# print(v1[0] is v2[0]) #True
# print(v1[0] is v3[0]) #True
# print(v2[0] is v3[0]) #True
# 9.看代码写结果

# 第二次浅拷贝
# import copy
#
# v1 = [1,2,3,{"name":'武沛齐',"numbers":[7,77,88]},4,5]
#
# v2 = copy.copy(v1)
#
# print(v1 is v2) #False
#
# print(v1[0] is v2[0]) #True
# print(v1[3] is v2[3]) #True
#
# print(v1[3]['name'] is v2[3]['name']) #True
# print(v1[3]['numbers'] is v2[3]['numbers']) #True
# print(v1[3]['numbers'][1] is v2[3]['numbers'][1]) #True
# 10.看代码写结果
# 第一次深拷贝
# import copy
#
# v1 = [1,2,3,{"name":'武沛齐',"numbers":[7,77,88]},4,5]
#
# v2 = copy.deepcopy(v1)
#
# print(v1 is v2) #False
#
# print(v1[0] is v2[0]) #True
# print(v1[3] is v2[3]) #False
#
# print(v1[3]['name'] is v2[3]['name']) #True
# print(v1[3]['numbers'] is v2[3]['numbers']) #False
# print(v1[3]['numbers'][1] is v2[3]['numbers'][1]) #True
# 11.简述文件的打开模式
# 打开模式有 read读取  write写入 append添加 r+ w+ a+。
# 12.请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
# info = ['骗子','不是','说','只有',"10",'个题吗？']
# data = '_'.join(info)
# print(data)
# file_object = open('readme.txt',mode='w',encoding='utf-8')
# file_object.write(data)
# file_object.close()
# 13.请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
# info = ['骗子，','不是','说','只有',10,'个题吗？']
# for i in range(len(info)):
#     info[i] = str(info[i])
# data = '_'.join(info)
# file_object = open('a.txt',mode='w',encoding='utf-8')
# file_object.write(data)
# file_object.close()
# 14.请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
# info = {'name':'骗子','age':18,'gender':'性别'}
# v = []
# for i in info:
#     v.append(i)
# d = '_'.join(v)
# f = open('readme1.txt',mode='w',encoding='utf-8')
# f.write(d)
# f.close()
#请将info中的所有值 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
# info = {'name':'骗子','age':18,'gender':'性别'}
# b = []
# f = open('readme3.txt',mode='w',encoding='utf-8')
# for v1 in info.values():
#     a = "%s" % (v1,)
#     b.append(a)
# c = '_'.join(b)
# f.write(c)
# f.close()
# 请将info中的所有键和值按照 "键|值,键|值,键|值" 拼接起来并写入到文件 "readme.txt" 文件中。
# info = {'name':'骗子','age':18,'gender':'性别'}
# b = []
# f = open('readme2.txt',mode='w',encoding='utf-8')
# for v1,v2 in info.items():
#     a = "%s|%s" %(v1,v2)
#     b.append(a)
# c = '_'.join(b)
# f.write(c)
# f.close()
# 15.写代码
file_object = open('data.txt',mode="r",encoding='utf-8')
content = file_object.read()
file_object.close()
con_list = content.strip().split('\n')
info = []
for i in range(len(con_list)):
    dic = {}
    con_list[i] = con_list[i].strip()
    dic['name'] = con_list[i].split('|')[0]
    dic['pwd'] = con_list[i].split('|')[1]
    dic['eml'] = con_list[i].split('|')[2]
    info.append(dic)
print(info)