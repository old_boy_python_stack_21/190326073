# 1.计算任意一个文件夹的大小（考虑绝对路径的问题）
    # 基础需求
        # 这个文件夹中只有文件
    # 进阶需求
        # 这个文件夹中可能有文件夹，并且文件夹中还可能有文件夹...不知道有多少层
# import sys,os,shutil
# print(sys.path[0])
# s = os.walk(sys.path[0])
# total = 0
# for a,b,c in s:
#     v2 = os.path.getsize(a)
#     total += v2
# print(total)
# 2.校验大文件的一致性
    # 基础需求
        # 普通的文字文件
    # 进阶需求
        # 视频或者是图片
# import hashlib
# md5 = hashlib.md5()
# md6 = hashlib.md5()
# with open('a.txt',mode='r',encoding='utf-8') as f1:
#     for line in f1:
#         md5.update(line.encode())
#     md6.update(f1.read().encode())
# print(md5.hexdigest(),md6.hexdigest())


# 3.发红包
    # 每一个人能够抢到的金额的概率都是平均的
    # 小数的不准确性的问题
# import random
# for i in range(20):
#     pass
# x = [1,2,3,4]
# v = random.choice(x)
# v2 = random.sample(x,3)
# random.shuffle(x)
# print(v,v2,x)