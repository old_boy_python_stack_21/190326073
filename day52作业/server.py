from wsgiref.simple_server import make_server
def login(url):
    with open('login.html','r',encoding='gbk') as f:
        info = f.read()
    return bytes(info,encoding='utf-8')
list1 = [
    ("/login/", login),
]
def run_server(environ,start_response):
    start_response('200 OK',[('Content-Type','text/html;charset=utf8'),])
    url = environ['PATH_INFO']
    func = None
    for i in list1:
        if i[0] == url:
            func = i[1]
            break
    if func:
        response = func(url)
    else:
        response = b"404 not found!"
    return [response,]
if __name__ == '__main__':
    httpd = make_server('127.0.0.1',9000,run_server)
    print('start')
    httpd.serve_forever()