from django.db import models
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
class Book(models.Model):
    title = models.CharField(max_length=32)
    pud = models.ForeignKey('Publisher',on_delete=models.CASCADE)
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book')


# Create your models here.
