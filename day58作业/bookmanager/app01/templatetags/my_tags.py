from django.template import Library
register = Library()

@register.inclusion_tag("menu.html")
def sql_list(x):
    sql_list = []
    for i in range(1, x + 1):
        dict = {}
        dict['key'] = i
        dict['text'] = '%s的平方是%s' % (i, i ** i)
        sql_list.append(dict)
    return {"sql_list":sql_list}