from django.shortcuts import render,redirect,HttpResponse
from app01 import models

def publisher_list(request):
    all_publishers = models.Publisher.objects.all().order_by('pk')
    return render(request,'publisher_list.html',{'all_publishers':all_publishers,'base':'base.html'})
def add_publisher(request):
    error = ''
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        if models.Publisher.objects.filter(name=publisher_name):
            error = '此出版社名称已经存在'
        if not publisher_name:
            error = '输入不能为空'
        if not error:
            obj = models.Publisher.objects.create(name=publisher_name)
            return redirect('/publisher_list/')
    return render(request,'add_publisher.html',{'error':error})
def del_publisher(request):
    pk = request.GET.get('id')
    obj_list = models.Publisher.objects.filter(pk=pk)
    if not obj_list:
        return HttpResponse('要删除的数据不存在')
    obj_list.delete()
    return redirect('/publisher_list/')
def edit_publisher(request):
    error = ''
    pk = request.GET.get('id')
    obj_list = models.Publisher.objects.filter(pk=pk)
    if not obj_list:
        error = '要编辑的数据不存在'
    obj = obj_list[0]
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        if models.Publisher.objects.filter(name=publisher_name):
            error = '修改的名字已存在'
        if obj.name == publisher_name:
            error = '内容未做修改'
        if not publisher_name:
            error = '输入不能为空'
        if not error:
            obj.name = publisher_name
            obj.save()
            return redirect('/publisher_list/')
    return render(request,'edit_publisher.html',{'obj':obj,'error':error})
# Create your views here.
def book_list(request):
    all_books = models.Book.objects.all().order_by('pk')
    return render(request,'book_list.html',{'all_books':all_books,'base':'base.html'})
def add_book(request):
    if request.method == 'POST':
        book_name = request.POST.get('book_name')
        pub_id = request.POST.get('pub_id')
        print(book_name,pub_id)
        models.Book.objects.create(title=book_name,pud_id=pub_id)
        return redirect('/book_list/')
    all_publishers = models.Publisher.objects.all()
    return render(request,'add_book.html',{'all_publishers':all_publishers})
def del_book(request):
    pk = request.GET.get('id')
    print(pk,2)
    models.Book.objects.filter(pk=pk).delete()
    return redirect('/book_list/')
def edit_book(request):
    pk = request.GET.get('id')
    book_obj = models.Book.objects.get(pk=pk)
    if request.method == 'POST':
        book_name = request.POST.get('book_name')
        pub_id = request.POST.get('pub_id')
        book_obj.title = book_name
        book_obj.pud = models.Publisher.objects.get(pk=pub_id)
        book_obj.save()
        return redirect('/book_list/')
    all_publishers = models.Publisher.objects.all()
    return render(request,'edit_book.html',{'book_obj': book_obj,'all_publishers':all_publishers})

def author_list(request):
    #查询所有的作者
    all_authors = models.Author.objects.all()
    return render(request,'author_list.html',{'all_authors':all_authors,'base':'base.html'})
def add_author(request):
    error = ''
    if request.method == 'POST':
        author_name = request.POST.get('author_name')
        print(models.Author.objects.filter(name=author_name))
        if models.Author.objects.filter(name=author_name):
            error = '修改的名字已存在'
        if not error:
            books = request.POST.getlist('books')
            author_obj = models.Author.objects.create(name=author_name)
            author_obj.books.set(books)
            return redirect('/author_list/')
    all_authors = models.Author.objects.all()
    all_books = models.Book.objects.all()
    return render(request,'add_author.html',{'all_authors':all_authors,'all_books':all_books,'error':error})
def del_author(request):
    #获取要删除对象的id
    #获取要删除的对象
    #跳转到展示页面
    pk = request.GET.get('pk')
    models.Author.objects.filter(pk=pk).delete()
    return redirect('/author_list/')
def edit_author(request):
    #查询要编辑的作者对象
    #查询所有的数据
    pk = request.GET.get('pk')
    author_obj = models.Author.objects.get(pk=pk)

    if request.method == 'POST':
        author_name =request.POST.get('author_name')
        books = request.POST.getlist('books')
        author_obj.name = author_name
        author_obj.save()
        author_obj.books.set(books)
        return redirect('/author_list/')
    all_books = models.Book.objects.all()
    return render(request,'edit_author.html',{'all_books':all_books,'author_obj':author_obj})





