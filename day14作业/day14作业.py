# 1.为函数写一个装饰器，在函数执行之后输入 after
# def wrapper(func):
#     def inner():
#         func()
#         print('after')
#     return inner
# @wrapper
# def func():
#     print(123)
#
# func()
# 2.为函数写一个装饰器，把函数的返回值 +100 然后再返回。
# def wrapper(func):
#     def inner():
#         a = func() + 100
#         return a
#     return inner
#
# @wrapper
# def func():
#     return 7
#
# result = func()
# print(result)
# 3.为函数写一个装饰器，根据参数不同做不同操作。
# flag为True，则 让原函数执行后返回值加100，并返回。
# flag为False，则 让原函数执行后返回值减100，并返回
# def x(counter):
#     def wrapper(func):
#         def inner():
#             if counter:
#                 data = func()
#             else:
#                 data = func()
#             return data
#         return inner
#     return wrapper
# @x(True)
# def f1():
#     return 11
#
# @x(False)
# def f2():
#     return 22
#
# r1 = f1()
# r2 = f2()
# print(r1,r2)
# 4.写一个脚本，接收两个参数。
# 第一个参数：文件
# 第二个参数：内容
# 将第二个参数中的内容写到文件（第一个参数中）
# 执行脚本 python  test.py  old.txt
# def test_py():
# with open('oldboy.txt',mode='w',encoding='utf-8') as f2:
#     with open('test.py',mode='rb') as f1:
#         for line in f1:
#             a = str(line)
#             f2.write(a)
# 5.递归的最大次数是多少？
# 1000次
# 6.看代码写结果
# print("你\n好") #你（换行）好
# print("你\\n好")#你\n好
# print(r"你\n好")#你\n好
# 7.写函数实现查看一个路径下的所有文件。
# import os
# result = os.walk(r'E:\刘小辉作业\day14作业')
# for a,b,c in result:
#     for item in c:
#         path = os.path.join(a,item)
#         print(path)
# 8.写代码
# import os
# path = r'E:\刘小辉作业\day14作业'
# result = os.walk(path)
# for a,b,c in result:
#     for item in c:
#         path1 = os.path.join(a,item)
#         print(path1)
# 9.写代码实现【题目1】和【题目2】
# def func(arg):
#     def inner(*args,**kwargs):
#         c= arg(*args,**kwargs)
#         return c
#     return inner
# @func
# def arg(a,b):
#     b +=a
# a = arg(1,2)
# print(a)
# 题目二
# dicta = {'a':'1','b':'2','c':'3','d':'4','f':'hello'}
# dictb = {'b':'3','d':'5','e':'7','m':'9','k':'world'}
# dictc = {}
# for i in dicta:
#     if i not in dictb:
#         dictc[i] = dicta[i]
#     else:
#         dictc[i] = int(dicta[i]) + int(dictb[i])
# for i in dictb:
#     if i not in dicta:
#         dictc[i] = dictb[i]
# print(dictc)
# 10.看代码写结果
# def extendList(val,list=[]):
#     list.append(val)
#     return list
# list1 = extendList(10)
# list2 = extendList(123)
# list3 = extendList('a')
# print(list1,list2,list3) #[10,123,'a] [10,123,'a] [10,123,'a]
# 11.实现以下面试题
# (1) b
# (2) 写出过程
# tuplea = ('a','b','c','d','e')
# tupleb = (1,2,3,4,5)
# res = {}
# for i in range(5):
#     res[tuplea[i]] = tupleb[i]
# print(res)
# (3)python 代码获取命令行参数：
# import sys
# import shutil
# sys.arg = r'[E:\刘小辉作业\day14作业\新建文件夹,E:\110.txt]'
# path = sys.arg[1]
# shutil.retree(path)
# （4）将ip写到列表
# ip = '192.168.0.100'
# v = ip.split('.')
# print(v)
# (5)实现转换
# A = ['a','b','c']
# print(','.join(A[0] +A[1]+A[2]))
# (6)获取字符
# a = '1234A6FAjkljljljlkjklk90'
# print(a[len(a)-2:])
# print(a[1:3])
# 7.通过列表获得列表，去重
# v = [1,2,3,1,2,3,1,3]
# val = []
# for i in v:
#     if i not in val:
#         val.append(i)
# print(val)
# 编程题
# 1.
# path_name = 'E:\刘小辉作业\day14作业\新建文件夹'
# def wrapper(func):
#     def inner(path_name):
#         func(path_name)
#     return inner
# import os
# def func(path_name):
#     v = os.path.abspath(path_name)
#     result = os.walk(v)
#     for a,b,c in result:
#         for item in c:
#             path = os.path.join(a,item)
#             print(path)
# func(path_name)
# 2.完美数

# 3.多进程读取两个文件

# 4.


