# day06数据类型（四）

## 今日内容

- 集合
- 内存相关
- 深浅拷贝

## 内容回顾&补充

1.内容回顾

2.补充

- 列表

  - reverse，反转

    ```Python
    v1 = [1,3,9,5,8,6,4]
    print(v1)
    v1.reverse()
    print(v1) #对v1列表的内容进行反转
    ```

  - sort ,对数据进行排序

    ```python
    v1 = [5,6,9,8,3,1]
    v1.sort(reverse=True) #排序自大到小
    v1.sort(reverse=False)#排序自大到小
    print(v1)
    ```

- 字典

  - keys/values/items

  - get  获取字典中的指定键的值

    ```python
    info ={'k1':'v1', 'k2':'v2', 'k3':'v3'}
    #v = info['k111'] 假如字典中键没有与之相对应将会报错
    #v = info.get('k111')  假如字典中没有与之对应的将会输出none
    #v = info.get('k1',333)假如字典中没有将会返回333，假如有就输出对应的值
    print(v)
    
    ```

  - pop

    ```python
    info = {'k1':'v1','k2':'v2'}
    result = info.pop('k2') #删除k2，并定义一个新的result
    print(info,result)
    del info[k1] #删除与k1对应的键值对
    ```

  - update 

    ```python
    info = {'k1':'v1','k2':'v2'}
    a = {'k3':'v3'}
    info.update(a)#对数据a更新值info，若有重复的，则重复的内容不更新
    print(info)
    ```

- 判断一个字符串是否有敏感词

  - str

    ```python
    v = 'Python全栈21期'
    while True:
        a = input('输入内容：')
        if a in v:
            print('含敏感')
        else:
            print('zhenguqe')
            break
    ```

  - list/tuple

    ```python
    v = ['alex','oldboy','藏老四','利奇航']
    a = input('输入内容：')
    #方法一
    for item in v:
        if item == a:
            print('敏感')
     #方法二       
    if a in v:
        print('敏感')
    ```

  - dict

    ```python
    v = {'k1':'v1','k2':'v2','k3':'v3'}
    while True:
    #方式一：循环判断
        #if 'x' in v:
        #pass#判断x是否在v内有没有存在 默认是键
        flag = '不存在'
        a = input('输入内容：')
        for b in v.values():
            if a == b:
                flag ='存在'
        print(flag) #循环判断是否存在
    #方式二：强制转换成列表
    if 'v2' in list(v.values())
    	pass
    #判断键值对k2:v2是否在字典内
    value = v.get('x')
    if value == 'v2':
    	print('cunzai')
    else:
        print('no')
    ```

  - 练习题

    ```python
    #让用户输入信息判断是否有敏感字符
    char_list = ['adad', 'bhfg' ,'hio']
    content = input('请输入信息：')
    for i in char_list:
        if i in content:
            print('敏感')
    ```

    

## 内容详细

### 1.集合set

- 无序

- 无重复

  ```python
  #各数据类型的特点
  '''
  none
  int
  	v1 = 123
  	v1 = int() -->0
  bool
  	v2 = True/False
  	v2 = bool() -->False
  str
  	v3 = ''
  	v3 = str()
  list
      v4 = []
      v4 = list()
  tuple
      v5 = ()
      v5 = tuple()
  dict
      v6 = {}
      v6 = dict()
  set
      v7 = set()
  '''
  ```

  1.集合独有功能

  - add 添加
  - discard 删除
  - update 上传更新
  - intersection  交集
  - union 并集
  - difference 差集
  - symmetric_difference

  2.公共功能

  - len

    ```python
    v = [1,2,3,6]
    print(len(v))
    ```

  - for 循环

    ```python
    v = [1,6,5,9,5]
    for item in v:
        print(item)
    ```

  - 索引【无】

  - 步长【无】

  - 切片【无】

  - 删除【无】

  - 修改【无】

  3.嵌套问题

  ```python
  #1.字典/列表/集合 -》不能放在集合中+不能作为字典的key（unhashable）
  # info = {1, 2, 3, 4, True, "国风", None, (1, 2, 3)}
  # print(info)
  # 2. hash -> 哈希是怎么回事？
  # 因为在内部会将值进行哈希算法并得到一个数值（对应内存地址），以后用于快速查找。
  
  # 3. 特殊情况
  # info = {0, 2, 3, 4, False, "国风", None, (1, 2, 3)}
  # print(info)
  
  # info = {
  #     1:'alex',
  #     True:'oldboy'
  # }
  # print(info)
  ```

  

### 2.内存相关

- 示例一

  ```python
  v1 = [11,22,33]
  v2 = [11,22,33]
  
  v1 = 666
  v2 = 666
  
  v1 = "asdf"
  v2 = "asdf"
  
  # 按理 v1 和 v2 应该是不同的内存地址。特殊：
  1. 整型：  -5 ~ 256 
  2. 字符串："alex",'asfasd asdf asdf d_asdf '       ----"f_*" * 3  - 重新开辟内存。
  ```

- 示例二

  ```python
  v1 = [11,22,33,44]
  v1 = [11,22,33]
  ```

- 示例三

  ```python
  v1 = [11,22,33]
  v2 = v1 
  
  # 练习1 (内部修改)
  v1 = [11,22,33]
  v2 = v1 
  v1.append(666)
  print(v2) # 含 666
  
  # 练习2：（赋值）
  v1 = [11,22,33]
  v2 = v1 
  v1 = [1,2,3,4]
  print(v2)
  
  # 练习3：(重新赋值)
  v1 = 'alex'
  v2 = v1
  v1 = 'oldboy'
  print(v2)
  ```

- 示例四

  ```python
  v = [1,2,3]
  values = [11,22,v]
  
  # 练习1：
  """
  v.append(9)
  print(values) # [11,22,[1,2,3,9]]
  """
  # 练习2：
  """
  values[2].append(999)
  print(v) # [1, 2, 3, 999]
  """
  # 练习3：
  """
  v = 999
  print(values) # [11, 22, [1, 2, 3]]
  """
  # 练习4：
  values[2] = 666
  print(v) # [1, 2, 3]
  ```

- 查看内存地址

  ```python
  v1 = [1,2,3]
  v2 = v1
  v1.append(999)
  print(v1,v2) #v1跟v2同时添加999
  print(id(v1),id(v2))
  ```

- 问题：==和is的区别

  - ==用于比较值是否相同。

  - is比较内存地址是否相同。

    

