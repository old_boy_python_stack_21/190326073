#1.列举你了解的字典中的功能（字典独有）。
#keys/values/items/get/update
#2.列举你了解的集合中的功能（集合独有）
#add添加 discard 删除  update 更新 intersection交集 union并集 difference 对比不同 symmetric_difference
#3.列举你了解的可以转换为 布尔值且为False的值。
#数字0，空元组，空字符串，空列表，空字典,None
#4.请用代码实现- 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出info = {'name':'王刚蛋','hobby':'铁锤'}。
#- 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出（如果key不存在，则获取默认“键不存在”，并输出）。
  #注意：无需考虑循环终止（写死循环即可）
info = {'name':'王刚蛋','hobby':'铁锤'}
print('若要查询姓名输入：name 爱好输入：hobby')
flag = '不存在'
while True:
    key1 = input('请输入您的选择：')
    for a in info.keys():
        if a == key1:
            flag = '存在'
    print(flag)
#5.请用代码验证 "name" 是否在字典的键中？
info = {'name': '王刚蛋', 'hobby': '铁锤', 'age': '18'}
v = '不在'
for i in info.keys():
    if i == 'name':
        v = '在'
print(v)
#6.请用代码验证 "alex" 是否在字典的值中？
info = {'name': '王刚蛋', 'hobby': '铁锤', 'age': '18'}
result = '不在'
for i in info.values():
    if i == 'alex':
        result = '在'
print(result)
#7.- 请得到 v1 和 v2 的交集并输出,请得到 v1 和 v2 的并集并输出,请得到 v1 和 v2 的 差集并输出,请得到 v2 和 v1 的 差集并输出
v1 = {'武沛齐', '李杰', '太白', '景女神'}
v2 = {'李杰', '景女神','shazi'}
v3 = v1.intersection(v2)
v4 = v1.union(v2)
v5 = v1.difference(v2)
v6 = v2.difference(v1)
print(v3)
print(v4)
print(v5)
print(v6)
#8.循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
li = []
while True:
    a = input('请输入要添加的姓名：')
    if a == 'n':
        break
    elif a == 'N':
        break
    li.extend(a)
    print(li)
#9.循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
set = set()
while True:
    b = input('请输入要添加的姓名：')
    if b == 'n':
        break
    elif b == 'N':
        break
    set.add(b)
    print(set)
#10.写代码实现
v1 = {'alex','武sir','肖大'}
v2 = []
while True:
    v3 = input('请输入姓名信息：')
    if v3 in v1:
        v2.append(v3)
    if v3 == 'n' or v3 == 'N':
        break
    else:
        v1.add(v3)
    print(v1,v2)
#11.判断以下值那个能做字典的key ？那个能做集合的元素？
#可以做字典的key可以做集合的元素的有(可哈希)：1、-1、‘’、None、（1，）
#12.is 和==的区别：==是数据相等相同，is是存储的地址相同。
#13.type使用方式及作用。
# type的作用可以查看数据的类型。使用方法是print(type(数据)).
#14.id的使用方式及作用？
#作用是查看数据的储存地址。print(id(数据))
#15.看代码写结果并解释原因
#v1 = {'k1':'v1','k2':[1,2,3]}
v2 = {'k1':'v1','k2':[1,2,3]}

#result1 = v1 == v2
#result2 = v1 is v2
#print(result1) 结果是True,此项是对数据进行比较，数据是相同的，所以是true
#print(result2)False，此项是对数据存储地址进行比较，因一般数据即使数据相同，数据存储的地址也是不同的。
#16.看代码写结果并解释原因
#v1 = {'k1':'v1','k2':[1,2,3]}
#v2 = v1

#result1 = v1 == v2
#result2 = v1 is v2
#print(result1) true因为v2=v1所以两项数据是相同的。
#print(result2) true因为v2=v1所以两项使用的数据及存储地址是相同的。
#17.看代码写结果并解释原因
#v1 = {'k1':'v1','k2':[1,2,3]}
#v2 = v1

#v1['k1'] = 'wupeiqi'
#print(v2)  {'k1':'wupeiqi','k2':[1,2,3]}数据v2=v1 ,v1['k1']是对k1进行从新赋值。
#18.看代码写结果并解释原因
#v1 = '人生苦短，我用Python'
#v2 = [1,2,3,4,v1]

#v1 = "人生苦短，用毛线Python"

#print(v2) 结果[1,2,3,4,'人生苦短，我用Python']，因为下面的v1是对其进行从新定义，按顺序执行下来v2是未发生变化
#19.看代码写结果并解释原因
#info = [1,2,3]
#userinfo = {'account':info, 'num':info, 'money':info}

#info.append(9)
#print(userinfo)    {'account':[1,2,3], 'num':[1,2,3], 'money':[1,2,3]}

#info = "题怎么这么多"
#print(userinfo)   {'account':"题怎么这么多", 'num':"题怎么这么多", 'money':"题怎么这么多"}
#下面的对数据进行从新定义，上面的作废
#20.看代码写结果并解释原因
#info = [1,2,3]
#userinfo = [info,info,info,info,info]
#info[0] = '不仅多，还特么难呢'
#print(info,userinfo)  ['不仅多，还特么难呢',2,3]，[['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3],、、、、]
#对info部分内容进行从新赋值。
#21.看代码写结果并解释原因
#info = [1,2,3]
#userinfo = [info,info,info,info,info]

#userinfo[2][0] = '闭嘴'
#print(info,userinfo)  [1,2,3]，[[1,2,3],[1,2,3],['闭嘴',2,3],[1,2,3],[1,2,3]]
#从新对userinfo内容赋值与info无关
#22.看代码写结果并解释原因
#info = [1, 2, 3]
#user_list = []
#for item in range(10):
    #user_list.append(info)

#info[1] = "是谁说Python好学的？"

#print(user_list)[[1, "是谁说Python好学的？", 3], [1, "是谁说Python好学的？", 3]、、、、]
#对info从新进行赋值
#23.看代码写结果
#data = {}
#for i in range(10):
    #data['user'] = i
#print(data)  {'date':9}
#字典内键值相同的只能存在一个，值为最终的那个
#24.看代码写结果
#data_list = []
#data = {}
#for i in range(10):
    #data['user'] = i
    #data_list.append(data)
#print(data)  {'user':9}
#date_list无作用
#25.看代码写结果
#data_list = []
#for i in range(10):
    #data = {}
    #data['user'] = i
# data_list.append(data)
#print(data) 'user':9}
#date_list无作用，不影响date数据