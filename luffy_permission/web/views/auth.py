from django.shortcuts import render,redirect,HttpResponse,reverse
from rbac import models
from django.conf import settings
from rbac.service.init_permission import init_permission
def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        obj = models.User.objects.filter(username=username,password=password)
        if not obj:
            return render(request,'login.html',{'error':'用户名或密码错误'})
        obj = obj[0]
        # print(obj)
        # print(obj.roles.all().values())
        init_permission(request,obj)
        request.session.set_expiry(0) #设置session失效时间，整数是多少秒后失效，0是关闭浏览器后失效，日期会在填写日期后失效
        return redirect('index')
    return render(request,'login.html')

def index(request):
    return render(request,'index.html')

def logout(request):
    request.session.delete()
    return redirect('login')