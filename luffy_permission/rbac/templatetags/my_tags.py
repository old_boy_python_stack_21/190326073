from django import template
from django.conf import settings
from collections import OrderedDict
register = template.Library()


@register.inclusion_tag('menu.html')
def menu(request):
    menu_dict = request.session.get(settings.MENU_SESSION_KEY)
    # print(menu_dict)
    # print(menu_dict.values())
    url = request.path_info
    od = OrderedDict()
    keys_list = sorted(menu_dict,key=lambda x:menu_dict[x]['weight'],reverse=True)
    for key in keys_list:
        od[key] = menu_dict[key]
    for i in menu_dict.values():
        i['class'] = 'hide'
        for m in i['children']:
            if request.current_menu_id == m['id']:
                m['class'] = 'active'
                i['class'] = ''
    # print(od)
    # print(od.values())
    return {'menu_list':od.values()}


@register.inclusion_tag('rbac/breadcrumb.html')
def breadcrumb(request):
    breadcrumb_list = request.breadcrumb_list
    return {'breadcrumb_list':breadcrumb_list}


@register.filter
def has_permission(request,name):
    permission_dict = request.session.get(settings.PERMISSION_SESSION_KEY)
    # print(permission_dict)
    if name in permission_dict:
        return True