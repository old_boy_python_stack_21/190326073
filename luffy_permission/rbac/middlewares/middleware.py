from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import reverse,redirect,HttpResponse
from django.conf import settings
import re


class AuthMiddleWare(MiddlewareMixin):

    def process_request(self,request):
        #获取当前访问的URL
        path = request.path_info
        request.current_menu_id = None
        request.breadcrumb_list = [
            {'title':'首页','url':'/index/'}
        ]
        #白名单
        for url in settings.WHITE_LIST:
            if re.match(url,path):
                return
        is_login = request.session.get('is_login')
        if not is_login:
            return redirect('login')
        #登录状态校验
        #免登陆校验
        #获取权限信息
        #权限校验
        for url in settings.NO_PERMISSION_LIST:
            if re.match(url, path):
                return
        permissions = request.session.get(settings.PERMISSION_SESSION_KEY)
        # print(permissions)
        for permission in permissions.values():
            if re.match(permission['url'],path):
                id = permission['id']
                pid = permission['pid']
                if pid:
                    request.current_menu_id = pid
                    p_permission = permissions[permission['pname']]
                    request.breadcrumb_list.append({'url': p_permission['url'], 'title': p_permission['title']})
                    request.breadcrumb_list.append({'url': permission['url'], 'title': permission['title']})
                else:
                    request.current_menu_id = id
                    request.breadcrumb_list.append({'url':permission['url'],'title':permission['title']})
                return
        return HttpResponse('无权限查看')