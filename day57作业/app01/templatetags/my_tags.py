from django import template
register = template.Library()
@register.filter
def add_t(value,arg):
    return value+arg
@register.filter
def sub_s(value,arg):
    return value-arg
@register.filter
def mul(value,arg):
    return value*arg
@register.filter
def div(value,arg):
    return value/arg
@register.filter
def show_a(value,arg):
    return '<a href="{}">{}</a>'.format(value,arg)