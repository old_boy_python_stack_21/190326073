1.查询男生女生人数

```python
select gender,count(gender) from student where gender='男';
select gender,count(gender) from student where gender='女';
select gender,count(*) from student group by gender;
```

2.查询姓张的学生名单

```python
select * from student where sname like '铁%';
```

3.程科平均分从高到低

```python
select * from score group by corse_id order by avg(number);
select sid,cname,number from score inner join course on course.cid=score.corse_id  group by corse_id order by avg(number);
```

4.查询所有课程成绩小于60分的同学的学号，姓名

```python
select t.sid,sname,gender,number from score s inner join student t on s.sid=t.sid where number<=60;
```

5.查询至少有一门课程与学号为1的同学所学课程相同的同学的学号和姓名

```python
select * from student where sid=1 or class_id=1 having class_id=1
select * from student where sid=1 or class_id=1 having class_id=class_id
```

6.查询出只选修了一门课程的全部学生的学号和姓名；

```python
select sname,class_id from score inner join student on student.sid=student_id group by student_id having count(corse_id)=1;
```

7.查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；

```python
select corse_id,max(number),min(number) from score inner join student on student.sid=student_id group by corse_id;
```

8.查询课程编号“2”的成绩比课程编号“1”课程低的所有同学的学号、姓名；xx

```python
select student_id,corse_id,number from score where corse_id=2;
select student_id,corse_id,number from score where corse_id=1;



select sid,sname from student inner join (select s2.student_id from (select student_id,corse_id,number from score where corse_id=2) as s2 inner join (select student_id,corse_id,number from score where corse_id=1) as s1 on s1.student_id=s2.student_id where s2.number<s1.number) as f on student.sid = f.student_id;
```

9.查询“生物”课程比“物理”课程成绩高的所有学生的学号；xxx

```python
select cid where cname='生物';
select cid where cname='物理';

select student_id,corse_id,number from score where corse_id=1;as shengwu;
select student_id,corse_id,number from score where corse_id=3;as wuli;
select student_id,number from score where corse_id=1;shengwu 
select student_id,number from score where corse_id=3;wuli

select student.sname from student inner join (select s3.student_id from (select student_id,number from score where corse_id=1) as s1 inner join (select student_id,number from score where corse_id=3) as s3 on s1.student_id=s3.student_id where s1.number>s3.number) as f on student.sid=f.student_id;
```

10.查询平均成绩大于60分的同学的学号和平均成绩;

```python
select student.sname,f.avg_num from student inner join (select * from (select student_id,avg(number) avg_num from score group by student_id) as s1 where s1.avg_num>60) as f on student.sid=f.student_id;

```

11.查询所有同学的学号、姓名、选课数、总成绩；

```python
select student_id,count(corse_id),sum(number) from score group by student_id;
select student.sname,f.选课数,f.总分 from student inner join (select student_id,count(corse_id) 选课数,sum(number) 总分 from score group by student_id) as f on student.sid=f.student_id;
```

12.查询姓“李”的老师的个数；

```python
select tname,count(tname) from teacher where tname like '李%';
```

13.查询没学过“张磊老师”课的同学的学号、姓名；

```python
select tid from teacher where tname='藏獒';
select cid from course where teacher_id=(select tid from teacher where tname='藏獒');
select * from score where corse_id not in (select cid from course where teacher_id=(select tid from teacher where tname='藏獒'));
select student.sid,student.sname from student inner join (select * from score where corse_id not in (select cid from course where teacher_id=(select tid from teacher where tname='藏獒'))) as f on student.sid=f.student_id;
```

14.查询学过“1”并且也学过编号“2”课程的同学的学号、姓名；xxxxx

```python
select * from score where corse_id=1;
select student_id from score where corse_id=1;
select student_id from score where corse_id=2;
select student_id from score where student_id in (select student_id s1 from score where corse_id=1,select student_id s2 from score where corse_id=2);
```

15.查询学过“李平老师”所教的所有课的同学的学号、姓名；

```python
select course.cid from course inner join teacher on course.teacher_id=teacher.tid where teacher.tname='藏獒';
select student.sid,student.sname from student inner join score on student.sid=score.student_id where score.corse_id=2 ; 
select student.sid,student.sname from student inner join score on student.sid=score.student_id where score.corse_id in (select course.cid from course inner join teacher on course.teacher_id=teacher.tid where teacher.tname='藏獒') group by student.sname; 

```





