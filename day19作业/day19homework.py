# 1.简述编写类和执行类中方法的流程。
# 面向过程：分析解决问题所需要的步骤，按照步骤一步步的完成。
# 和面向对象：把需要解决的问题分解成各个类（对象）。
# 2.面向对象的三大特性：封装、继承、多态。
# 3.函数改成类并调用
# class Aaa:
#     def func(self, a1):
#         print(a1)
# v = Aaa()
# v.func('110')
# 4.面向对象中的self指的是类的本身
# 5.以下代码体现面向对象的什么特点? 封装的特性，可重用，存储些内容方便以后用
# class Person:
#     def __init__(self, name, age, gender):
#         self.name = name
#         self.age = age
#         self.gender = gender
# obj = Person('武沛齐', 18, '男')
# 6.以下代码体现面向对象的什么特点? 封装的特性，可重用
# class Message:
#     def email(self):
#         """
#         发送邮件
#         :return:
#         """
#         pass
#
#     def msg(self):
#         """
#         发送短信
#         :return:
#         """
#         pass
#
#     def wechat(self):
#         """
#         发送微信
#         :return:
#         """
#         pass
# 7.看代码写结果   foo.func   None
# class Foo:
#     def func(self):
#         print('foo.func')
# obj = Foo()
# result = obj.func()
# print(result)
# 8.定义个类，其中有计算周长和面积的方法
# (圆的半径通过参数传递到初始化方法)。
# class Circular:
#     def area(self,r):
#         print('面积是%s平米'%(3.14*r**2,))
#         return r**2
#     def r_length(self,r):
#         print('周长是%s米' %(3.14*r*2))
#         return 3.14*r
# p = Circular()
# a = int(input('请输入半径(m)：'))
# areas = p.area(a)
# lengths = p.r_length(a)
# 9.面向对象中为什么要有继承?
# 增加数据的重用性，方便使用，减少数据的重复修改。
# 10.Python继承时，查找成员的顺序遵循 么规则?
# 优先从本类中查找，若无依次向父级查找。
# 11.看代码写结果
# class Base1:
#     def f1(self):
#         print('base1.f1')
#     def f2(self):
#         print('base1.f2')
#     def f3(self):
#         print('base1.f3')
#         self.f1()
# class Base2:
#     def f1(self):
#         print('base2.f1')
# class Foo(Base1, Base2):
#     def f0(self):
#         print('foo.f0')
#         self.f3()
# obj = Foo()
# obj.f0() #foo.f0   base1.f3  base1.f1
# 12.看代码写结果
# class Base:
#     def f1(self):
#         print('base.f1')
#     def f3(self):
#         self.f1()
#         print('base.f3')
# class Foo(Base):
#     def f1(self):
#         print('foo.f1')
#     def f2(self):
#         print('foo.f2')
#         self.f3()
# obj = Foo()
# obj.f2() #foo.f2   foo.f1  base.f3
# 13.写代码
# class Print_3:
#     def func(self):
#         user_list = []
#         while True:
#             ZT = False
#             user = input('请输入用户名:')
#             pwd = input('请输入密码:')
#             email = input('请输入邮箱:')
#             if '@' in email:
#                 ZT = True
#             if ZT:
#                 item = {'user': user, 'pwd': pwd, 'email': email}
#                 user_list.append(item)
#                 a = len(user_list)
#                 print('添加成功，已添加%s个' % a)
#                 if len(user_list) == 3:
#                     break
#             if not ZT:
#                 print('邮箱格式错误从新输入')
#         for i in user_list:
#             print(i)
#         print('成功打印三个')

# 14.写代码实现 户注册和登录。
class User:
    def __init__(self, name, pwd):
        self.name = name
        self.pwd = pwd
class Account:
    def __init__(self):
        # 用户列表，数据格式：[user对象，user对象，user对象]
        self.user_list = []
        self.login_zt = False
    def login(self):
        while True:
            name = input('输入账户名N退出：')
            if name == 'N' or name == 'n':
                break
            pwd = input('输入密码：')
            for i in self.user_list:
                if i['name'] == name and i['pwd'] == pwd:
                    self.login_zt = True
            if self.login_zt:
                print('登录成功')
                break
            if not self.login_zt:
                print('账户或密码错误请从新输入')
                continue
        self.run()
    def register(self):

        while True:
            name = input('输入注册名：')
            for i in self.user_list:
                if i.get('name') == name:
                    print('账户名已存在请从新输入')
                    continue
            pwd = input('输入密码：')
            item = {'name':name,'pwd':pwd}
            self.user_list.append(item)
            print('注册成功')
            break
        self.run()
    def run(self):
        item = [self.register, self.login]
        while True:
            print('请选择要选择的序号：\n'
                  '1.注册账户\n'
                  '2.登录账户\n'
                  '3.阅读内容')
            choice_num = int(input('输入要选择的序号：'))
            if choice_num == 3 and self.login_zt == True:
                print('阅读内容')
                return
            if choice_num == 3 and self.login_zt == False:
                print('未登录，登录后可阅读')
                continue
            if not 0 < choice_num < 4:
                print('输入错误请从新输入')
                continue
            else:
                item[choice_num-1]()


if __name__ == '__main__':
    obj = Account()
    obj.run()

