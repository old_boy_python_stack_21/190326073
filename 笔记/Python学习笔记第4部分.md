### CRM项目

crm：customer relationsship management客户关系管理系统

### 第一部分（登录、注册）：

#### 1.分析业务需求，对功能进行划分

- 销售：

1. 客户信息的管理

   展示   新增   编辑   删除

2. 跟进记录的管理

   展示   新增   编辑  删除

3. 报名记录的管理

   展示   新增   编辑  删除

4. 缴费记录的管理

   展示   新增   编辑  删除

班主任

1. 班级的管理

   展示   新增   编辑  删除

2. 课程的管理

   day66

3. 学习记录的管理

   id     stu_id    cou_id 

#### 2.设计表结构

1. 用户表
2. 客户表
3. 跟进记录表
4. 报名记录表
5. 缴费记录表
6. 班级表
7. 课程记录表
8. 学习记录表

#### 3.创建Django项目

1.配置settings文件

DATABASES配置连接数据库

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME':'crm',#库名
        'HOST':'127.0.0.1',#ip地址
        'PORT':3306,#端口
        'USER':'root',#用户
        'PASSWORD':'123',#密码
    }
}
```

`__init__`文件中设置

```python
import pymysql
pymysql.istall_as_MySQLdb()
```

设置静态文件路径别名static

```python
STATIC_URL = '/static/'   # 别名
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static1'),
    os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'static2'),
]
#默认按照STATICFILES_DIRS列表的顺序进行查找。
```

```python
<link rel="stylesheet" href="/static/css/login.css">   # 别名开头
```

设置路由分发urlpatterns

```python
from django.contrib import admin
from django.conf.urls import url,include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^crm/', include('crm.urls')),
]
```

```python
#新建urls文件设置分发地址
from django.conf.urls import url
from crm import views#从

urlpatterns = [
    url(r'^login/', views.login, name='login'),
    url(r'^reg/', views.reg, name='reg'),
    url(r'^index/', views.index, name='index'),
]
```

```python
#设置name可进行反向解析是使用或在模板中使用
{% url 'home' %}
#在views函数中使用
from django.urls import reverse

return reverse('index',args=("2018", ))
```

创建form文件使用form组件:

Form的使用

```python
from django import forms


class RegForm(forms.Form):
    username = forms.CharField()
    pwd = forms.CharField()
```

实例化并return给视图

```python
form_obj = RegForm()
form_obj = RegForm(request.POST)
form_obj.is_valid():  # 对数据进行校验
print(form_obj.cleaned_data)  # 通过校验的数据

return render(request, 'reg2.html', {'form_obj': form_obj})
```

模板使用

```python
{{ form_obj.as_p }}    __>   生产一个个P标签  input  label
{{ form_obj.errors }}    ——》   form表单中所有字段的错误
{{ form_obj.username }}     ——》 一个字段对应的input框
{{ form_obj.username.label }}    ——》  该字段的中午提示
{{ form_obj.username.id_for_label }}    ——》 该字段input框的id
{{ form_obj.username.errors }}   ——》 该字段的所有的错误
{{ form_obj.username.errors.0 }}   ——》 该字段的第一个错误的错误
```

Django中使用admin

```python
在Django中terminal下 执行命令：
python manage.py createsuperuser
输入要穿件的名及密码 密码八位必须含字符串数字
python manage.py runserver 运行

```

```python
在app下的admin文件下注册要使用的表
from app import models
admin.site.register(models.表名)
```

修改全局时间格式

```python
在settings中修改
USE_L10N = False
DATEFORMAT = 'Y-m-d'
```

### 第二部分（客户信息展示）

#### 1.展示客户信息

1.普通展示：对象.字段名

2.有choice参数：

​	对象.字段名-----》数据库的数据

​	`对象.get_字段名_display()` -----》数据库的数据对象的中文提示

3.外键：

​	对象.外键 -----》在外键对象下定义该字段的`__str__`方法

```python
def __str__(self):
    return self.字段名

#使用时   对象.外键.字段
```

4.自定义方法：

多对多：

```python
def show_class(self):
    return ' '.join([str(i) for i in self.class_list.all()])
#使用str(i)是对i字段下定义了`__str__`方法会将外键的值打印出来，或者使用i.外键字段也可以
```









第N部分  RBAC 基于角色权限控制

1.创建新app，执行命令

```python
1.python manage.py startapp rbac#app名rbac
2.python manage.py createsuperuser#创建超级用户方便创建表密码不少于八位数字及字母
3.注册app
INSTALLED_APPS = ['rbac.apps.RbacConfig']
```

2.设置用户与权限之间的关系，在models中创建类

```python
from django.db import models

class Permission(models.Model):
    """权限表"""
    url = models.CharField('url地址',max_length=64)
    title = models.CharField('标题',max_length=32)
    def __str__(self):
        return self.title#设置__str__显示要显示的标题

class Role(models.Model):
    """角色表"""
    name = models.CharField('角色名称',max_length=32)
    permissions = models.ManyToManyField('Permission',verbose_name='角色拥有全新',blank=True)
    def __str__(self):
        return self.name
class User(models.Model):
    """用户表"""
    username = models.CharField('用户名称',max_length=32)
    password = models.CharField('密码',max_length=32)
    roles = models.ManyToManyField('Role',verbose_name='用户拥有的角色',blank=True)
    def __str__(self):
        return self.username
```

```python
执行数据库迁移命令
python manage.py makemigrations
python manage.py migrate
```

在admin.py文件中写入

```python
from django.contrib import admin
from rbac import models

class PermissionConfig(admin.ModelAdmin):
    list_display = ['id','url','title']#按自己的要求显示
    list_editable = ['url','title']#设置可编辑字段

admin.site.register(models.Permission,PermissionConfig)
admin.site.register(models.Role)
admin.site.register(models.User)
```

3.写view函数

```python
获取post提交的名字及密码
username = request.POST.get('username')
password = request.POST.get('password')
在数据库中查询账户名及密码并校验
obj=models.User.objects.filter(username=username,password=password)[0]
查询获取用户的权限并去重
permissions = obj.roles.filter(permissions__url__isnull=False).values('permissions__url').distinct()
在session中设置权限内容，由于permissions是queryset对象不能序列化，需要转化成列表
request.session['permissions'] = list(permissions)

```

3.设置中间件

```python
from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import reverse,redirect,HttpResponse
from django.conf import settings
import re#正则匹配

class AuthMiddleWare(MiddlewareMixin):
    def process_request(self,request):
        #获取当前访问的URL
        path = request.path_info
        #白名单
        for url in settings.WHITE_LIST:
            if re.match(url,path):
                return
        is_login = request.session.get('is_login')
        print(is_login)
        if not is_login:
            return redirect('login')
        #登录状态校验
        #免登陆校验
        #获取权限信息
        #权限校验
        for url in settings.NO_PERMISSION_LIST:#设置免验证内容
            if re.match(url, path):
                return
        permissions = request.session.get('permissions')
        for permission in permissions:
            print(permission)
            if re.match(permission['permissions__url'],path):#匹配白名单
                return
        return HttpResponse('无权限查看')
```

```python
注册中间件
MIDDLEWARE =['web.middlewares.middleware.AuthMiddleWare']
```

```python
设置白名单及免验证在settings中设置
WHITE_LIST = [
    r'/login/$',
    r'/reg/$',
    r'/admin/.*',
]
NO_PERMISSION_LIST = [
    r'/index/$',
    r'/logout/$',
]
```



RBAC 项目流程

1.设计表结构

权限表

- URL 权限   正则表达式^$
- title  标题

角色表

- name  角色名称
- permissions  多对多

角色和权限的关系表

- role_id
- permission_id

用户表

- username
- password
- roles  多对多

用户和角色关系表

- user_id
- role_id

一级菜单

权限表

- URL
- title
- is_menu
- icon

二级菜单

菜单表

- title
- icon

权限表

- URL
- title
- menu  外键  关联菜单表

权限的列表

permission_list = [{'url':url},{'url':url}]

菜单的字典

menu_dict = {

​	 一级菜单的id: {

​					title  

​					icon

​					children :  [   

​								{   'url'   'title '   }

​					   ]  

​			}

}



### 一级菜单的排序

- 对一级菜单添加字段优先级

```python
class Menu(models.Model):
    title = models.CharField(max_length=32,verbose_name='以及菜单')
    icon = models.CharField(max_length=32,verbose_name='图标')
    weight = models.IntegerField(default=1)#设置权重，默认为1
```

使用sorted在自定义标签中对展示列表进行排序

```python
from django import template#导入模板
from django.conf import settings
from collections import OrderedDict#导入有序字典
register = template.Library()


@register.inclusion_tag('menu.html')
def menu(request):
    menu_dict = request.session.get(settings.MENU_SESSION_KEY)
    # 从settings中获取MENU_SESSION_KEY的值（登录成功时会初始化此内容存储到session中）
    url = request.path_info#获取URL的地址
    od = OrderedDict()#实例化有序字典
    keys_list = sorted(menu_dict,key=lambda x:menu_dict[x]['weight'],reverse=True)#menu_dict的key进行排序，依照value中的weight的值进行排序
    for key in keys_list:
        od[key] = menu_dict[key]#循环有序的key列表创建有序字典
    for i in menu_dict.values():
        i['class'] = 'hide'#循环列表menu_dict.values()获取其中的字典，并添加'class':'hide'
        for m in i['children']:#循环children列表
            if request.current_menu_id == m['id']:#假如request中的current_menu_id跟children字典中存储的父级菜单的id相同执行以下内容，将现父级字典的'class':''设置不隐藏，则其他的未选中的隐藏，m['class']设置选中效果
                m['class'] = 'active'
                i['class'] = ''
    print(od)
    print(od.values())
    return {'menu_list':od.values()}#之后将列表返回给menu页面
```

menu.html页面循环展示

```html
<div class="multi-menu">
    {% for menu in menu_list %}
        <div class="item left-menu">
            <div class="title menu-item"><i class="fa {{ menu.icon }}"></i> {{ menu.title }}</div>
            <div class="body {{ menu.class }} ">
                {% for i in menu.children %}
                    <a class="{{ i.class }}" href="{{ i.url }}"> {{ i.title }}</a>
                {% endfor %}
            </div>
        </div>
    {% endfor %}
</div>
```

在继承模板页面导入展示

```html
{% load my_tags %} {#导入自定义标签 #}
{% menu request %}
```

### 二级菜单默认选中并且展开的效果

```python
paht = request.path_info
permissions = request.session.get(settings.PERMISSION_SESSION_KEY)
#设置字典permissions，以权限URL的id为key，其他信息为value（{'url': '/customer/list/', 'id': 1, 'pid': None, 'title': '客户列表'}），
        for permission in permissions.values():#循环permissions.values()，每个value都为字典，对现在的path地址与value的URL进行匹配，若匹配成功，咋对id跟pid（父级的id二级菜单）进行赋值字典中的id跟pid值，之后将id或pid其中之一存在的值存储于request.current_menu_id中。
            if re.match(permission['url'],path):
                id = permission['id']
                pid = permission['pid']
                if pid:
                    request.current_menu_id = pid
                    p_permission = permissions[str(pid)]
                    request.breadcrumb_list.append({'url': p_permission['url'], 'title': p_permission['title']})
                    request.breadcrumb_list.append({'url': permission['url'], 'title': permission['title']})
                else:
                    request.current_menu_id = id
                    request.breadcrumb_list.append({'url':permission['url'],'title':permission['title']})
                return
```

```python
#详见一级菜单的排序
for i in menu_dict.values():
        i['class'] = 'hide'#循环列表menu_dict.values()获取其中的字典，并添加'class':'hide'
        for m in i['children']:#循环children列表
            if request.current_menu_id == m['id']:#假如request中的current_menu_id跟children字典中存储的父级菜单的id相同执行以下内容，将现父级字典的'class':''设置不隐藏，则其他的未选中的隐藏，m['class']设置选中效果
                m['class'] = 'active'
                i['class'] = ''
    print(od)
    print(od.values())
    return {'menu_list':od.values()}#之后将列表返回给menu页面
```

### 路径导航功能

```python
paht = request.path_info
permissions = request.session.get(settings.PERMISSION_SESSION_KEY)
#设置字典permissions，以权限URL的id为key，其他信息为value（{'url': '/customer/list/', 'id': 1, 'pid': None, 'title': '客户列表'}），
        for permission in permissions.values():#循环permissions.values()，每个value都为字典，对现在的path地址与value的URL进行匹配，若匹配成功，咋对id跟pid（父级的id二级菜单）进行赋值字典中的id跟pid值，之后将id或pid其中之一存在的值存储于request.current_menu_id中。
            if re.match(permission['url'],path):
                id = permission['id']
                pid = permission['pid']
                if pid:#若是子权限，则pid存在，从permissions的字段中获取key的值与pid相同的value的字典，需注意permissions的key由于序列化为字符串，所以需对pid转化为字符串。之后将获取的父级的权限信息的字典添加到request中的breadcrumb_list中，再将子权限字典信息添加到request中的breadcrumb_list。
                    request.current_menu_id = pid
                    p_permission = permissions[str(pid)]
                    request.breadcrumb_list.append({'url': p_permission['url'], 'title': p_permission['title']})
                    request.breadcrumb_list.append({'url': permission['url'], 'title': permission['title']})
                else:#若pid不存在id存在则此权限为二级菜单的权限，设置以下内容
                    request.current_menu_id = id                request.breadcrumb_list.append({'url':permission['url'],'title':permission['title']})
                return
```

自定义标签展示路径

```python
@register.inclusion_tag('rbac/breadcrumb.html')
def breadcrumb(request):
    breadcrumb_list = request.breadcrumb_list
    return {'breadcrumb_list':breadcrumb_list}
```

breadcrumb.html页面

```html
<ol class="breadcrumb no-radius no-margin" style="border-bottom: 1px solid #ddd;">
    {% for breadcrumb in breadcrumb_list %}
        {% if forloop.last %}
            <li class="active" style="color: green">{{ breadcrumb.title }}</li>
        {% else %}
            <li><a href="{{ breadcrumb.url }}">{{ breadcrumb.title }}</a></li>
        {% endif %}
    {% endfor %}
</ol>
```

在继承模板页面导入展示

```html
{% laod my_tags %}
{% breadcrumb request %}
```



财务管理

​	缴费列表

​		添加缴费

​		编辑缴费

客户管理

​	客户列表

​		添加客户

​		编辑客户

permission

id      url                            title             menu_id     parent_id 

1    /payment/list/         缴费列表         1                      null  

2   /payment/add/    	添加缴费         null                  1 

3   /payment/edit/        编辑缴费         null                  1

