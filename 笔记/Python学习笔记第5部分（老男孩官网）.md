## 老男孩官网项目

### 1.设置主页

设置静态home和shanghai-home页面

- 设置路由分发，include('web.urls')，在web内创建urls文件并从web中导入view视图函数

  ```python
  from django.conf.urls import url,include
  from django.contrib import admin
  urlpatterns = [
      url(r'^admin/', admin.site.urls),
      url(r'', include('web.urls')),
  ]
  
  from django.conf.urls import url, include
  from web import views
  
  urlpatterns = [
      url(r'^$', views.home, name='home'),
      url(r'^shanghai/$', views.shanghai, name='shanghai'),
  ]
  
  
  ```

- 定义视图函数

  ```python
  from django.shortcuts import render
  
  def home(request):
      return render(request, 'home\home.html')
  
  def shanghai(request):
      return render(request,'shanghai-home.html')
  ```

- 在web文件中创建templates，并将home主页html文件导入,在项目文件夹下创建static文件夹并将相关css样式及js样式导入

- 修改home主页html文件的样式导入路径，使用get_static_prefix方法

  ```html
  {% load static %} 导入static静态路径
  {% get_static_prefix %} 使用此方法获取静态路径拼接到路径内
  <img src="{% get_static_prefix %}images/footer/code2.png" alt="">   列子
  ```

### 2.文章

列表表

- 列表的名称

文章表

- title
- category 类别
- create_at datetime类型
- summary 摘要

文章详情表

- content textfield

media的配置

settings的配置

```
MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
```

url的配置

```python
from django.views.static import serve
from django.conf import settings

urlpatterns = [
 
    url(r'^media/(?P<path>.*)', serve, {'document_root': settings.MEDIA_ROOT}),
]
```

富文本编辑框

下载 

pip install django-ckeditor

注册

```
INSTALLED_APPS = [
	
    'ckeditor',
    'ckeditor_uploader',
]
```

使用字段：

```
from ckeditor_uploader.fields import RichTextUploadingField


class ArticleDetail(models.Model):
    content = RichTextUploadingField(verbose_name='文章详情')
```

配置URL：

```python
from ckeditor_uploader import views

urlpatterns = [

    url(r'^media/(?P<path>.*)', serve, {'document_root': settings.MEDIA_ROOT}),
    # 上传文件
    url(r'^ckeditor/upload/', views.upload),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

]
```

模板中：

```
{{ field }}   富文本编辑框的字段

<script src="{% static 'ckeditor/ckeditor/ckeditor.js' %}"></script>
<script src="{% static 'ckeditor/ckeditor-init.js' %}"></script>
```

### 3.Djangorestframework使用

1.下载安装

```python
pip install djangorestframework  ## djangorestframework
pip install django-filter  ## 过滤使用
```

2.注册app

```python
INSTALLED_APPS = [
	...
    'rest_framework',
    'django_filters'
]
```

3.model

```python
class Feedback(models.Model):
    type_choice = (
        (0, '未分类'),
        (1, '学习笔记'),
        (2, '学员评价'),
        (3, '入职邀约'),
    )

    img = models.ImageField(upload_to='img/feedback/')
    back_type = models.IntegerField(choices=type_choice, default=0)
```

4.路由

```python
url(r'^ajax_feedback/', views.AjaxFeedback.as_view()),
```

5.视图函数

```python
from rest_framework import generics
from web.serializers import FeedbackSerializer
from web.pagination import DefaultPagination
from django_filters.rest_framework import DjangoFilterBackend


class AjaxFeedback(generics.ListAPIView):
    queryset = models.Feedback.objects.all()
    serializer_class = FeedbackSerializer
    pagination_class = DefaultPagination
    filter_backends = [DjangoFilterBackend, ]
    filter_fields = ['back_type']
```

6.序列化器类

```python
from rest_framework import serializers
from repository import models


class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Feedback
        fields = ['img']
```

7.分页类

```python
from rest_framework.pagination import PageNumberPagination

class DefaultPagination(PageNumberPagination):
    page_size = 8  # 一页多少条数据
    page_query_param = 'page'  # 分页查询条件的key
    page_size_query_param = 'size'
    # max_page_size = 8
```



### 4.RESTful API

是一种软件架构风格

URI   统一资源标识符

- URL 统一资源定位符
- URN 统一资源名称

REST 表述性状态转移

URI表示一个资源

4种请求方式表示4中操作

GET   获取资源

POST  新建资源

PUT  更新资源

DELETE 删除资源

url  使用名词 不适用动词 

/publishers/

book   

name 

publisher_id

{

​	name:

​	publisher:   /publisher/1/

}

域名：https://example.org/api/    

路径  名词（复数）表示     不用动词

请求方式   返回结果

GET   获取资源        返回 资源的列表  

GET   /XXXXX/1       返回 一个资源

POST  新建资源      返回 新建的资源

PUT  更新资源        返回 更改后的资源

DELETE 删除资源     返回空内容  也可以是提示 

状态码 

<https://www.cnblogs.com/blackball9/p/11246958.html>

发生错误时 返回错误信息

过滤条件:

​	?page=1&page_size=10

​	?type=1

Hypermedia API   返回的关联的数据时  尽量返回url 

/books/      GET  POST 

/book/(\d+)/   GET  PUT  DELETE 

使用模块：Django   Djangorestframework  rest_framework

推荐使用CBV

```python
from rest_framework.views import APIView
from rest_framework.response import Response
```

序列化  json

对象等数据类型 ---》   可用于传输或存储的数据形式

发送get请求时返回数据

```python
    def get(self, request, *args, **kwargs):
        """ 获取所有的数据  返回所有的书籍数据 json格式"""
        # 1. 从数据库获取所有数据
        all_books = models.Book.objects.all().values('id', 'title', 'pub_date')  # [ {} ] queryset类型，不能被序列化
        # 2. 返回数据  json序列化
        return HttpResponse(json.dumps(list(all_books), ensure_ascii=False))#设置不进行编码 正常显示

```

```python
from django.http.response import JsonResponse
#在返回数据的时候由于datetime类型不能被序列化，所以需要把datetime类型转换成字符串在序列化
class BookListView(View):

    def get(self, request, *args, **kwargs):
        """ 获取所有的数据  返回所有的书籍数据 json格式"""
        # 1. 从数据库获取所有数据
        all_books = models.Book.objects.all().values('id', 'title', 'pub_date')  # [ {} ]
        # 2. 返回数据  json序列化
        return JsonResponse(list(all_books), safe=False, json_dumps_params={'ensure_ascii': False})#设置允许非字典被序列化，需要设置safe=False，all_book是queryset对象，需要转化成list形式，设置json_dumps_params={'ensure_ascii': False} 设置不进行编码正常显示
```

```python
from rest_framework.views import APIView#导入APIView
from rest_framework.response import Response#导入Response
#在app中注册rest_framework

class BookListView(APIView):
    def get(self, request, *args, **kwargs):
        all_books = models.Book.objects.all().values('title', 'authors__name')#查询书的名字及作者的名字，跨表查
        print(all_books)
        return Response(all_books)
```

```python
from app01.serializer import BookSerializer


class BookListView(APIView):
    def get(self, request, *args, **kwargs):
        all_books = models.Book.objects.all()
        ser_obj = BookSerializer(all_books, many=True)#由于all_books是多可对象，之前传instance=obj是一个对象，所以需要设置many=True

        return Response(ser_obj.data)

    def post(self, request, *args, **kwargs):
        ser_obj = BookSerializer(data=request.data)#需要使用request.data才能取出从网页发来的post的数据
        if ser_obj.is_valid():
            ser_obj.save()
            return Response(ser_obj.data)
        return Response(ser_obj.errors)
```

```python
class BookView(APIView):
    def get(self, request, pk, *args, **kwargs):
        book_obj = models.Book.objects.filter(pk=pk).first()
        ser_obj = BookSerializer(book_obj)

        return Response(ser_obj.data)

    def put(self, request, pk, *args, **kwargs):
        book_obj = models.Book.objects.filter(pk=pk).first()
        ser_obj = BookSerializer(book_obj, data=request.data, partial=True)#partial=True允许部分修改
        if ser_obj.is_valid():
            ser_obj.save()
            return Response(ser_obj.data)
        return Response(ser_obj.errors)

    def delete(self, request, pk, *args, **kwargs):
        obj = models.Book.objects.filter(pk=pk).first()
        if obj:
            obj.delete()
            return Response({'msg': '删除成功'})
        return Response({'error': '数据不存在'})

```

序列化器

```python
from rest_framework import serializers
from app01 import models


class PublisherSerializer(serializers.Serializer):
    name = serializers.CharField()


class AuthorSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()


class BookSerializer(serializers.Serializer):
    title = serializers.CharField()
    price = serializers.DecimalField(max_digits=6, decimal_places=2)
    pub_date = serializers.DateTimeField()
#以下两个是读取展示数据的时候才使用
    pub = PublisherSerializer(required=False, read_only=True)
    authors = serializers.SerializerMethodField(read_only=True)  # get_字段名
#以下两个是写入数据的时候才使用
    post_pub = serializers.IntegerField(write_only=True)
    post_author = serializers.ListField(write_only=True)

    def get_authors(self, obj):
        ser_obj = AuthorSerializer(obj.authors.all(), many=True)
        return ser_obj.data
#由于序列化器跟models无关系，创建的时候需要自定义create函数来创建。
    def create(self, validated_data):
        book_obj = models.Book.objects.create(
            title=validated_data['title'],
            price=validated_data['price'],
            pub_date=validated_data['pub_date'],
            pub_id=validated_data['post_pub']
        )
        book_obj.authors.set(validated_data['post_author'])
        return book_obj

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title',instance.title)
        instance.price = validated_data.get('price',instance.price)
        instance.pub_date = validated_data.get('pub_date',instance.pub_date)
        instance.pub_id = validated_data.get('post_pub',instance.pub_id)
        instance.save()
        instance.authors.set(validated_data.get('post_authors',instance.authors.all()))
        return instance
```

### 5.极验验证码

所需模块：geetest     request

设置登录URL

```python
url(r'^login\.html', views.Login.as_view(), name='login'),
```

编写CBV登录函数

```python
from django.views import View
from django.http.response import JsonResponse


class Login(View):
    def get(self, request):
        mode = request.GET.get('mode', 'password')
        return render(request, 'signin/signin.html', {'mode': mode})

    def post(self, request):
        phone = request.POST.get('phone')
        code = request.POST.get('code')
        if code != request.session.get('code'):
            ret = {'code': 1, 'msg': '验证码错误'}
            return JsonResponse(ret)
        exist = models.Customer.objects.filter(phone=phone).exists()
        if exist:
            # 登录成功
            ret = {'code': 0, 'msg': '登录成功', 'url': reverse('home')}
            return JsonResponse(ret)
        else:
            ret = {'code': 2, 'msg': '此手机号暂没有注册'}
            return JsonResponse(ret)
```

把登录页面放到web下的templates下，设置get请求时返回登录页面

设置登录页面的css样式使用正则匹配统一调整样式路径

```python
搜索： "../../(.*?)"
替换为："{% static '$1' %}"
{% load static %}
```

设置密码登录跟短信登录页面分开?mode=sms，使用URL反向解析拼接地址

```html
在后端通过request.GET.get('mode','password')获取网页的mode信息，默认为password。之后返给前端页面
<a href="{% url 'login' %}"><span class="{% if mode == 'password' %}this{% endif %}">密码登录</span></a>
                    <a href="{% url 'login' %}?mode=sms"><span class="{% if mode == 'sms' %}this{% endif %}">短信登录</span></a>
```

文档地址：https://docs.geetest.com/install/overview/start/

注册登录之后获取行为验证ID和KEY

将行为验证的ID跟KEY设置到settings中

```python
PC_GEE_ID = '13e4f7b3b8323da2b0ff9950b7ab76bc'
PC_GEE_KEY = '2b7352c63ed6252df1762fb29c54ee92'
```

在URL中写上pc端校验的路径，之后再view函数中将demo的pc函数写入

```python
    url(r'^pc-geetest/register', views.pcgetcaptcha, name='pcgetcaptcha'),
    url(r'^pc-geetest/ajax_validate', views.pcajax_validate, name='pcajax_validate'),
```

将settings中设置的ID跟KEY导入之后传参

```python
from geetest import GeetestLib
from django.conf import settings

def pcgetcaptcha(request):
    user_id = 'test'
    gt = GeetestLib(settings.PC_GEE_ID, settings.PC_GEE_KEY)#ID跟KEY
    status = gt.pre_process(user_id)
    request.session[gt.GT_STATUS_SESSION_KEY] = status
    request.session["user_id"] = user_id
    response_str = gt.get_response_str()
    return HttpResponse(response_str)
```

设置ajax的二次校验

```python
def pcajax_validate(request):
    if request.method == "POST":
        gt = GeetestLib(settings.PC_GEE_ID, settings.PC_GEE_KEY)
        challenge = request.POST.get(gt.FN_CHALLENGE, '')
        validate = request.POST.get(gt.FN_VALIDATE, '')
        seccode = request.POST.get(gt.FN_SECCODE, '')
        status = request.session[gt.GT_STATUS_SESSION_KEY]
        user_id = request.session["user_id"]
        if status:
            result = gt.success_validate(challenge, validate, seccode, user_id)
        else:
            result = gt.failback_validate(challenge, validate, seccode)
        if result:
            result = {"status": "success"}
            # 二次校验成功
            # 发短信
            code = sms.get_code()
            phone = request.POST.get('phone')
            sms.send_sms(phone, code)
            request.session['code'] = code
        else:
            result = {"status": "fail"}
        return HttpResponse(json.dumps(result))
    return HttpResponse("error")
```



在前端设置js获取手机号进行校验是否是手机号，假如判断成功之后返回True，

打开Django项目，将下载的django_demo放到项目文件中，引入虚拟环境并安装好锁需要的各种所需的包。

aliyun短信

所需模块：aliyun-python-sdk-core

文档地址：<https://help.aliyun.com/document_detail/53090.html?spm=a2c1g.8271268.10000.94.772fdf25kEccNH>

[https://api.aliyun.com/new#/?product=Dysmsapi&api=SendSms&params=%7B%22RegionId%22%3A%22cn-hangzhou%22%2C%22PhoneNumbers%22%3A%22%22%2C%22SignName%22%3A%22%22%2C%22TemplateCode%22%3A%22%22%7D&tab=DEMO&lang=JAVA](

在settings中配置设置阿里云发送短信的相关信息

```python
ACCESSKEY_ID = "LTAIOj7VnM7ho77j"
ACCESS_KEY_SECRET = 'pKGn0wNVezkcjzhKwR1xi69XgCJQ22'

SMS_TEMPLATE_CONF = {'TemplateCode': 'SMS_167972102',
                     'TemplateParam': '{"code":"%s"}',
                     'SignName': "14K"}
```

在项目目录下创建文件utils，在文件夹下创建sms文件，将发短信的代码复制进来,将settings中配置的参数导入进来

```python
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest

from django.conf import settings

client = AcsClient(settings.ACCESSKEY_ID, settings.ACCESS_KEY_SECRET, 'cn-hangzhou')


def send_sms(phone, code):
    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', phone)#手机号
    request.add_query_param('SignName', settings.SMS_TEMPLATE_CONF.get('SignName'))
    request.add_query_param('TemplateCode', settings.SMS_TEMPLATE_CONF.get('TemplateCode'))
    request.add_query_param('TemplateParam', "{'code':'%s'}" % (code))#code随机验证码

    response = client.do_action(request)
    # python2:  print(response)
    print(str(response, encoding='utf-8'))

import random

def get_code():
    return ''.join(random.sample([str(i) for i in range(0, 10)], 6))

```









