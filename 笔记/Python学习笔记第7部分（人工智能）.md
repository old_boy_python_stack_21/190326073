课程信息：

2019年8月5日：15-16天
Flask Web 框架 轻量

websocket 全双工通讯 socket TCP 通讯

MongoDB 数据库 文件型数据库 {} 没有限制 和 约束

Mui + HTML5 Plus 调用移动操作系统的封装 IOS Android 

人工智能技术应用 BaiduAI
ASR 语音识别 声音转换成文字
TTS 语音合成 文字转换成声音
NLP 自然语言处理 你的名字叫什么 你的名字是什么 文本相似度
paddle paddle 百度
Ai studio

## Flask Web 框架

特点：框架 轻量 以简单为基准 一切从简

### 1.Flask与Django对比

框架对比

```python
Django                           Flask
Admin-Model                      原生无
Model                            原生无
Form                             原生无
session                          有
```

Django的优势：组件全，功能全，教科书式

Django的劣势：占用资源，创建复杂度较高

Flask的优势：轻、快、灵活简单上手

Flask的劣势：先天不足，第三方组件稳定性较差

### 2.Flask的安装

pip3 install Flask   (不要使用工具中的插件创建Flask项目)

创建flask项目

```python
from flask import Flask

app = Flask(__name__) #实例化

@app.route('/url') #路由分发
def home():
    return 'helloworld' #return 不能为None

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/get_file')
def get_file():
    return send_file('app.py') #使用send_file 前端自动识别content-type类型

app.run()
app.run("127.0.0.1",5000) #默认host跟port端口
```

创建templates模板，将templates模板右键设置成templates文件，可以在里面创建HTML页面，使用jinja

创建login.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>login</title>
</head>
<body>
<form action="" method="post">
    <input type="text">
    <input type="submit" value="提交">
    <button>123</button>

</form>
</body>
</html>
```

request.form 获取formdata数据

request.args 获取URL上的信息（类型：werkzeug.datastructures.ImmutableMultiDict）

```python
@app.route('/login',methods=['GET','POST']) #需在此设置允许访问视图函数类型列表
def login():
    if request.method == 'GET':
        dic =request.args.to_dict()将信息转换成字典类型
        return render_template('login.html')
    else:
        print(requestform.to_dict())
        #{'username': 'alex'}
        name =request.form.get('username')
        print(name)
        return 'login ok'
```

request.files上传文件

```python
my_file = request.files.get('my_file')#获取文件
filename = my_file.filename #获取文件名
file_path = os.path.join('avatar',filename)#创建avatar文件夹来保存文件
my_file.save(file_path)
```

```html
<form action="" method="post" enctype="multipart/form-data"> #设置enctype类型
    <input type="text" name="username">
    <input type="file" name="my_file" id="">
    <input type="submit" value="提交">
</form>
```

使用jinja2在全局传递函数

```python
@app.template_globle()
def ab(a,b):
    return a+b
```

```html
{{ ab(10,3) }}在前端页面使用
```

使用Markup将数据传递到前端

```python
@app.route('/stu_list')
def student_list():
    inp = Markup('<input type="submit" value="提交">')
    return render_template('stu_list.html', stu=STUDENT,inp=inp)
```

```python
@app.template_global()
def my_input(na,ty):
    s = f"<input type='{ty}' value='{na}'>"
    return Markup(s)
```

flask中设置session

```python
app.secret_key = 'sadasdasd' #设置secret_key
session['username'] =name#设置session
print(session.get('username'))
```



### 3.Flask中的Response

​	1.HTTPResponse('hello')        "str"   字符串

​	2.render   响应模板                   render_template("html") str

​	3.redirect("/")                              redirect("/") str

​	以上是web框架的Response三剑客

​	4.send_file()      instance      返回文件内容，自动识别文件类型，Content-type中添加文件类型，Content-type:文件类型

​	**浏览器特性  可识别的Content-type自动渲染不可识别的Content-type会自动下载

​	5.jsonify() str #返回标准格式的json字符串，先序列化json的字典，Content-type中加入Appliction/json

​	**Flask 1.1.1版本中可以直接返回字典格式，无序jsonify

### 4.Flask中的请求Request

request的方法

```python
request.method  获取请求方式
request.form    获取FormData中的数据 也就是所谓的Form标签 to_dict()
request.args    获取URL中的数据to_dict()

request.json    请求中Content-Type:application/json请求体中的数据被序列化到request.json中以字典的形式存放
request.data    请求中Content-Type中不包含Form或FormData保留请求体中的原始数据b
request.files  获取Form中的文件

request.path   请求路径 路由地址
request.url    访问请求的完成路径 包括url
request.host   主机位 127.0.0.1:5000
request.cookies字典获取浏览器请求时带上的cookie
**request.values 获取URL和FormData中的数据 敏感地带
```

5.jinja2 -----template语言

{{ }} 引用 或执行

{% %} 逻辑引用

### 5.Flask中的路由

@app.route()的参数

```python
rule        "/login"路由地址
methods     允许进入视图函数的请求方式
endpoint    Mapping路由地址和endpoint-路由地址和视图函数Mapping endpoint在同一个app中不能重复出现，默认值是视图函数名
defaults    默认路由参数
strict_slashes  是否严格遵循路由匹配规则
redirect_to  永久重定向 308 301
动态参数路由  "/get_music/<filename>"    def get_music(filename)可以分页，获取文件，解决分类，解决正则路由问题
```

endpoint:反向URL地址，默认为视图函数名（url_for）

```python
from flask import url_for


@app.route("/info", methods=["GET", "POST"], endpoint="r_info")
def student_info():
    print(url_for("r_info"))  # /info
    stu_id = int(request.args["id"])
    return f"Hello Old boy {stu_id}"  # Python3.6的新特性 f"{变量名}"
```

defaults:视图函数的参数默认值{"uid":100}

```python
from flask import url_for


@app.route("/info", methods=["GET", "POST"], endpoint="r_info", defaults={"nid": 100})
def student_info(nid):
    print(url_for("r_info"))  # /info
    # stu_id = int(request.args["id"])
    print(nid)  # 100
    return f"Hello Old boy {nid}"  # Python3.6的新特性 f"{变量名}"
```

strict_slashes:url 地址结尾符"/"的控制False：无论结尾'/'是否存在均可访问，True：结尾必须是'/'

```python
# 访问地址 : /info 
@app.route("/info", strict_slashes=True)
def student_info():
    return "Hello Old boy info"


# 访问地址 : /infos  or  /infos/
@app.route("/infos", strict_slashes=False)
def student_infos():
    return "Hello Old boy infos"
```



6.Flask初始化配置

app = Flask(`__name__`,template_floder="templates")

```python
template_floder="templates"  模板文件存放路径
static_folder      静态文件存放目录 默认值是static
static_url_path    静态文件访问路径，默认值是/static_floder
```

### 7.Flask示例配置

app的配置 app.config

```python
class TestConfig(object):
    TESTING = True
    SECRET_KEY = haslib.md5(f'{time.time()}##$${time.time()}'.encode('utf8')).hexdigest()
    PERMANENT_SESSION_LIFETIME = 36000
    SESSION_COOKIE_NAME = 'session_cookie_name'

app.config.form_object(TestConfig)
```

### 8.Flask中的蓝图

Blueprint(app01)

不能被run的Flask实例，不存在Config

设置blueprint

```python
from flask import Blueprint

bprint = Blueprint("app01",__name__,url_prefix="/urlname")

@bprint.route("/add_user")
def add_user():
    return "adduser"
```

导入使用

```python
from addr* import bprint

app.register_blueprint(bprint)
```

配置setting

```python
import haslib,time

class DebugConfig(object):
    DEBUG = True
    SECRET_KEY = haslib.md5(f"{time.time()}$%%".encode(utf8)).hexdigest()
    PERMANENT_SESSION_LIFETIME = 36000
    SESSION_COOKIE_NAME = 'session_cookie_name'
```

导入使用

```python
from setting import DebugConfig

app.config.form_object(DebugConfig)
```

### 9.特殊装饰器

```python
@qpp.before_request
在请求进入视图函数之前作出处理

@app.after_request
在响应返回客户端之前，结束视图函数之后

before+after请求生命周期：
正常 before1 - before2 - viewfunc - after2 - after1
异常 before1 - after2 - after1

@app.errorhandler(404)
def error404(errorMessage)
	return '访问异常 '
```

### 10.CBV-RestAPI

```python
from flask import views

class Login(views.MethodView):
    def get(*args,**kwargs):
        pass
    def post(*args,**kwargs):
        pass

  
app.add_url_rule("url",endpoint=None,view_func=Login.as_view(name="当前视图函数名，必须唯一，它是endpoint"))
```

### 11.redis

Redis的安装及使用

```python
将Redis文件地址配置到环境变量中
运行cmd 输入命令redis-server
另再运行cmd终端  输入命令redis-cli 
之后可输入命令使用
redis默认端口为6379（MySQL默认端口3306）
redis部分常用命令：
set name hehe  #设置key为name，value为hehe的键值对
keys * #查看所有的key
get name  #查看key为name的value值
select num #切换库，redis有16个库 0-15 数据隔离
key g* #查询以g开头的key
key *g* #查询key中包含g的
```

flask中使用redis

```python
安装redis的包
from redis import Redis

redis_cli = Redis(host="192.168.12.62",port=6379,db=6)

redis_cli.set("name","s21")
res = redis_cli.get("name")
res = res.decode("utf8")

user_info = json.dumps({"name":"juaner","age":20,"gender":"nv"})
redis_cli.set("userinfo",user_info)
res = redis_cli.get("userinfo")
res = json.loads(res)
```

flask-session的使用

```python
from flask import Flask,request,session

from flask_session import Session

app = Flask(__name__)
app.secret_key = "#$%^"
app.config["SESSION_TYPE"] = "redis"
app.config["SESSION_REDIS"] = Redis(host="192.168.12.62",port=6379,db=10)
Session(app)

app.route("/sets")
def set():
    session["key"] = "sesionvluae"
    return "set ok"

app.route("/gets")
def get():
    return session.get("key")

if __name__ = "__main__":
    app.run()
    
#从redis库中取session的key跟value
#get key(session:valve)
```

### 12.websocket原理

websocket与http

websocket的最大特点就是，服务器可以主动向客户端推送信息，客户端也可以主动向服务器发送信息，是真正的双向平等对话。

http有1.0和1.1，后者增加了keep-alive，把多个http请求合并为一个，但是websocket也是一个协议，跟http协议基本没有关系，知识为了兼容现有的浏览器，在握手阶段使用了http。见下图：

![](E:\刘小辉作业\笔记\http协议.jpg)

![](E:\刘小辉作业\笔记\websocket协议.jpg)

websocket的其他特点：

- 建立在tcp协议之上，服务端的实现比较容易。
- 与http协议有着良好的兼容性。握手阶段采用http协议，握手时不容易屏蔽，能通过各种http代理服务器。
- 数据格式比较轻量，性能开销校，通信高效。
- 可以发送文本，也可以发送二进制数据。
- 没有同源限制，客户端可以与任意服务器通信。
- 协议标识符是ws（如果加密为wss），服务器网址是URL。

websocket的优点

- 现对于http这种非持久的协议来说，websocket是一个持久化的协议，http的声明周期通过request来界定，也就是一个request一个response，在http1.0中，这次http请求就结束了；在http1.1中进行了改进，使得有一个keep-alive，也就是说在一个http连接中，可以发送多个request，接收多个response。但是request=response，而且response是被动的，不能主动发起。

- websocket是基于http协议的，或者说借用了hppt协议来完成一部分握手。

- websocket握手请求

  ```python
  GET /chat HTTP/1.1
  Host: server.example.com
  Upgrade: websocket
  Connection: Upgrade
  Sec-WebSocket-Key: x3JJHMbDL1EzLkh9GBhXDw==
  Sec-WebSocket-Protocol: chat, superchat
  Sec-WebSocket-Version: 13
  Origin: http://example.com
  ```

- 以上协议请求中相对于http协议多了以下

  ```python
  Upgrade: websocket
  Connection: Upgrade
  ```

- 这个是websocket的核心了，告诉Apache、NGINX等服务器：我发送的请求时websocket协议，不是http协议

  ```python
  Sec-WebSocket-Key: x3JJHMbDL1EzLkh9GBhXDw==
  Sec-WebSocket-Protocol: chat, superchat
  Sec-WebSocket-Version: 13
  ```

  Sec-WebSocket-Key是一个base64 encode的值，这个是浏览器随机生成的，来验证是不是真的websocket助理；Sec-WebSocket-Protocol是一个用户定义的字符串，用来区分同URL下不同服务所需要的协议；Sec-WebSocket-Version是告诉服务器使用的websocket draft（协议版本）。

- 之后服务器会返回下列内容，表示已经接收到请求，成功建立websocket连接

  ```python
  HTTP/1.1 101 Switching Protocols
  Upgrade: websocket
  Connection: Upgrade
  Sec-WebSocket-Accept: HSmrc0sMlYUkAGmm5OPpG2HaGWk=
  Sec-WebSocket-Protocol: chat
  ```

- 这里开始就是http最后负责的区域了，告诉客户，已经成功切换协议

  ```python
  Upgrade: websocket
  Connection: Upgrade
  ```

- Sec-WebSocket-Accept这个是经过服务器确认，并且加密过后的Sec-WebSocket-Key；Sec-WebSocket-Protocol则表示最终使用的协议，至此http已经完成他所有工作了，接下来完全按照websocket协议进行。

websocket的作用

- ajax轮询

  轮询的原理非常简单，让浏览器间隔一段时间发一次请求，询问是否有新消息。

- long poll  长链接

  long poll其实原理与ajax轮询相似，都是采用轮询的方式，不过采取的是阻塞模式，也就是说客户端发起请求后如果没有消息就一直不返回response给客户端，知道有消息才返回，返回完成之后客户端再次建立连接，周而复始。

- 以上两种方式都是在不断的建立http连接，然后等待服务端处理，可以体现http协议的另外一个特点，被动型。（被动性就是服务端不能主动联系客户端，只能客户端发起，以上两种都是非常耗资源的，ajax轮询需要夫妻有很快的处理速度和资源，long poll需要有很高的并发，也就是说同时接待客户的能力。）

- websocket

  websocket解决了http的无状态特性和服务端的被动性，当服务器升级后（http升级为websocket），服务端就可以主动推送信息给客户端。这样经过一次http请求切换为websocket协议后就可以做到源源不断的信息传送了。

基于websocket实现群聊

```python
1.建立websocket服务 + flask web框架 + gevent-websocket
2.request.environ.get("wsgi.websocket")获取连接，将获取到的链接保持在服务器中socket_list = []
3.基于长链接socket接收用户传递的消息
4.将消息转发给其他用户

基于 JavaScript 实现 websocket 客户端
var ws = new WebSocket("ws://")  #ws的路由地址
ws.onmesssage = func(MessageEvent) #实现逻辑

ws.onmessage 当ws客户端收到消息时执行回调函数
ws.onopen 当ws客户端建立完成连接时 status == 1 时执行回调函数
ws.onclose 当ws客户端关闭中或关闭时 执行的回调函数 status == 2,3
ws.onerror 当ws客户端出现错误时

Websocket单点聊天时的转发
服务器中保存的连接方式发生变化 以字典形式储存
存储结构 : {用户的唯一标识:Websocket连接,user1:Websocket}
单点消息发送时 receive data = {sender:发送方,receiver:接收方,data:数据}
从data中找到接收方的"名字"Key
拿着名字key 去存储结构中 找到 名字Key所对应的websocket连接
websocket连接.send(data) # socket 传输 bytes
```

websocket实现点对点聊天

```python
import json

from flask import Flask.request,render_template
from geventwebsocket.handler impot WebSocketHandler#请求处理WSGI HTTP
from geventwebsocket.server import WSGIServer#替换原来的WSGI服务
from geventwebsocket.websocket import WebSocket #语法提示

app = Flask(__name__)

socket_dict = {}

@app.route("/ws/<username>")
def my_ws(username):
    ws_socket = request.environ.get("wsgi.websocket") #type:WebSocket
    socket_dict[username] = ws_socket
    
    while True:
        msg = ws_socket.receive()
        
        msg_dict = json.lods(msg)
        receiver = mas_dic.get("receiver")
        receiver_socket = socket_dict.get(receiver)
        
        receiver_socket.send(msg)
    

@app.route("/wechat")
def wechat():
    return render_template("ws.html")

if __name__ = "__main__":
    http_serv = WSGIServer(("0.0.0.0",9527),app,handler_class=WebSocketHandler) #handler_class=WSGIHandler/HTTPHandler
    http_serv.serve_forever()
```

ws.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
</head>
<body>
<p>你的昵称：<input type="text" id="username"><button onclick="login()">loginchatbox</button></p>
to<input type="text" id="receiver">msg<input type="text" id="content"><button onclick="send_msg()">
send</button>   
<div id="content_list" style="width:500px;"></div>
</body>
<script type="application/javascript">
    var ws = null;
    function send_msg(){
        var msg = document.getElementById("content").value;
        var receiver = document.getElementById("receiver").value;
        var sender = document.getElementById("username").value;
        
        var send_str = {
            receiver:receiver,
            sender:sender,
            data:msg
        };
        ws.send(JSON.stringify(sent_str));
        var my_div = document.getElementById("content_list");
        var ptag = document.createElement("p");
        ptag.innerText = msg + " : 我";
        ptag.style.cssText = "text-align: right";
        my_div.appendChild(ptag);
    }
    
    funtion login(){
        var username = document.getElementById("username").value;
        ws = new WebSocket("ws://192.168.12.60:9527/ws/"+username);
        ws.onmessage = function(messageEvent){
            consle.log(messageEvent.data);
            var obj = JSON.parse(messageEvent.data);
            var my_div = document.getElementById("content_list");
            var ptag = document.createElement("p");
            ptag.innerText = obj.sender + " : " + obj.data;
            my_div.appendChild(ptag);
        };
    }

</script>
</html>
```

群聊

```python
from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler  # 请求处理 WSGI HTTP
from geventwebsocket.server import WSGIServer  # 替换Flask原来的WSGI服务
from geventwebsocket.websocket import WebSocket  # 语法提示

app = Flask(__name__)

socket_list = []

@app.route("/ws")
def my_ws():
    ws_socket = request.environ.get("wsgi.websocket")  # type:WebSocket
    socket_list.append(ws_socket)
    print(len(socket_list),socket_list)
    while True:
        msg = ws_socket.receive()
        print(msg)
        for usocket in socket_list:
            if usocket == ws_socket:
                continue
            try:
                usocket.send(msg)
            except:
                continue
                
@app.route("/wechat")
def wechat():
    return render_template("ws.html")


if __name__ == '__main__':
    # app.run()
    http_serv = WSGIServer(("0.0.0.0", 9527), app,
                           handler_class=WebSocketHandler)  # handler_class=WSGIHandler / HTTPHandler
    http_serv.serve_forever()
```

ws.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
</head>
<body>
<input type="text" id="content"><button onclick="send_msg()">发送消息</button>
<div id="content_list">

</div>

</body>
<script type="application/javascript">
    var ws = new WebSocket("ws://192.168.12.87:9527/ws");
    ws.onmessage = function (messageEvent) {
        console.log(messageEvent.data);
        var my_div = document.getElementById("content_list");
        var ptag = document.createElement("p");
        ptag.innerText = messageEvent.data;
        my_div.appendChild(ptag);
    };

    function send_msg() {
        var msg = document.getElementById("content").value;
        ws.send(msg);
    }

</script>
</html>
```

### 13.使用语音识别实现对话

将语句转换成语音

```python
from aip import AipSpeech

#APPID AK SK信息
APP_ID = ""
API_KEY = ""
SECRET_KEY = ""

msg = "zifuchuan" #将要转成语音的文字
client = AipSpeech(APP_ID,API_KEY,SECRET_KEY)#实例化AipSpeech，并将相关信息传进去

result = client.synthesis(msg,"zh",1,{"vol":5,"spd":5,"pit":6,"per":3})

if not isinstance(result,dict):
    with open("audio.mp3","wb") as f:
        f.write(result)#当数据正常时，result是正常数据
else:
    print(result)#当数据不正常时，是一个错误信息字典

```

将语音转换成语句

```python
from aip import AipSpeech

import os

#APPID AK SK信息
APP_ID = ""
API_KEY = ""
SECRET_KEY = ""

client = AipSpeech(APP_ID,API_KEY,SECRET_KEY)#实例化

def get_file_content(filepath):
    cmd_str = f"ffmpeg -y  -i {filepath}  -acodec pcm_s16le -f s16le -ac 1 -ar 16000 {filepath}.pcm"#拼接字符串
    os.system(cmd_str)#执行cmd命令将其他格式的文件转换成name.pcm的百度可识别的文件
    with open(f"{filepath}.pcm","rb") as fp:
        return fp.read()

res = client.asr(get_file——content("音频文件"),"pcm",16000,{"dev_pid":1536,})#将音频文件交由百度处理返回包含索要信息的字典

print(res.get("result")[0])
```

匹配相似度

```python
from aip import AipNlp

#APPID AK SK信息
APP_ID = ""
API_KEY = ""
SECRET_KEY = ""

client = AipNlp(APP_ID,API_KEY,SECRET_KEY)#实例化

msg_a = ""
msg_b = ""
res = client.simnet(msg_a,msg_b)#匹配两个信息相似度并返回一个字典
ret = res.get("score")#获取相似度的值
```

简配版对话

```python
import os

import requests

from aip import AipEpeech,AipNlp

#APPID AK SK信息
APP_ID = ""
API_KEY = ""
SECRET_KEY = ""

client = AipSpeech(APP_ID,API_KEY,SECRET_KEY)#实例化将语音转换成文字
nlp_client = AipNlp(APP_ID,API_KEY,SECRET_KEY)#实例化匹配相似度

# 读取语音文件
def get_file_content(filePath):
    cmd_str = f"ffmpeg -y  -i {filePath}  -acodec pcm_s16le -f s16le -ac 1 -ar 16000 {filePath}.pcm"
    os.system(cmd_str)
    with open(f"{filePath}.pcm", 'rb') as fp:
        return fp.read()

# 识别处理本地文件返回包含文字信息的字典
res = client.asr(get_file_content('wyn.m4a'), 'pcm', 16000, {
    'dev_pid': 1536,
})

Q = res.get("result")[0] #取出字典中的文字信息

if nlp_client.simnet(Q,"你的名字叫什么").get("score") >= 0.6:
    A = "my name is xxx"
else:
    tl_url = "http://openapi.tuling123.com/openapi/api/v2"#设置图灵api接口
    tl_data = {
        "perception": {
            "inputText": {
                "text": "北京今天天气怎么样"
            }
        },
        "userInfo": {
            "apiKey": "",#设置apikey
            "userId": ""#自定义设置userid，任意
        }
    }
    tl_data["perception"]["inputText"]["text"] = Q
    res = requests.post(tl_url,json=tl_data)
    res_json = res.json()
    A = res_json.get("results")[0].get("values").get("text")

result = client.synthesis(A,"zh",1,{"vol":5,"spd":4,"pit":6,"per":4,})

if not isinstance(result,dict):
    with open("Answer.mp3","wb") as f:
        f.write(result)
else:
    print(result)
os.system("ffplay Answer.mp3")
```

### 14.mongoDB

MongoDB简介

​	MongoDB是一个基于分布式文件存储的数据库，由c++语言编写，是一个介于关系数据库和非关系数据库直接的产品，是非关系数据库当中最丰富的，最像关系数据库的。它支持的数据结构非常松散，类似json的bson格式，因此可以存储比较复杂的数据类型。Mongo最大的特点是它支持的查询语言非常强大，其语法有点了类似于面向对象的查询语言，技术可以实现类似关系数据库单表查询的绝大部分功能，而且还支持对数据建立索引。

​	NoSQL不仅仅只是SQL文件型数据库

​	MongoDB - NoSQLBooster4MongoDB

​	MongoDB启动 - cmd - 默认27017监听端口   MySQL（3306）   Redis（6379）

```python
MongoDB - 启动MongoDB服务
数据库存放路径，默认： C:/data/db or /data/db
    --dbpath = "D:/data/db" 更改数据库存放路径
    --port 更改监听端口
```

MongoDB的使用

```python
MongoD客户端连接MongoDB服务
mongo  启动客户端
show databases 查看当前服务器中的数据库
use dbname 切换当前使用的数据库
db 查看当前使用的数据库
show tables 查看当前数据库活动的数据表

use 不存在的数据库名 会在内存创建数据库空间
show databases 查看当前磁盘中的数据库

db(内存中的).tablename 在内存中创建数据表空间
show tables 查看在当前数据库中磁盘上的数据表

当数据表中存在数据，数据库及数据表均会写入到磁盘中
```

增删改查

- 增

  ```sql
  db.tablename.insert({})
  db.users.insert({name:"username"})
  ```

  官方推荐

  ```sql
  db.tablename.insertOne({name:"username"})增加一条数据
  db.tablename.insertMany([{name:"name1"},{name:"name2"}]) 增加多条数据
  ```

- 简单查

  ```sql
  db.tablename.find() 查找当前数据表中的所有数据
  { "_id" : ObjectId("5d50bdd1399ab50efdf765de") }
  { "_id" : ObjectId("5d50be7c399ab50efdf765df"), "name" : "name" }
  
  db.tablename.findOne({}) 查询符合条件的第一条数据
  db.tablename.find({}) 查询符合条件的所有数据
  ```

- 高级查

  ```sql
  db.users.find({age:20}) 查询所有age=20的数据
  db.users.find({age:20,name:"tony"}) 并列条件
  db.users.find({"$and":[{name:"tony"},{age:20}]}) $and的使用 并列
  db.users.find({"$and":[{name:"tony"},{age:20}]}) $or 或
   db.users.find({"age":{"$in":[999,1000]}}) age结果为999或1000
   db.users.find({"hobby":{"$all":[5,1,3]}}) $all子集 必须是子集，交集不可以
   
   如果查询 _id 那么 数据的值必须带上 ObjectId 的数据类型
  db.Users.find({"_id":ObjectId("5d50e778b2a72712f5ee54c5")})
  
  Object查询时可以直接使用 对象.属性 的方式作为Key
  ** 当Array中出现了Object 会自动遍历 Object中的属性 
  ```

- $修改器

  ```sql
  db.users.update({name:"name"},{"$set":{name:"changedname"}})
  db.tablename.update({查询符合条件的数据},{$修改器:{修改的内容}})   修改符合查询条件的第一条数据
  ```

  ```sql
  单个字段 进行修改
  $set 强制将某值修改
  db.users.update({name:"tony"},{"$set":{age:16,gender:1}})
  
  $unset 强制删除一个字段
  db.users.update({age:73},{"$unset":{gender:0}})
  
  $inc 引用增加
  db.users.update({name:"DSB"},{"$inc":{age:1}})
  db.users.update({name:"DSB"},{"$inc":{age:-2}})
  
  针对$array的修改器 以及 $关键字的用法和特殊性
  Array是list 列表类型 
  list -> append remove pop extends
  Array -> $push $pull  $pop $pushAll $pullAll
  $push 在array中追加数据 
  db.users.update({name:"MJJ"},{"$push":{"hobby":"抽烟"}})
  
  $pushAll 在Array中批量增加数据 [] == list.extends
  db.users.update({name:"MJJ"},{"$pushAll":{"hobby":["喝酒","烫头"]}})
  
  $pull 删除符合条件的所有元素
  db.users.update({name:"MJJ"},{"$pull":{"hobby":"烫头"}})
  
  $pop 删除array中第一条或最后一条数据
  db.users.update({name:"MJJ"},{"$pop":{"hobby":1}}) 正整数为 删除最后一条数据
  db.users.update({name:"MJJ"},{"$pop":{"hobby":-1}}) 负整数为 删除第一条数据
  
  $pullAll 批量删除符合条件的元素 []
  db.users.update({name:"MJJ"},{"$pullAll":{"hobby":[3,4,5]}})
  
  $关键字 的特殊用法
  储存符合条件的元素下标索引 - 只能存储一层遍历的索引位置
  $ = 9
  db.users.update({"hobby":10},{"$set":{"hobby.$":0}})
  
  官方推荐:
  db.tablename.updateOne({},{}) # 修改符合条件的第一条数据
  db.tablename.updateMany({},{}) # 修改符合条件的所有数据
  ```

- 删

  db.Tables.remove({})删除所有符合条件的数据

  ```sql
  db.tablename.deleteOne({查询条件}) 删除符合条件的第一条数据
  db.tablename.deleteMany({查询条件}) 删除所有符合条件的数据
  ```

- 比较

  ```sql
  $gt		大于
  $lt		小于
  $gte	大于等于
  $lte	小于等于
  $eq		等于 : 是用在 "并列" "或者" 等条件中
  $ne		不等于 不存在和不等于 都是 不等于
  ```

- 数据类型：

  ```sql
  ObjectID ：Documents 自生成的 _id # 自增ID 全世界唯一的计数ID
  String： 字符串，必须是utf-8
  Boolean：布尔值，true 或者false (这里有坑哦~在我们大Python中 True False 首字母大写)
  Integer：整数 (Int32 Int64 你们就知道有个Int就行了,一般我们用Int32)
  Double：浮点数 (没有float类型,所有小数都是Double)
  Arrays：数组或者列表，多个值存储到一个键 (list哦,大Python中的List哦)
  Object：如果你学过Python的话,那么这个概念特别好理解,就是Python中的字典,这个数据类型就是字典
  Null：空数据类型 , 一个特殊的概念,None Null
  Timestamp：时间戳
  Date：存储当前日期或时间unix时间格式 (我们一般不用这个Date类型,时间戳可以秒杀一切时间类型)
  看着挺多的,但是真要是用的话,没那么复杂,很简单的哦
  ```

- MongoDB中的概念

  ```sql
  使用了不存在的对象及创建该对象
  json结构存储
  MySQL       MongoDB
  database    database
  Table       Collections
  Colum       Field
  Row         Documents
  ```

- 高级函数：

  排序     筛选    跳过

  sort      limit     skip

  ```sql
  排序:
  db.users.find({}).sort({age:-1}) 依照age字段进行倒序
  db.users.find({}).sort({age:1}) 依照age字段进行正序
  
  筛选:
  db.users.find({}).limit(4) 筛选前4条数据
  
  跳过:
  db.users.find({}).skip(3) 跳过前3条数据 显示之后的所有数据
  
  分页
  1.排序 2.跳过 3.筛选
  
  page = 页码 = 1
  count = 条目 = 2
  
  db.users.find({}).limit(2).skip(2).sort({ age:-1 })
  
  limit = count
  skip = (page-1) * count
  
  db.users.find({}).limit(count).skip((page-1)*count).sort({ age:-1 })
  ```

- pymongo的使用

  ```python
  from  import
  ```

  

15.手机app开发

软件安装

- 安装夜神模拟器

  设置为手机版

- 安装HbuildX

HbuildX的使用

收藏页面"www.html5plus.org"

- 创建新展示项目

  ```html
  新建项目  选择5+app  （HTML5plus） 选择模板使用hello mui
  若使用的是夜神模拟器，则在hbuilderX中工具下设置运行配置中修改模拟器端口为62001
  ```

- 创建手机项目

  ```html
  常用命令http://dev.dcloud.net.cn/mui/snippet/
  新建项目 选择5+app  模板选择mui
  index页面内将页面全部清除，输入mdo（mui -dom结构）回车
  mbo(mbody主体)
  msl   轮播图mGallery
  mg    九宫格mgrid
  mli    列表mlist
  ```

  











































