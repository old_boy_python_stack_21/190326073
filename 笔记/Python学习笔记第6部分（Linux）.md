Linux

:blonde_woman:

:blonde_man:

:man:

Unix家谱

![1564385051430](C:\Users\xiaohui\AppData\Roaming\Typora\typora-user-images\1564385051430.png)

Linux目录

![1564385743844](C:\Users\xiaohui\AppData\Roaming\Typora\typora-user-images\1564385743844.png)

### 1.Linux常见发型版本

- Ubuntu
- Redhat
- Fedora
- openSUSE
- Linux mint
- Debian
- manjaro
- mageia
- centos
- arch

### 2.安装centos

- 直接在机器上安装
- 双系统
- 通过虚拟软件在Windows上安装Linux

### 3.虚拟软件

- VMware
- VBox
- Mac

设置虚拟机上的网络连接方式：

​	桥接：跟当前Windows机器使用同一个网段的ip地址

​	nat：不会跟当前Windows机器获取同一个网段的ip地址

电脑32位与64位：

​	32位：2^32 bit

​	64位：2^64 bit

运维开发的电脑密码要求：

- 12位及其以上
- 必须包含大写字母，小写字母，数字，特殊字符
- 3个月或者半年更换一次

### 4.Linux用户

- root用户
  - 超级管理员
  - 对系统有完全操作的权限
  - 误操作对系统的损害无限大
  - 尽量不要使用root登录操作
- 普通用户
  - 对系统的操作权限有限
  - 损害有限
  - 需要使用普通用户登录

### 5.终端

- 图形终端
- 虚拟终端  Ctrl+alt+F1-6  /dec/tty#
- 物理终端
- 设备终端
- 串行终端
- 伪终端  /dev/pts/#
- tty 查看命令

### 6.远程连接工具

- xshell
- putty
- SecureCRT

终端查看ip地址

```shell
ifconfig  /    ip addr   /     ip a   三种方式
```

- 交互式接口

  启动终端以后，在终端设备上会打开一个接口

  - GUI 图形接口
  - GLI
    - shell
    - powershell

  shell

  用来在Linux系统上的一个接口，用来将用户的输入发送给操作系统去执行，并把得到的结果输出出来

  ```shell
  查看系统支持的shell  cat /etc/shells
  切换shell   chsh -s shell
  查看当前运行的shell echo $SHELL
  ```

- 命令提示符

  ```shell
  [root@localhost ~]# 
  # 超级管理员
  $ 普通用户
  [用户@主机名 目录]命令提示符
  永久生效
  echo 'PS1="\[\e[1;30;36m\][\u@\h \W]\\$\[\e[0m\]"' >> /etc/profile.d/ps.sh 
  写到配置文件中
  [root@centos ~]#cat /etc/profile.d/ps.sh
  PS1="\[\e[1;30;36m\][\u@\h \W]\\$\[\e[0m\]"
  ssh root@192.168.100.128使用xmanager登录主机
  
  ```

- 修改提示符格式

  ```shell
  PS1="\[\e[1;5;41;33m\][\u@\h \W]\\$\[\e[0m\]"
  \e 
  \h 主机名简称
  \w 当前工作目录 \t 24小时时间格式 \! 命令历史数
  \u 当前用户
  \H 主机名
  \W 当前工作目录基名 \T 12小时时间格式
  \# 开机后命令历史数
  1表示字体加粗， 0表示默认字体。4表示给字体加上下划线。5表示字体闪烁。7表示用亮色突出显示，来让你的文字更加醒目
  31表示字符颜色。
  可选颜色：红色、绿色、黄色、蓝色、洋红、青色和白色。他们对应的颜色代码是：30（黑色）、31（红色）、32（绿色）、 33（黄色）、34（蓝色）、35（洋红）、36（青色）、37（白色）
  40表示字符背景色。可选颜色 40、41、42、43、44、45、46、47
  ```

  

- 执行命令

  写完命令后直接回车

  - 内部命令：安装完系统以后自带的命令，通过help来获取内部命令的列表
  - 外部命令：第三方提供的，在某些地方可以直接找到执行文件

  ```shell
  type  查看命令的类型
  which  查找命令的路径
  ```

- alias别名

```shell
alias 直接列出了系统里面所有的别名
alias cdetc='cd /etc' 设置别名
unalias cdetc 取消别名
#让命令一致生效
#对当前用户
[root@localhost ~]#echo "alias cdetc='cd /etc'" >> .bashrc
#对所有的用户都生效
echo "alias cdetc='cd /etc'" >> /etc/bashrc
ls 相当于list
```

- 执行原来本身的命令

  - "ls"
  - \ls
  - 'ls'

  单双引号的区别：""可以直接打印变量的值；''引号里面写什么就打印什么。

- date

  ```shell
  [root@localhost ~]#date
  Mon Jul 29 12:18:14 CST 2019
  [root@localhost ~]#date +%F
  2019-07-29
  [root@localhost ~]#date +%H（24小时制）
  12
  [root@localhost ~]#date +%I（12小时制）
  12
  [root@localhost ~]#date +%y
  19
  [root@localhost ~]#date +%m
  07
  [root@localhost ~]#date +%d
  29
  [root@localhost ~]#date +%M
  22
  [root@localhost ~]#date +%S
  25
  [root@localhost ~]#date +%a
  Mon
  [root@localhost ~]#date +%A
  Monday
  [root@localhost ~]#date +%T
  12:23:31
  [root@localhost ~]#date +%y-%m-%d
  19-07-29
  [root@localhost ~]#date +%Y-%m-%d
  2019-07-29
  unix元年
  [root@localhost ~]#date +%s 时间戳
  1564374331
  [root@localhost ~]#date +%W 一年中的多少周
  30
  ```

- 时区

  ```shell
  [root@localhost ~]#timedatectl
  [root@localhost ~]#timedatectl set-timezone  Asia/Shanghai
  ```

- 日历

  ```shell
  cal 展示当月的日历
  cal -y 展示当年的日历
  cal -y # 显示#年的日历
  ```

- 关机重启

  ```shell
  shutdown 默认是一分钟之后关机
  shutdown -c 取消
  shutdown -r 重启
  TIME
  	- now  立即
  	hh：mm
  	+# 表示多长时间以后重启
  reboot 重启
         -p 切断电源
  init 6 重启
  init 0 关机
  poweroff 关机
  ```

- 命令的格式

  ```shell
  command [options] [args...]
  选项：启用或者禁用某些功能的
  	短选项：-a
  	长选项：--all
  参数：命令的作用对象，一般情况是目录，用户等等
  注意：
  	多个选项及参数和命令之间需要用空格隔开
  	ctrl+c来取消命令的执行
  	用；来隔开同时执行的多个命令
  	使用\来将命令切换成多行
  ```

  

### 7.Linux常用命令1

- 命令补全

  tab键

  内部命令：

  外部命令：shell会根据环境变量从左至右一次查找，找到第一个匹配的则返回。

  - 如果给定的字符串只能搜索到一个的话，则直接显示
  - 若给定的字符串搜索到多个，则需按两次tab键

  目录补全

  - 把用户给定的字符串当做路径的开始部分来搜索
    - 如果只搜索到一个则直接显示，直接tab
    - 若搜索到多个，列出一个列表，让用户选择，需按两次tab键获取列表

- echo回显

  输入什么就输出什么，并且假如一个换行符

- 获取环境变量

  ```shell
  [root@localhost ~]#echo $PATH
  /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
  ```

- 命令历史

  可以通过键盘上的箭头来查找之前执行过的命令

  通过history来获取之前执行过的命令记录

  执行刚才执行过的命令

  ```python
  一：键盘上的向上箭头  二：!!   三：!-1    四：Ctrl+P
  ```

  使用!:0来执行上一条命令（去掉参数）

  !num来执行第多少条命令

  ```shell
  [root@localhost ~]#!123
  which cp
  alias cp='cp -i'
  	/usr/bin/cp
  ```

  !string直接查找最近的一条包含string的命令

  Ctrl+r搜索之前执行过的命令    Ctrl+g来退出来搜索

  调用上一个命令的参数

  - esc.

  history的命令历史

  ```shell
  history num   显示最后num条的命令
  history -c  清空命令记录
  ```

  当用户登录系统之后，会读取家目录下的.bash_history文件

  正常退出，会将之前执行过的命令写入到文件中

- 快捷键

  ctrl+l 清屏，相当于clear

  ctrl+s 锁定屏幕

  ctrl+q 解开锁定

  ctrl+c 总之命令

  ctrl+a 移动到命令的行首 相当于home

  ctrl+e 移动到行尾，相当于end

  ctrl+xx 光标在命令行首和光标之间来回移动

  ctrl +k  删除光标到结尾位置的字符

  ctrl+u 删除光标到行首的字符

  alt+r 删除正行

  需要注意，alt会跟别的快捷键冲突

- 帮助信息

  内部命令

  - help command
  - man bash

  外部命令

  - command --help

  - command -h

  - man command q退出

  - 官方文档

    ```shell
    Usage: date [OPTION]... [+FORMAT]
      or:  date [-u|--utc|--universal] [MMDDhhmm[[CC]YY][.ss]]
      [] 代表可选
      ... 表示一个列表
      [-u|--utc|--universal] 任选其中一个
      -lh 代表-l -h
      date 052805271980 设置时间
    ntpdate time.windows.com  自动与时间服务器同步时间
    ```

- man

  箭头来控制输出；回车输出下一行；空格切换到下一屏

  章节：1.用户的命令 2.系统的调用  3.c库的调用 4.设备文件或者特殊文件 5.配置文件 6.游戏  7.杂项  8.管理类的命令 9.Linux内核api

- 目录结构

  目录结构是一个倒置的树

  目录从‘/’开始

  目录严格区分大小写

  隐藏文件以.开头

  路径的分隔符是/

- 文件命名规范

  文件名最长255个字符

  包括路径在内最长4096个字符

  除了/和null以外，其他的字符都生效

  名称大小写敏感

- 颜色的表示

  蓝色 表示目录

  绿色 表示可执行文件

  红色 表示压缩文件

  蓝绿色 链接文件

  白色 普通的文件

  灰色 其他文件

- 文件系统结构

  /boot 存放系统的引导文件，内核文件、引导的加载器放在该目录

  /bin 所有的用户都可以使用的一些基本命令

  /sbin 管理员可以使用的命令，管理类命令

  /lib 基本的一些库文件（windows 是.dll linux是.so）

  /lib64 专门用于64位操作系统的一些辅助库文件

  /etc 配置文件目录

  /home/Username 普通用户的家目录

  /root 超级管理员的家目录

  /media 便携式移动设备挂载点

  /opt 第三方的安装程序

  /srv 系统上允许的服务用到的数据

  /tmp 存放临时文件的目录

  /usr 存放安装程序

  /var 存放经常变化的数据，比如日志

  /proc 用来存放内核和进程相关的虚拟文件

  /dev 用来存放设备的

  /mnt 临时文件挂载

  /run 服务或者系统启动以后生成的文件

  /sys 存放的是硬件设备相关的虚拟文件

- 程序组成部分

  二进制

  - /bin
  - /sbin
  - /usr/bin
  - /usr/sbin
  - /usr/local/bin
  - /usr/local/sbin

  库文件

  - /lib
  - /lib64
  - /usr/lib
  - /usr/lib64
  - /usr/local/lib
  - /usr/local/lib64

  配置文件

  - /etc
  - /etc/directory
  - /usr/local/etc

  帮助文件

  - /usr/share/man
  - /usr/share/doc
  - /usr/local/share/man
  - /usr/local/share/doc

- 相对路径绝对路径

  绝对路径：从根开始完整的路径

  相对路径：相对于某个文件或目录，..父级目录，.代表当前路径

- 获取文件名和文件目录

  ```shell
  [root@localhost network-scripts]#basename /etc/sysconfig/network-scripts/ifcfg-ens33
  ifcfg-ens33
  [root@localhost network-scripts]#dirname /etc/sysconfig/network-scripts/ifcfg-ens33
  /etc/sysconfig/network-scripts
  ```

- cd

  change directory，可以使用相对路径，也可以使用绝对路径

  ```shell
  [root@localhost etc]#cd
  [root@localhost ~]#
  [root@localhost ~]#cd - 可以快速的回到上一次的目录
  /etc/sysconfig/network-scripts
  [root@localhost network-scripts]#cd -
  /root
  ```

- pwd

  print working directory

  ```shell
  [root@localhost etc]#pwd
  /etc
  [root@localhost etc]#cd sysconfig/network-scripts/
  [root@localhost network-scripts]#pwd
  /etc/sysconfig/network-scripts
  ```

- ls

  list 列出指定目录的文件或文件夹

  语法：ls [OPTION]...[FILE]...

  ```shell
  ls -a  列出所有的文件（包括隐藏文件）
  ls -l =ll 使用长格式来显示文件相关信息
  ls —R 递归显示
  ls -d 显示目录本身
  ls -1（数字1） 竖着显示文件
  ls -S 根据文件的大小来排序
  ls -lSr 升序排序
  ls -d */ 显示当前目录下的目录，不能指定目录
  ls -h 以人类易读的方式显示
  ls -ltu 按照atime时间排序
  ```

- touch

  创建空文件修改文件的时间戳

  ```shell
  touch 用来修改时间和创建文件
  如果文件存在的话，则修改时间
  如果不存在，则创建文件
  ```

- 命令的展开

  ```shell
  a{1..10}   命令展开
  a{1..10..2} 指定步长
  seq 1 10 
  seq 1 2 10
  ```

- 命令的引用

  ```shell
  `date`
  $(date)
  ```

- 文件通配符

  *代表零个或者多个字符

  ？代表任意的一个字符

  ~代表家目录

  代表数字

  ```shell
  一：[0-9]     二：[:digit:]
  ```

  [:lower:]小写

  [:upper:]大写

  [abcdef]表示其中的任何一个

  [a-z]字母从a-z并且包括A-Y

  [A-Z]字母，从A-Z并且包括b-z

  a`[^abcd ]`取反

  所有字母

  ```shell
  一：a-zA-Z     二：[:alpha:]
  ```

  任意字母或数字

  ```shell
  一：a-zA-Z0-9     二：[:alnum:]
  ```

- stat查看文件状态

  ```shell
    File: ‘aa’
    Size: 0         	Blocks: 0          IO Block: 4096   regular empty file
  Device: fd00h/64768d	Inode: 19864315    Links: 1
  Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)
  Context: unconfined_u:object_r:admin_home_t:s0
  Access: 2019-07-30 11:57:02.279384871 +0800
  Modify: 2019-07-30 11:57:02.279384871 +0800
  Change: 2019-07-30 11:57:02.279384871 +0800
   Birth: -
  访问时间：access      读取文件内容  atime
  修改时间：Modify      改变文件的内容 mtime
  改变时间：change      改变文件的内容 ctime
  ```

- 复制文件和文件夹

  ```shell
  Usage: cp [OPTION]... [-T] SOURCE（源文件） DEST（目标文件）
    or:  cp [OPTION]... SOURCE... DIRECTORY
    or:  cp [OPTION]... -t DIRECTORY SOURCE...
  
  ```

  如果source是一个文件的话

  - 如果目标不存在，新建一个目标文件，滨江数据写入到目标文件里面
  - 如果目标文件存在
    - 如果目标文件是一个目录，直接在目标目录下面新建一个跟源文件同名的文件，并将数据目标文件写入到文件
    - 如果目标文件夹是一个文件夹，直接覆盖，为了安全起见，建议cp配合-i使用

  如果源文件是多个文件的话

  - 目标文件如果是文件的话，则直接报错
  - 如果目标文件是一个目录的话，则直接复制进目录

  如果源文件是目录的话

  - 如果目标不存在，则创建指定的目录，必须-r选项
  - 如果说目录存在
    - 如果目录是文件夹的话会报错
    - 如果目录是一个目录的话，则在目录下面创建一个新的同名目录，并把文件复制过去

  常用参数

  ```shell
  -i 覆盖之前提示
  -n 不覆盖
  -r 递归复制，复制目录及目录下的所有文件
  -f 强制
  -v 显示复制过程
  -b 在覆盖之前，对源文件做备份
  cp  --backup=numbered 1.cfg 2.cfg 覆盖文件，备份文件添加上数字
  -p 保留原来的属性
  ```

- 移动或重命名

  ```shell
  Usage: mv [OPTION]... [-T] SOURCE DEST
    or:  mv [OPTION]... SOURCE... DIRECTORY
    or:  mv [OPTION]... -t DIRECTORY SOURCE...
  -i  交互式
  -f  强制
  -b  覆盖前做备份
  -v 显示进度
  ```

- 删除

  ```shell
  rm [OPTION]... FILE...
  -i 交互式
  -f 强制删除
  -r 递归删除
  rm -rf /* 慎用
  rm -rf /* 慎用
  rm -rf /* 慎用
  cd /
  rm -rf *
  ```

### 8.Linux常用命令2

- mkdir

```shell
mkdir s21
mkdir s21-{3..10}
mkdir -p a/b/c/d
mkdir -pv {s13,s14}/{ss11,ss12}/{sss11,sss12} 
-p 递归创建
-v 显示创建过程
```

- tree

```shell
yum install -y tree
tree name
-L 控制显示的层数
-b 只显示目录
```

- 文件类型

  \- 表示文件

  d 表示目录

  l 表示链接

  b 块设备

  c 字符设备

  s 表示socket套接字

- rmdir

  只能删除空目录

  ```shell
  mkdir -p s15/s16/s17/s18
  rmdir -p s15/s16/s17/s18
  ```
  

若要删除不为空的文件文件夹使用 rm -rf  *  (r递归 f强制 *通配所有)

- 链接

  硬链接

  - ln 源文件 目标文件
  - 源文件发生改变，目标会发生改变
  - 将硬盘的引用次数+1
  - 删除
    - 将磁盘的引用次数-1
    - 源文件删除对目标不会受影响
  - 不能对目录做硬链接
  - 不能跨越分区

  软链接

  - 相当于Windows的快捷方式
  - ln -s可以生成软链接
  - 链接大小就是指定的源文件的字符数
  - 源文件发生改变，目标会发生改变
  - 删除
    - 源文件删除目标会受影响
  - 可以对目标目录做软链接
  - 可以跨区域分区

- 文件权限

  ```shell
  drwxr-xr-x. 4                          root root 30 Jul 31 08:55 s12
  	权限     在磁盘上的引用次数（硬链接）    属主  属组 大小  atime      文件名 
  ```

- 输入输出

  输入：

  - 标准输入：接收来自键盘的输入 stdin 0

  输出：

  - 标准输出：默认输出到终端 stdout 1
  - 错误输出：默认输出到终端  stderr 2

- 分文件输出

```shell
ls b bbbb > info.log 2> error.log
```

- 合并输出

  &> 写入会覆盖

  &>> 写入不会覆盖会增加

  command>info.log 2>&1 将错误输出及标准输出写入到info.log覆盖

  command>info.log 2>>&1 将错误输出及标准输出写入到info.log不覆盖

  /dev/null 无限接收

  (ls ;ls /bbb) &> a 多个命令的合并

- tr替换或者删除字符

```shell
tr 'a-z' 'A-Z' </etc/issue
[root@localhost jiangyi]#tr ab 12
ab
12
[root@localhost jiangyi]#tr abc 12  如果后面的位数不足的话，则用最后一位补齐
abc
122
ab
12
tr -d abc < issue > issue2 从定向不能为原来的文件，如果说为原来的文件，则文件清空
-t 用来截断
[root@localhost jiangyi]#tr -t abcd 12
abcd
12cd
cd
cd
-s 压缩 去重
[root@localhost jiangyi]#tr -s abc
abc
abc
aaabbbccccccccccccccccccccccccccccc
abc
-c 取反
[root@localhost jiangyi]#tr -sc abc
aaaabbbbbbbbcccccccccccccc
aaaaaaabbbbbbbcccccccccccccc
aaabbbccccddddeeff
aaabbbbccccdef
aaabbbbbcccc1122333
aaaabbbbbbbbbbccc123
[root@localhost jiangyi]#tr -dc abc
aaaaaaaaaaaaabbbbbccccccdddddddddwqweqwqwqwq
wqqqqqqqqqqqqqqqqqqqqqqqqq
ctrl+d结束
[root@localhost jiangyi]#tr -dc "abc\n"
adsada
aaa
sadasdcxzczx
aacc
asdadwq
aa
[root@localhost jiangyi]#seq 1 10 >f1
[root@localhost jiangyi]#tr -d "\n" <f1
[root@localhost jiangyi]tr "\n" " "<f1
[root@localhost jiangyi] tr " " "\n" <f2
```

- 进制覆盖

  ```shell
  set -C 禁止覆盖
  set +C 允许覆盖
  ```

- 多行输入

  ```shell
  [root@localhost jiangyi]#cat >f1 <<EOF
  > 1
  > 2
  > 3
  > 4
  > 5
  > 6
  > 7
  > 
  > 8
  > 9
  > EOF
  [root@localhost jiangyi]# cat > f4
  asdas
  sad
  asd
  ctrl+d结束 ctrl+c也可以
  两者区别
  第一种方式输出结束，文件才会产生
  第二方式，回车一次就会写入文件
  EOF 约定俗成
  ```

- 管道

  使用'|'来连接多个命令

  命令1|命令2|命令3|...

  - 将命令的stdout发送给命令2的stdin，将命令2 的stdout命令发送给命令3的stdin...

  - stderr默认是不能通过管道传递

    ```shell
    [root@localhost jiangyi]#ls /dadadasda|tr -s "a-z" "A-Z"
    ls: cannot access /dadadasda: No such file or directory
    [root@localhost jiangyi]#ls|tr "a-z" "A-Z"
    
    ```

- 查看当前的登录的用户信息

  ```shell
  whoami  获取登录的用户
  [jiangyi@localhost ~]$who am i
  jiangyi（用户）  pts/4（登录的终端）        2019-07-31 08:27（登录的时间） (192.168.182.1（登录ip地址）)
  w 可以查看当前登录的所有用户执行的命令
  ```

- 文件权限

  chown change owner

  ```shell
  Usage: chown [OPTION]... [OWNER][:[GROUP]] FILE...
    or:  chown [OPTION]... --reference=RFILE FILE...
  chown jiangyi d  修改属主
  chown jiangyi:jiangyi d 修改属主和属组
  chown root.root d
  chown :jangyi d 只修改属组信息
  chown -R jiangyi a 递归修改目录下的所有文件
  chown --reference=b f3 指定源文件
  ```

  chgrp

  ```shell
  chgrp jiangyi b
  chgrp --reference=b f3 指定源文件
  ```

  权限

  ```shell
  rwxr-xr-x
  三位一组
  属主   属组   其他
   u      g     o
  r read可读取权限
  w write可写的权限
  x excut执行的权限
  ```

  文件

  - r 可以查看
  - w 可以修改
  - x 可以直接执行

  文件夹：

  - r 可以使用ls查看 可以cd进去
  - w 可以在其中创建文件或文件夹，可以删除文件夹中的文件或文件夹
  - x  可以cd，如果没有x权限的话，w权限不会生效，r权限可以查看有哪些权限

  chmod

  - 可以直接用+-来操作

    - 可以用[u|g|o]+-=r w x
    - 可以什么都不写，表示全部+-

  - 还可以用数字表示

    \---

    r--   100 4

    -w- 010 2

    --x 001  1

    r:4

    w:2

    x:1

    r-xr-x---

  - 建议：

    - 不要给文件或者文件夹设置成777权限

  - 特殊权限

    ```shell
    chattr +i 不能删除、改名、不能修改内容
    chattr +a 只能追加，不能删除，不能改名
    lsattr 查看属性
    ```

- 文本处理工具

  cat

  ```shell
  Usage: cat [OPTION]... [FILE]...
  -E 显示行的结束符号$
  -n 显示每一行的行号
  -b 给非空行编号
  -s 折叠空行为一行
  ```

  tac到续显示文件内容

  less分屏显示

  - 可以分屏显示
  - 空格下一屏 回车下一行
  - /来搜索
  - n向下搜索 N向上搜索
  - q退出

  more

  - 可以分屏显示
  - 空格下一屏 回车下一行
  - -d 显示翻页和退出信息
  - 输出完后自行退出

  head 显示前多少行默认是10行

  - -num显示前多少行的数据

  tail显示后面多少行默认是后10行

  - -num显示后多少行的数据
  - -f追踪显示文件新加入的内容，常用语日志的追踪
  - tailf 相当于tail -f命令

  cut抽取文件

  <https://www.cnblogs.com/longjshz/p/5792502.html>

  ```shell
  -b ：以字节为单位进行分割。这些字节位置将忽略多字节字符边界，除非也指定了 -n 标志。
  -c ：以字符为单位进行分割。
  -d ：自定义分隔符，默认为制表符。
  -f  ：与-d一起使用，指定显示哪个区域。
  -n ：取消分割多字节字符。仅和 -b 标志一起使用。如果字符的最后一个字节落在由 -b 标志的 List 参数指示的<br />范围之内，该字符将被写出；否则，该字符将被排除。
  # 显示指定的数据
  #，#，#，# 离散数据
  #-# 连续数据
  cut -d: -f3 passwd 
  cut -d: -f1,3-7 passwd 
  cut -d: -f3,4,5,6 passwd
  cut -d: -f3-6 passwd 
  -c 按照字符切割
  cut -c2-5 passwd
  ```

  paste合并文件

  ```shell
  -d 用来指定合并的符号，默认的是tab
  -s 把所有的行合并成一行显示
  ```

- 分析文件的工具

  wc word count

  ```shell
  [root@localhost jiangyi]#wc passwd 
    44   87 2301 passwd
   行数   单词个数  字节数  文件
   -l 统计行的个数
   -w 统计单词的个数
   -c 统计字节的个数
   -m 统计字符的个数
   -L 显示最长一行的长度
  ```

  sort排序

  ```shell
  默认按照字母
  -n 按照数字来排序
  -r 按照倒叙来排序
  -R 随机排序
  sort -t: -nk4 passwd 切割以后在排序
  -t 指定切割符号
  -k 指定按照第几行排序
  ```

  uniq删除重复的行

  ```shell
  -c 显示重复出现的次数
  -d 只显示重复的行
  -u 只显示不重复的行
  连续且完全一样的才是重复
  ss -tnp|cut -d: -f2|tr -s " "|cut -d" " -f2|sort -n|uniq -c
  ```

  diff对比两个文件

  ```shell
  [root@localhost jiangyi]#echo "abc" >b
  [root@localhost jiangyi]#echo "abcd" >d
  [root@localhost jiangyi]#diff b d
  1c1
  < abc
  ---
  > abcd
  [root@localhost jiangyi]#echo "abcde" >b
  [root@localhost jiangyi]#diff b d
  1c1
  < abcde
  ---
  > abcd
  [root@localhost jiangyi]#echo "abcde" >> b
  [root@localhost jiangyi]#diff b d
  1,2c1
  < abcde
  < abcde
  ---
  > abcd
  [root@localhost jiangyi]#echo "abcd" >> b
  [root@localhost jiangyi]#diff b d
  1,2d0
  < abcde
  < abcde
  ```

### 9.Linux常用命令3

文件操作vi   vim

- 打开文件

  ```shell
  vim [options] file
  +# 打开文件以后光标在#行的行首
  +/str 直接定位到第一个被匹配到的strg的行的行首
  -m  只读模式打开
  -e  直接进入扩展命令行模式
  -b 以二进制方式打开文件
  ```

- 模式

  命令模式：打开后默认

  插入模式：编辑文件

  扩展命令模式（末行模式）：保存、退出等待

  可视化模式

  esc键 退出当前模式

  ![ms](http://linux.imock.club/images/command/vimms.png)

- 模式转换

  命令模式 --》 插入模式

  - i在当前光标的所在位置插入
  - l在当前行的行首位置开始插入
  - a在光标的后面开始插入
  - A在当前行的行尾位置开始插入
  - o在当前光标所在行的下方打开一个新行
  - O在当前光标所在行的上方打开一个新行

  插入模式 --》 命令模式

  - esc

  命令模式 --》 扩展命令模式

  - ：

  扩展命令模式 --》 命令模式

  - esc enter
  - esc esc

- 关闭文件

  扩展命令行模式

  - :q 退出
  - :q! 不保存强制退出
  - :wq 保存退出
  - :wq! 强制保存退出
  - :x 保存退出

  命令模式

  - ZZ 保存退出
  - ZQ 不保存退出

- 扩展命令行模式

  ':'进入扩展命令行模式

  命令

  - w 写入磁盘
  - wq 保存退出
  - x 保存退出
  - q 退出
  - r filename 读入一个文件，在光标的下一行插入
  - w filename 将当前的文件另存在另一个文件
  - !command 执行命令，然后回车回到文件中
  - r!command 直接将命令的执行结果写入到文件中，光标所在行的下一行

- 光标的移动

  字符键移动

  - h 左移动
  - l 右移动
  - j 往下移动
  - k 往上移动
  - num command 多个字符间跳转

  单词间移动（除了下划线都认为是单词的分隔符）

  - w 下一个单词的词首
  - e 当前单词或者下一个单词的词尾
  - b 当前单词或者上一个单词的词首
  - num command 多个单词之间进行跳转

  当前页跳转

  - H 当前页的第一行
  - L 当前页的最后一行
  - M 当前页面的中间位置
  - zt 将光标所在的行移动到屏幕的顶端
  - zb 将光标所在的行移动到屏幕的底端
  - zz 将光标所在的行移动到屏幕的中间位置

  行首行尾跳转

  - 0（数字）跳转到行首
  - $ 跳转到行尾
  - ^ 跳转到行首的第一个非空白字符

  行间跳转

  - gg 回到第一行
  - 1G 回到第一行
  - G 跳转到最后一行
  - 扩展命令模式下：num 跳转到对应的num行 
  - numG 跳转到指定的num行

  段落间跳转

  - } 下一段
  - { 上一段

  翻屏

  - Ctrl+f 向下翻一屏
  - Ctrl+b 向上翻一屏
  - Ctrl+d 向下翻半屏
  - Ctrl+u 向上翻半屏

- 命令行的操作

  字符编辑

  - x 删除光标所在的位置
  - #x 删除#个字符
  - xp 交换位置
  - p 粘贴到光标所在的位置
  - ~大小写转换
  - J 删除当前行后的换行符

  替换

  - r 替换光标所在的位置
  - R 切换到替换模式

  删除

  - d 删除，需要结合光标跳转字符
  - d0 删除到行首
  - d$ 删除到行尾
  - d^ 删除到非空行首
  - dw 删除一个单词

  - de 删除当前单词到词尾的位置或者下一个单词
  - db
  - dd 删除光标所在的行
  - #dd 删除#行
  - dG 删除到文件的行尾
  - dgg 删除到文件的行首
  - D 相当于d$

  复制

  - y 复制，需要结合光标跳转字符
  - y$
  - y0
  - y^
  - yw
  - ye
  - yb
  - yG
  - ygg
  - yy 复制整行
  - #yy 复制#行

  粘贴

  - p 如果是整行，则粘贴到当前所在行的下面，否则，则粘贴到光标所在位置的后面
  - P 如果是整行，则粘贴到当前行所在的上面，否则，则粘贴到光标所在位置的前面

  改变命令

  - c 修改之后直接切换到插入模式
  - c$
  - c0
  - c^
  - cb
  - ce
  - cw
  - #
  - cc 删除当前行并且输入新内容
  - #cc 删除#行，并切换到插入模式
  - C 相当于c$

  #[i|I|a|A|o|O]string esc 粘贴#次string

  撤销

  - u 撤销最近的更改
  - #u 撤销之前的#次更改
  - U 撤销光标所在行的关于此行操作
  - ctrl+r 重做最后的操作，撤销撤销
  - . 重复前一个操作
  - #. 重复之前的动作多少次

  查找

  - /pattern 从当前光标所在位置向下查找
  - ?pattern 从光标所在位置向上查找
  - n 同搜索命令
  - N 同搜索命令相反

- 扩展命令模式补充

  地址定界：

  : start, end

  - \# 具体到多少行
  - m，n  从m开始到n结束
  - m，+n，从m开始，到m+n结束 2， +3 第2行到第5行
  - . 代表当前行
  - $ 最后一行
  - $-# 倒数第#+1行
  - % 全部
  - #,/pat/，从第#开始，到第一次被pat匹配到的行
  - /pat1/,/pat2/从第一个别pat1匹配到的行到 第一个被pat2匹配的行
  - /pat/,# 从第一个被pat匹配到的行，到#行

  使用方法：后面跟

  - d 删除
  - y 复制
  - w file ：将范围内的文件另存为一个文件
  - r file 在指定的位置插入文件中的内容

- 查找替换

  :地址定位符s/要查找的内容/要替换的内容/装饰器

  - 要查找的内容：可以使用正则表达式
  - 要替换的内容： 不可以使用正则表达式，但是可以使用\1,\2等，也可以使用&来代替前面的内容
  - 装饰器
    - i 忽略大小写
    - g 全部替换
    - gc 是提前之前要确认
      - y 确认
      - n 跳过
      - a 全部
    - /可以替换成别的符号
      - @
      - #

- 可视化模式

  v 面向字符

  V 面向行

  Ctrl+v 面向的是块

  可以配合键盘移动键使用

  突出显示的文件可以删除、赋值、变更、过滤、替换等等

- 多文件操作

  vim file1 file2 file3

  - :next 下一个
  - :prev 上一个
  - :first 第一个
  - :last 最后一个
  - :wqall
  - :wall
  - :qall
  - 保存之后再切换

- 使用多个窗口

  多个文件切割

  - vim -o|-O  file1 file2 file3
  - -o 水平切割
  - -O 垂直切割
  - ctrl+w 切换不同窗口

  单个文件切割

  - ctrl+w，s 水平分割
  - ctrl+w，v 垂直分割
  - ctrl+w，方向键
  - ctrl+w，q 取消相邻的屏幕
  - ctrl+w，o取消所有的屏幕

- 定制化vim

  配置文件

  - 全局： /etc/vimrc
  - 个人：~/.vimrc

  扩展命令模式

  - 添加行号 set nu 取消 set nonu
  - 搜索时忽略大小写 set ic  取消set noic
  - 自动缩进（与上一行对其） set ai 取消 set noai
  - 搜索高亮 set hls  取消 set nohls
  - 语法高亮 syntax on 关闭 syntax off
  - 设置光标所在的行的标识符 set cul 取消 set nocul
  - 获取帮助 ：set all
  - 文件格式
    - 转换成windows set fileformat=dos
    - 装换成linux set fileformat=unix

- vim帮助信息

  :help

  :help topic

  vimtutor

### 10.Linux常用命令4

文件查找

find实时查找工具，通过便利指定路径完成文件查找

语法

```shell
find [OPTION]... [查找路径] [查找条件] [处理动作]
查找路径：指定具体目录路径；默认为当前目录
查找条件：指定的查找标准，可以为文件名、大小、类型、权限等，默认为查找指定路径下的所有文件
处理动作：对符合条件的文件做操作，默认输出值屏幕
```

- 查找条件

  指定搜索层级

  - -maxdepth level 最大搜索深度，指定目录为第一级

    ```shell
    find -maxdepth 2 -name a
    ```

  - -mindepth level 最小搜索目录深度

    ```shell
    find -mindepth 2 -name a
    ```

  先处理文件夹内的文件，再处理文件夹

  - depth

  根据文件名搜索

  - -name 文件名 支持global *?[]
  - -iname 不区分大小写
  - -link n 链接数为n的文件
  - -regex 'pattern' 以pattern匹配整个文件路径，而非文件名，整个路径必须匹配

  根据属主、属组查找

  - -user username查找属主为指定用户名的文件
  - -group groupname查找属组为指定组的文件
  - -uid userID查找属主为指定的UID的文件
  - -gid grouppid 查找属组为指定gid的文件
  - -nouser 查找没有属主的文件
  - -nogroup 查找没有属组的文件

  根据文件类型查找

  - -type type
    - f 普通文件
    - d 文件夹
    - l 软链接
    - s 套接字
    - b 块设备
    - c 字符设备文件
    - p 管道文件

  空文件或空文件夹

  - -empty

    ```shell
    find / -type d -empty
    ```

  组合条件

  - 与 -a
  - 或 -o
  - 非 -not

  摩根定律

  - （非A）或（非B）=非（A且B）

  - （非A）且（非B）=非（A或B）

    ```shell
    find -not -user aname -a -not -user bname -ls|wc -l
    find -not \( -user aname -o -user bname) -ls|wc -l
    -l 统计有多少行  wc（word count）统计指定文件中的字节数、字数、行数
    ```

  小例：

  ```shell
  find -name snow.png 查找指定名文件
  find -iname snow.png 查找指定文件不区分大小写
  find / -name '*.txt' 查找指定路径下的txt文件
  find /var -name '*log*'查找指定路径下的名含log文件
  find -user joe -group joe 查找主、组为joe的文件
  find -user joe -not -group joe 主为joe，组不是joe
  find -user joe -o -user Jane 或的关系
  find -not \( -user joe -o -user jane \) 非此两个主name
  find / -user joe -o -uid 500 或uid500
  找出/tmp目录下，属主不是root，且文件名不以f开头的文件 
  find /tmp \( -not -user root -a -not -name 'f*' \) -ls
  find /tmp -not \( -user root -o -name 'f*' \) –ls
  ```

  排除目录

  - -path

    ```shell
    查找/etc/下，除/etc/sane.d目录的其它所有.conf后缀的文件
    find /etc -path ‘/etc/sane.d’ -a –prune -o -name “*.conf” 
    查找/etc/下，除/etc/sane.d和/etc/fonts两个目录的所有.conf后缀的文件
    find /etc \( -path "/etc/sane.d" -o -path "/etc/fonts" \) -a -prune -o - name "*.conf"
    命令行的意思是：如果目录/etc/sane.d存在（即-a左边为真），则求-prune的值，-prune 返回真，‘与’逻辑表达式为真（即-path '/etc/sane.d' -a -prune 为真），find命令将在除这个目录以外的目录下查找txt后缀文件并打印出来；如果目录dir0不存在（即-a左边为假），则不求值-prune ，‘与’逻辑表达式为假，则在当前目录下查找所有txt后缀文件
    ```

  根据文件大小来查找

  - -size[+|-] #unit 常用单位：k,M,G,c(byte)
  - #unit :(#-1,#] 不包括前面，包括后面
  - -#unit: [0,#-1] 0-#-1
  - +#unit:(#,….) #到无穷

  根据时间戳

  - 以天为单位
    - -atime [+|-]#   创建时间
      - #:[#,#+1)
      - +#:[#+1,...]
      - -#:[0,#)
    - -mtime 修改时间
    - -ctime  访问时间
  - 以分钟为单位
    - -amin
    - -mmin
    - -cmin

  根据权限

  - -perm[/|-] mode
    - mode 精确权限匹配
    - /mode任何一类对象权限只要有以为匹配即可
    - -mode 每一类必须都同时拥有指定权限

  ```shell
  find -perm 755 会匹配权限模式恰好是755的文件
  • 只要当任意人有写权限时，find -perm /222就会匹配
  • 只有当每个人都有写权限时，find -perm -222才会匹配
  • 只有当其它人(other)有写权限时，find -perm -002才会匹配find -perm 755 会匹配权限模式恰好是755的文件
  • 只要当任意人有写权限时，find -perm /222就会匹配
  • 只有当每个人都有写权限时，find -perm -222才会匹配
  • 只有当其它人(other)有写权限时，find -perm -002才会匹配
  ```

- 处理动作

  -print 默认的处理动作，显示之屏幕

  -ls 类似于查找到的文件执行'ls -l' 命令

  -delete 删除查找到的文件

  -fls file 查找的所有文件的长格式信息保存至指定的文件中

  -ok command {} \; 对查找到的每个文件执行command指定的命令，对于每个文件执行命令之前，都会交互式要求用户确认

  

  -exec command {} \;对查找到的每个文件执行command命令

  - {} 对于引用查找到的文件名本身
  - find 传递查找到的文件至后面指定的命令时，查找到所有的符号条件的文件一次性传递给后面的命令

- 参数替换xargs

  由于很多命令不支持管道来传递参数，而日常工作中有这个必要，所以就有了xargs命令

  xargs用于产生某个命令参数，cargs可以读入stdin的数据，并且以空格符号或者回车符号作为stdin的数据分割

  有些命令不能接受过多参数，命令执行可能会失败，xargs可以借鉴

  ```shell
  echo a{1..100000}|xargs touch 创建十万个文件
  ls f* |xargs rm
  find /sbin -perm +700|ls -1 这个命令时错误的
  find /sbin -perm +7000|xargs ls -1 查找特殊权限的文件
  ```

  find和xargs格式：find|xargs command

- find 实例

  ```shell
  备份配置文件，添加.orig这个扩展名
  find -name “*.conf” -exec cp {} {}.orig \;
  提示删除存在时间超过3天以上的joe的临时文件 
  find /tmp -ctime +3 -user joe -ok rm {} \;
  在主目录中寻找可被其它用户写入的文件
  find ~ -perm -002 -exec chmod o-w {} \;
  查找/data下的权限为644，后缀为sh的普通文件，增加执行权限
  find /data –type f -perm 644 -name “*.sh” –exec chmod 755 {} \;
  查看/home的目录
  find /home –type d -ls
  ```

  

文件处理工具三剑客：

- grep
- sed
- awk

grep 文件过滤工具

grep：Global search REgular expression and Print out the line

作用：文本搜索工具，根据用户指定的'模式'对目标文件逐行进行匹配检查并打印出匹配的行

模式：由正则表达式字符及文本字符锁编写的过滤条件

格式：grep [option] pattern [file...]

其中pattern项需要使用'或者'，如果需要对模式进行转换，则需要使用""，如果不需要进行转换，则使用"或"都可以，模式还可以使用正则表达式来表示。

- grep 命令选项

  ```shell
  --color=auto 对匹配到的文本进行颜色显示
  -v 取反
  -i 忽略大小写
  -n 显示匹配到的行号
  -c 统计匹配到的行数
  -o 仅显示匹配到的字符串
  -q 静默模式，不输出任何信息
  -A num after  后num行
  -B num before 前num行
  -C num context 前后各num行
  -e 实现多个选项的逻辑or关系
  -E 货站的正则表达式
  -r 连带文件夹以下目录也查找
  -w 匹配整个单词
  ```

  小例

  ```shell
  grep 'root' passwd 
   grep -v "root" passwd 
   grep "root" passwd 
   grep -i "root" passwd 
   grep -n "root" passwd 
   grep -ni "root" passwd 
   grep -ci "root" passwd 
   grep -i "root" passwd 
   grep -o "root" passwd 
   grep -oi "root" passwd 
   grep -q "root" passwd 
   grep -q "qwertyuip;qwertyuo" passwd 
   echo $?
   grep -q "root" passwd 
   echo $?
   grep -nA 2 "root" passwd 
   grep -nB 2 "root" passwd 
   grep -nC 2 "root" passwd 
   grep -e "root" -e "mail" passwd 
   grep -r root /etc/ 
  ```

- 正则表达式

  REGEXP：Regular Expressions 由一类特殊字符以及文本字符编写的模式，其中有些字符（元字符）不表示字符本身的含义，而是表示控制和统配的含义

  程序支持：grep sed awk vi/vim less nginx 等

  分为两类：

  - 基本正则表达式
  - 扩展正则表达式grep -E

  man 7 regex查看元字符

基本正则表达式元字符

- 字符匹配

  . 匹配任意一个字符

  [] 匹配指定范围内的人一个字符[alex] [0-9] [a-zA-Z]

  ! 取反

  [:alunm:] 字母和数字

  [:alpha:] 大小写字母

  [:lower:] 小写字母

  [:upper:] 大写字母

  [:blank:] 空白字符

  [:digit:] 数字

  [:xdigit:] 十六进制数字

  [:punct:] 标点符号

- 匹配次数

  用在要指定次数的字符后面，用于指定前面额字符要出现的次数

  - *匹配前面的字符任意次数，包括0此，贪婪模式：尽可能多的匹配
  - .*任意长度的任意字符
  - \?匹配其墙面出现的字符0或1此
  - \\+匹配前面出现的字符至少一次
  - \\\{n\\}匹配前面的字符n次
  - \\{m,n\\}匹配前面的字符至少m次，最多n次
  - \\{,n\\}匹配前面的字符最多n次
  - \\{n\\}匹配前面的字符最少n次

- 位置锚定

  ^行首锚定

  $行尾锚定

  ^$空行

- 分组

  \\(\\)  将一个或者多个字符捆绑在一起，当做一个整体来处理

  (C|c)cat:Cat或cat

- 向后引用

  对分组的字符串基于位置引用

  \1:后引用，表示引用前面的第一个左括号与之对应的右括号中的模式所匹配到的内容

  \2:表示引用前面的第二个左括号与之对应的右括号中的模式所匹配到的内容

  ```shell
  He like his lover
  
  She love her liker
  
  He loves his lover
  
  She like his liker
  
      要求：前面显示like的后面显示liker，前面显示love的后面显示lover
  [root@localhost ~]# grep "\(l..e\).*\1r" i.txt
  He loves his lover
  She like his liker
  ```


egrep及扩展的正则表达式

egrep = grep -E

支持扩展正则表达式，与标准正则表达式的却别就是不需要转义

压缩

- gzip

  Usage:gzip [option] ...[FILE]...

  ```shell
  gzip passwd 压缩文件 默认会删除文件
  gunzip pass.gz 解压文件，默认也会删除文件
  gzip -d passwd.gz 解压文件
  -c 保存原来的文件
  gzip -c passwd > passwd.gz 压缩
  gzip -c -d passwd.gz > passwd  解压
  -num 1-9，指定压缩比，值越大压缩比例越大 默认是9
  zcat 查看压缩包内的文件
  zcat passwd.gz > passwd
  ```

- bzip2/bunzip2/bzact

  bzip [option]..file..

  bunzip2 解压

  -k 保留原文件

  -d 解压缩

  -num 1-9解压缩比，默认是9

- xz

  ```shell
  -k 保留源文件
  -d 解压
  unxz 解压
  -num 1-9 默认9
  xzcat 查看压缩包内的文件
  ```

- tar

  ```shell
  tar cvf a.tar b c
  c 创建
  v 显示过程
  f 指定文件
  r 追加
  x 解压
  -c 指定解压位置
  j 使用bzip2来压缩
  z 使用gzip来压缩
  J 使用xz来压缩
  --exclude 排除
  tar cvf a.tar b c
  tar -r -f a.tar d
  tar xf a.tar -C /opt
  tar jcvf a.tar.bz b c d
  tar zcvf a.tar.gz b c d 
  tar Jcvf a.tar.xz b c d 
  
  tar zcf etc.tar.gz --exclude=/etc/yum.repos.d --exclude=yum.conf /etc/
  ```

- 分卷压缩

  ```shell
  split -b size file -d tarfile
  -b 指定每一个分卷的大小
  -d 指定数字 默认是字母
  -a 指定后缀个数
  合并：
  cat tarfile* > file.tar.gz
  dd if=/dev/zero of=b bs=10M count=2
  split -b 5M b b.tar.gz
  split -b 5M b -d b.tar.gz
  split -b 5M b -d -a 3 b.tar.gz
  ```

  

### 11.Linux常用命令5

- 用户和组管理命令

  用户管理命令

  - useradd
  - usermod
  - userdel

  组账户维护命令

  - groupadd
  - groupmod
  - groupdel

- 用户

  管理员  root  0

  普通用户：1-65535 自动分配

  - 系统用户 1-499,1-999（centos7）一般是用来启动服务或者申请权限分配的
  - 登录用户500+，1000+（centos7）交互式登录

- 组

  管理员组  root  0

  普通组：

  - 系统组：1-499,1-999（centos7）
  - 普通组：500+，1000+（centos7）

  用户的主要组

  - 一个用户可以属于0个或多个附加组

- 用户和组的配置文件

  /etc/passwd 用户及其属性信息

  /etc/group 组及其属性信息

  /etc/shadow 用户密码及其相关属性

  /etc/gshadow  组密码及其相关属性

- passwd文件格式

  login name 登录用户名

  passwd 密码占位符（x）

  UID 用户身份编号

  GECOS 用户全名或注释

  home directory 用户家目录

  shell 用户登录后默认使用的shell

- shadow文件格式

  登录用户

  用户密码：一般用sha512加密

  从1970年1月1日起到木马最近一次被更改的时间

  密码再过几天可以被变更（0表示随时可以被变更）

  密码再过几天必须别变更（99999表示永不过期）

  密码过期前几天系统系统用户（默认为一周）

  密码过期几天后账户被锁定
  从1970年1月1日算起，多少天后账户失效

- 密码的复杂性策略

  包括数字、大小写字母以及特殊字符
  12位以上
  使用随机密码
  定期更换（3个月）

- group文件格式

  群组名称
  群组密码：通常不需要设定
  GID：群组的id
  以当前组为附加组的用户列表

- gshadow文件格式

  群组名称
  群组密码
  组管理员密码：组管理员的列表，更改组密码和成员
  以当前组为附加组的用户列表

- 用户创建：useradd

  ```shell
  useradd [options] LOGIN
  -u UID
  -o 配合-u选项，允许使用重复的UID创建用户账号
  -g GID：知名用户所属基本组，可以为组名，也可以为GID
  -c "comment" 用户的注释信息
  -d HOME_DIR 以指定的路径（不存在）为家目录
  -s SHELL 指定用户的默认shell程序，可用列表在/etc/shells文件中
  -G GROUP1[,GROUP2] 为用户指明附加组，组需要事先存在
  -N 不创建私有组做主组，使用users组做主组
  -r 创建系统用户 centos6 id<500,centos7 id<1000
  ```

  默认值设定：/etc/default/useradd文件中

  显示或更改默认设置

  - useradd -D 显示
  - useradd -D -s shell 修改登录后shell
  - useradd -D -b BASE_DIR 修改用户的默认家目录
  - useradd -D -g GROUP 修改用户默认的组，为-N选项设定

  相关文件

  - /etc/default/useradd 默认配置文件
  - /etc/skel/* 默认复制文件
  - /etc/login.defs 默认用户与组设置文件

- 用户属性修改

  ```shell
  usermod [options] login
  -u uid 新的uid
  -g gid 新主组
  -G GROUP1[,GROUP2,....[GROUPN]] 新附加组，原来的会被覆盖，若想保留原来的，则需要使用-a选项
  -s shell 新的登录后shell
  -c 'comment' 新的注释信息
  -d home 新的家目录不会自动创建，若想要创建新家目录并移动原家目录数据，使用-m选项
  -l loginname 新的登录名称
  -L 锁定用户
  -U 解锁用户
  -e yyyy-mm-dd 指明用户账户过期日期
  -i inactivedays 密码过期后经过多少天改账户会被禁用
  ```

- 删除用户

  ```shell
  userdel [option] login
  -r 删除用户家目录
  ```

- 查看用户的id信息

  ```shell
  id [option] ...[user]
  -u 显示uid
  -g 显示gid
  -G 显示用户所属组的ID
  -n 显示名称，需要配置ugG使用
  ```

- 切换用户或其他用户身份执行命令

  ```shell
  su [options...][-][user[args...]]
  ```

  切换用户的方式

  - su UserName 非登录式切换，即不会读取目标用户的配置文件，不改变当前工作目录
  - su - UserName 登录式切换，会读取目标用户的配置文件，切换至家目录，完全切换
  - root su至其他用户无需密码；非root用户时需要密码

  换个身份执行命令

  ```shell
  su [-] UserName -c 'command'
  ```

- 设置密码

  ```shell
  passwd [options] UserName 修改指定用户的密码
  -d 删除指定用户的密码
  -l 锁定指定用户
  -u 解锁指定用户
  -e 强制用户下次登录修改密码
  -f 强制操作
  -n mindays 指定最短使用期限
  -x maxdays 指定最大使用期限
  -w wandays 提前多少天开始警告
  -i inactivedays 密码过期后经过多少天后账户会被禁用
  --stdin 从标准输入读取密码 echo ‘password’|passwd --stdin username
  ```

- 修改用户密码策略

  ```shell
  chage [option] ... login
  -d 将密码修改时间改为指定的时间
  -E 指定用户的过期时间
  -I 密码过期后经过多少天后账户会被禁用
  -l 显示用户的账号信息
  -m 两次修改密码之间的最少天数
  -M 两次修改密码的最大天数
  -W 设置过期警告天数
  直接chage login 可以根据提示来设置
  ```

- 修改个人信息

  chfn修改个人信息

- 创建组

  ```shell
  groupadd [option] ... groupname
  -g gid 指定gid号
  -r 创建系统组
  ```

- 修改和删除组

  ```shell
  groupmod [option] ... groupname
  -n group_name 新名字
  -g GID 新的GID
  groupdel groupname
  ```

  

软件包管理

- 程序包管理器

  debian：deb文件 dpkg包管理器

  redhat：rpm文件，rpm包管理器

  rpm： Redhat Package Manager RPM Package Manager

  windows exe文件

- 包命名

  源码：name-version.tar.gz|bz2|xz

  rpm包 命名方式：name-version-release.arch.rpm

  - version 版本 major.minor.release
  - release：relases.os
  - arch
    - x86：i386 i486 i586 i686
    - x86_64:x64,x86_64,amd64
    - powerpc:ppc
    - 跟平台无关：noarch

- 包管理器

  功能：将编译好的应用程序的各组成部分打包一个或者几个程序包文件，从而方便快捷的实现程序包的安装、卸载、查询、升级和校验等管理工作

- 程序包的来源

  管理程序包的方式：rpm或者yum
  获取软件包的途径：

  - 系统发版的光盘或者官方的服务器
    https://www.centos.org/download/
    http://mirrors.aliyun.com
    http://mirrors.sohu.com
    http://mirrors.163.com
    项目官方站点
    第三方组织
    epel
    http://pkgs.org
    http://rpmfind.net
    https://sourceforge.net/
    自己制作
    第三方包建议要检查其合法性，来源合法性，程序包的完整性

- rpm包安装

  ```shell
  rpm -i [install-options] pkg_name
  -v 详细信息
  -vv 详细信息
  -h 以#显示程序包管理执行进度
  install-options
     --test 测试安装，但是不真正执行安装
     --nodeps 忽略依赖关系
     --nosignature 不检查来源合法性
     --nodigest 不检查包完整性
  rpm -ivh pkg_name
  ```

- 软件包升级

  ```shell
  rpm -U [install-options] pkg_name
  rpm -F [install-options] pkg_name
  upgrade 安装有旧版本包，则‘升级’，如果不存在旧版本包，则‘安装’
  freshen 安装有旧版本包，则‘升级’，如果不存在旧版本包，则不执行升级操作
  --oldpackage 降级
  --force 强制安装
  ```

- 升级注意项

  不要对内核进行升级操作，linux支持多内核版本并存，直接安装新版本内核
  如果原程序包的配置文件安装后被修改 ，升级时，新版本提供的配置文件不会直接覆盖老版本的配置文件，而是把心版本的文件重命名（filename.rpmnew）后保留

- 包查询

  ```shell
  rpm -q [select-options] [query-options]
  [select-options]
  -a 所有包
  -f 查看指定的文件由那个程序包安装生成
  rpm -q -f /etc/ssh/sshd_config
  [query-options]
  --changelog 查询rpm包的changelog rpm -q --changelog openssh-server
  -c 查询程序的配置文件
  -d 查询程序的文档
  -i 程序说明信息
  -l 查看指定的程序包安装后生成的所有文件
  -R 查询指定的程序包所依赖的库文件
  #常用用法
  -qi
  -qc
  -ql
  -qd
  -qa
  ```

- 包卸载

  ```shell
  rpm -e
  ```

- yum

  yum rpm的前端程序，可解决软件包相关依赖性，可以在多个库之间定位软件

  yum repo 存储了多个rpm包，以及包的相关元数据（放在特定目录下的repodata下）

  文件服务器：

  - http://
  - https://
  - ftp://
  - file://

- yum配置文件

  yum客户端配置文件

  ```shell
  /etc/yum.conf 为所有仓库提供公共配置
  /etc/yum.repos.d/*.repo 为仓库的指向提供配置
  [repositoryID] # 名字
  name=Some name for this repository # 描述信息
  baseurl=url://path/to/repository/ #地址
  enabled={1|0} #是否启用
  gpgcheck={1|0} #是否校验key文件
  gpgkey=URL #key文件地址
  failovermethod={roundrobin|priority} # 访问规则
  roundrobin:意为随机挑选，默认值
  priority:按顺序访问 
  cost= 默认为1000 权重
  其中$releasever: 当前OS的发行版的主版本号
  $arch: 平台，i386,i486,i586,x86_64等
  $basearch:基础平台;i386, x86_64
  ```

- yum源

  阿里云、清华、163、sohu、、、

- yum命令

  - 命令语法 yum [options] [command] [pkg]
  - 显示仓库列表 yum repolist [all|enabled|disabled]
  - 显示程序包 yum list [installed|updates]
  - 安装程序包 yum [install|reinstall] pkg1 pkg2
  - 升级程序包 yum update pkg1 pkg2
  - 降级程序包 yum downgrade pkg1 pkg2
  - 检查可用升级 yum check-update
  - 卸载程序包 yum remove pkg1 pkg2
  - 查看程序包 yum info
  - 清理本地缓存：/var/cache/yum/$basearch/$releasever缓存
    - yum clean [packages|all]
  - 重新构建缓存 yum makecache
  - 搜索 yum search string1 以指定的关键字搜索程序包名以及summary信息
  - 查看指定包所依赖的包 yum deplist pkg1
  - 日志 /var/log/yum.log
  - 安装本地包 yum install rpmfile1 rpmfile2

- 包组管理的相关命令

  yum groupinstall group1

  yum groupupdate group1

  yum grouplist group1
  yum groupremove group
  yum groupinfo group1

- yum命令行选项

  nogpgcheck 禁止进行gpg check
  -y 自动回答为 “yes”
  -q 静默模式

- 系统光盘yum 仓库

  挂在光盘值某目录，mount /dev/cdrom /mnt/cdrom
  创建配置文件指定baseurl为file://mnt/cdrom

- 程序包编译

  下载

  - wget

  解压

  - tar

  编译

  - ./configure

  安装

  - make 构件应用程序
  - make install赋值文件到相应目录

  安装前可以查看INSTALL,README文件

  

磁盘相关

- mount挂载

  ```shell
  mount DEVICE MOUNT_POINT
  -r 只读挂载
  -w  读写挂载
  -o 挂载文件系统的选项 
       loop
  mount 显示挂载信息
  ```

- 查看磁盘空间占用的工具

  df [option]...[file]

  ```shell
  -H 显示人类易读的方式
  ```

- 查看某目录总体空间占用状态

  du [option]...dir

  ```shell
  -h 显示人类易读方式
  -s 查看目录
  ```

  dd  复制文件生成文件

  ```shell
  dd if=/PATH/FROM/SRC of=/PATH/TO/DEST 
  bs=#:block size, 复制单元大小
  count=#:复制多少个bs
  of=file 写到所命名的文件而不是标准输出
  if=file 从所命名文件读取而不是标准输入
  ```

  RAID

  - RAID-0

    - 读写性能提升

    - 可用空间N*大小

    - 无容错能力

    - 最小磁盘书2

      ![raid-0](http://linux.imock.club/images/command/raid-0.png)

  - RAID-1

    - 读性能提升、写性能略下降

    - 可用空间 大小/个数

    - 用冗余能力

    - 最小磁盘数 2 2N

      ![raid-1](http://linux.imock.club/images/command/raid-1.png)

  - RAID-5

    - 读、写性能提升

    - 可用空间（n-1）*大小

    - 有容错能力 允许最多1块磁盘损害

    - 最少磁盘数 3，3+

      ![raid-5](http://linux.imock.club/images/command/raid-5.png)

  

- RAID-6
  - 读、写性能提升

  - 可用空间（N-2）*大小

  - 有容错能力：允许坏2块硬盘

  - 最少磁盘数: 4

    ![raid-6](http://linux.imock.club/images/command/raid-6.png)

  RAID-10

  - 读、写性能提升
  - 可用空间：N*大小/2
  - 有容错能力：没组镜像最多只能坏一块
  - 最少磁盘数：4

![raid-10](http://linux.imock.club/images/command/raid-10.png)

RAID-01

- 多块磁盘先实现raid0 在实现raid1

![raid-01](http://linux.imock.club/images/command/raid-01.png)

网络配置

- 静态指定
  - ifconfig
  - route
  - netstat
  - ip
  - 配置文件
- 动态分配
  
- dhcp
  
- ip命令

  用来机器的ip地址和路由表信息

  ip [options] object {COMMAND|HELP}

  object = link|addr |route

- ip link

  ip link 配置网卡设备

  - set 网卡设备
    - up 激活指定的接口 ifup
    - down 禁用指定接口 ifdown
  - show
    - 网卡设备
    - up

- ip addr

```shell
ip addr {add|del} ifaddr dev string
ip addr show
ip addr add 172.16.1.100/16 dev eth0
ip addr del 172.16.1.100/16 dev eth0
ip addr add 172.16.100.100/16 dev eth0 label eth0:0 
ip addr del 172.16.100.100/16 dev eth0 label eth0:0
```

- 网卡配置文件

  配置文件：/etc/sysconfig/network-scripts/ifcfg-IFACE

  DEVICE 此设备文件应用到的设备

  HWADDR 对应的设备的mac地址

  BOOTPROTO 激活此设备时使用的地址配置协议，常用的dhcp static none

  ONBOOT 在系统引导是是否激活此设备

  TYPE 接口类型；常见的有Ethernet，Bridge

  UUID 设备的唯一标识

  IPADDR ip地址

  NETMASK 子网掩码

  GATEWAY 默认网关

  DNS1 第一个DNS服务器

  DNS2 第二个DNS服务器

- ss命令

  ss 命令用来打印Linux中网络系统的状态信息，可让你得知整个Linux系统的网络情况

  格式 ss [options]….[filter]

```shell
-t tcp协议相关
-u udp协议相关
-x unix socket相关
-l listen状态的连接
-a 所有
-p 相关的程序及pid
-n 显示端口
常用组合 -tan -tanl -anlp -unlp
```

主机名

- 配置当前主机的主机名
  - hostname
  - hostnamectl set-hostname 永久生效
- 显示当前的主机名
  - hostname
- 配置文件 /etc/hostname

本地解析

配置文件 /etc/hosts

- 本地主机名和ip地址的映射
- 对小型独立网络有效
- 通常在使用DNS前检查

dns解析

配置文件 /etc/reslov.conf

- nameserver dns1
- nameserver dns2

网络工具

- wget 下载文件

  ```shell
  -q 静默模式
  -c 断点续传
  -P 保存在指定的目录
  -O filename 保存为指定的文件名
  --limit-rate=  限制传输速率
  -r 递归下载
  -p 下载所有的html元素
  ```

### 12.Linux常用命令

- 计划任务

  cron

  ```shell
  SHELL=/bin/bash
  PATH=/sbin:/bin:/usr/sbin:/usr/bin
  MAILTO=root
  
  # For details see man 4 crontabs
  
  # Example of job definition:
  # .---------------- minute (0 - 59)
  # |  .------------- hour (0 - 23)
  # |  |  .---------- day of month (1 - 31)
  # |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
  # |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
  # |  |  |  |  |
  # *  *  *  *  * user-name  command to be executed
  ```

  时间表示法：

  - 特定值
  - *代表所有
  - 离散取值#，#，#
  - 连续取值#-#
  - 步长/#

  系统的计划人事

  - /etc/crontab 配置文件
  - /etc/cron.d/ 配置文件
  - /etc/cron.hourly/ 脚本
  - /etc/cron.daily/ 脚本
  - /etc/cron.weekly/ 脚本
  - /etc/cron.monthly/ 脚本

  crontab命令

  - 定义

    每个用户都有专用的cron任务文件：/var/spool/cron/USERNAME

  - 命令格式

    -l 列出所有任务

    -e 编辑任务

    -r 删除任务

    -u user指定用户