1 查找所需内容

- 使用requests模块 下载内容 保存到Cover Music

- MongoDB创建Content数据表用于保存Content数据

  ```puython
  {
  cover:"cover.jpg",
  music:"music.mp3"
  }
  ```

2 对接前端App的接口

- /content_list获取内容所需列表

  ```python
  生成器 使用list转成列表 （查询所有Content数据库中的数据）
  返回之间 id = ObjectId
  循环的方式将ObjectId转换成字符串
  ```

- /get_cover /get_music 获取内容详情

  ```python
  /get_cover/cover.jpg 动态参数路由
  返回内容流
  send_file（file_path）
  ```

- /reg

  ```python
  注册用户
  前端提交user_info 不足以满足数据库中的字段
  user_info[头像] = 将制作完成的user_info存储在数据库中的users表中
  ```

- /login

  ```python
  用户登录
  不能将密码传输过去  -  MongoDB.users.find_one({},{"password":0})
  ```

- /auto_login

  ```python
  每次开启app前端启动自动向后端发起信息请求查看用户的信息
  后端查到"_id"的用户信息后将除用户密码外的信息发给前端，前端校验后显示用户信息
  ```

- /scan_qr

  ```python
  前端发起post请求将二维码的信息传给后端
  后端对数据进行校验 查看是什么功能 是绑定玩具还算加好友
  之后将携带的信息返给前端
  ```

  

