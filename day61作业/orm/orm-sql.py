import os
# https://www.cnblogs.com/maple-shaw/articles/9414626.html
os.environ.setdefault('DJANGO_SETTINGS_MODULE','day61作业.settings')
import django

django.setup()
from app01 import models
from django.db.models import Max,Min,Avg,Sum,Count

'''#查找所有书名里包含金老板的书
ret = models.Book.objects.filter(title__contains='金老板')
print(ret)
#select * from app01_book where title like '%金老板%';
'''
'''
# 查找出版日期是2018年的书
ret = models.Book.objects.filter(publish_date__year='2018')
print(ret)
#select * from app01_book where publish_date like '2018%';
'''
'''
# 查找出版日期是2017年的书名
ret = models.Book.objects.filter(publish_date__year='2017')
for i in ret:
    print(i.title)
#select title from app01_book where publish_date like '2017%';
'''
'''
# 查找价格大于10元的书
ret = models.Book.objects.filter(price__gt=10)
print(ret)
#select * from app01_book where price>10;
'''
'''
# 查找价格大于10元的书名和价格
ret = models.Book.objects.filter(price__gte=10)
print(ret)
#select * from app01_book where price>=10;
'''
'''
# 查找memo字段是空的书
ret = models.Book.objects.filter(memo__isnull=True)
print(ret)
#select * from app01_book where memo is null;
'''
'''
# 查找在北京的出版社
ret = models.Publisher.objects.filter(city='北京')
print(ret)
#select * from app01_publisher where city='北京';
'''
'''
# 查找名字以沙河开头的出版社
ret = models.Publisher.objects.filter(name__startswith='沙河')
print(ret)
##select * from app01_publisher where name like '沙河%';
'''
'''
# 查找“沙河出版社”出版的所有书籍
ret = models.Book.objects.filter(publisher__name='沙河出版社')
print(ret)
select title,name from app01_book inner join app01_publisher on publisher_id = app01_publisher.id
where app01_publisher.name='沙河出版社';
'''
'''
# 查找每个出版社出版价格最高的书籍价格
ret = models.Publisher.objects.annotate(Min('book__price')).values()
for ret in ret:
    print(ret)
select title,name,max(price),city from app01_book inner join app01_publisher on
 publisher_id = app01_publisher.id group by publisher_id;
'''
'''
# 查找每个出版社的名字以及出的书籍数量
ret = models.Publisher.objects.annotate(count=Count('book'))
for ret in ret:
    print(ret.name,ret.count)
select name,count(title),city from app01_publisher inner join app01_book on
 publisher_id = app01_publisher.id group by publisher_id;
'''
'''
# 查找作者名字里面带“小”字的作者
ret = models.Author.objects.filter(name__contains='小')
print(ret)
select * from app01_author where name like '%小%';
'''
'''
# 查找年龄大于30岁的作者
ret = models.Author.objects.filter(age__gt=30)
print(ret)
select * from app01_author where age>30;
'''
'''
# 查找手机号是155开头的作者
ret = models.Author.objects.filter(phone__startswith='155').values_list('name')
print(ret)
select * from app01_author where phone like '155%';
'''
'''
# 查找手机号是155开头的作者的姓名和年龄
ret = models.Author.objects.filter(phone__startswith='155').values_list('name','age')
print(ret)
select name,age from app01_author where phone like '155%';
'''
'''
# 查找每个作者写的价格最高的书籍价格
ret = models.Author.objects.annotate(max=Max('book__price')).values()
print(ret)
select name,max(price) from app01_book inner join app01_author on
 app01_author.id = app01_book.id group by app01_author.name;
'''
'''
# 查找每个作者的姓名以及出的书籍数量
ret = models.Author.objects.annotate(count=Count('book')).values('name','count')
print(ret)
select name,count(book_id) from app01_author inner join app01_book_author on
 author_id = app01_author.id group by app01_author.name;
'''
'''
# 查找书名是“跟金老板学开车”的书的出版社
ret = models.Publisher.objects.filter(book__title='跟金老板学开车')
print(ret)
select * from app01_book inner join app01_publisher on
 publisher_id = app01_publisher.id where title = '跟金老板学开车';
'''
'''
# 查找书名是“跟金老板学开车”的书的出版社所在的城市
ret = models.Publisher.objects.filter(book__title='跟金老板学开车').values('city')
print(ret)
select city from app01_book inner join app01_publisher on
 publisher_id = app01_publisher.id where title = '跟金老板学开车';
'''
'''
# 查找书名是“跟金老板学开车”的书的出版社的名称
ret = models.Publisher.objects.filter(book__title='跟金老板学开车').values('name')
print(ret)
select name from app01_book inner join app01_publisher on
 publisher_id = app01_publisher.id where title = '跟金老板学开车';
'''

'''
# 查找书名是“跟金老板学开车”的书的出版社出版的其他书籍的名字和价格
ret = models.Book.objects.filter(publisher__name__in=models.Publisher.objects.filter(book__title='跟金老板学开车').values('name')).values_list('title','price')
print(ret)
select title,price from app01_book inner join (select publisher_id from app01_book inner join app01_publisher on
 publisher_id = app01_publisher.id where title = '跟金老板学开车') as f on f.publisher_id = app01_book.publisher_id;
'''
'''
# 查找书名是“跟金老板学开车”的书的所有作者
ret = models.Author.objects.filter(book__title='跟金老板学开车').values_list('name')
print(ret)

'''
'''
# 查找书名是“跟金老板学开车”的书的作者的年龄
ret = models.Author.objects.filter(book__title='跟金老板学开车').values_list('name','age')
print(ret)
'''
'''
# 查找书名是“跟金老板学开车”的书的作者的手机号码
ret = models.Author.objects.filter(book__title='跟金老板学开车').values_list('name','age','phone')
print(ret)
'''
'''
# 查找书名是“跟金老板学开车”的书的作者们的姓名以及出版的所有书籍名称和价钱
ret = models.Book.objects.filter(author__name__in=models.Author.objects.filter(book__title='跟金老板学开车').values('name')).values_list('author__name','title','price')
print(ret)
'''