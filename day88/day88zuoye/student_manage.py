from flask import Flask, render_template, request, session,redirect
STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}
app = Flask(__name__)
app.secret_key = 'hehe'

def war(func):
    def inner(*args,**kwargs):
        if session.get('user'):
            ret = func(*args,**kwargs)
            return ret
        else:
            return redirect('/login')
    return inner
@app.route('/stu_list',methods=['POST','GET'],endpoint='stu_list')
@war
def stu_list():
    if request.method == 'GET':
        if request.args.to_dict():
            id = request.args.to_dict().get('id')
            id = int(id)
            print(id)
            dic = STUDENT_DICT.get(id)
            print(dic)
            return render_template('stu.html',dic=dic,id=id)
        return render_template('stu_list.html',stu = STUDENT_DICT)
@app.route('/',endpoint='index')
@war
def index():
    return 'index'

@app.route('/login',methods=['POST','GET'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        name = request.form.get('name')
        pwd = request.form.get('pwd')
        print(name,pwd)
        session['user'] = name
        print(session.get('user'))
        return 'login ok'

if __name__ == '__main__':
    app.run()