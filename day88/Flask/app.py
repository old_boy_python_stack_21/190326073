import os

from flask import Flask, redirect, make_response, render_template, send_file, jsonify, request, session
from markupsafe import Markup

STUDENT = {'name': 'Old', 'age': 38, 'gender': '中'},

STUDENT_LIST = [
    {'name': 'Old', 'age': 38, 'gender': '中'},
    {'name': 'Boy', 'age': 73, 'gender': '男'},
    {'name': 'EDU', 'age': 84, 'gender': '女'}
]

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}


app = Flask(__name__)

app.secret_key = 'sadasdasd'

@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        print(request.args.to_dict(),type(request.args.to_dict()))
        print(request.cookies)
        return render_template('login.html')
    else:
        name =request.form.get('username')
        session['username'] =name
        print(session.get('username'))

        my_file = request.files.get('my_file')
        if my_file:
            filename = my_file.filename
            file_path = os.path.join('avatar',filename)
            print(filename)
            my_file.save(file_path)
        return 'login ok'

@app.route('/get_file')
def get_file():
    return send_file('1.mp3')
@app.route('/get_json')
def get_json():
    dic = {'name':'hehe'}
    return dic
    # return jsonify('hello world')
@app.route('/stu_list')
def student_list():
    inp = Markup('<input type="submit" value="提交">')
    return render_template('stu_list.html', stu=STUDENT,inp=inp)

@app.template_global()
def ab(a,b):
    return a+b

app.run()