STUDENT = {'name': 'Old', 'age': 38, 'gender': '中'},

STUDENT_LIST = [
    {'name': 'Old', 'age': 38, 'gender': '中'},
    {'name': 'Boy', 'age': 73, 'gender': '男'},
    {'name': 'EDU', 'age': 84, 'gender': '女'}
]

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}
from flask import Flask,render_template

app = Flask(__name__)
# app.debug = True

@app.route('/stu_list')
def student_list():
    return render_template('stu_list.html', stu=STUDENT)

if __name__ == '__main__':
    app.run('0.0.0.0',9527)