## python网络爬虫网络爬虫

### 一、相关基础概念

什么是爬虫

- 爬虫就是通过编写程序模拟浏览器上网，然后让其去互联网上抓取数据的过程。

哪些语言可以实现爬虫

- php：可以实现爬虫，php被号称是全世界最优美的语言，但是在实现爬虫中支持多线程和多进程方面做的不好。
- Java：可以实现爬虫，Java可以非常好的处理和实现爬虫，是唯一可以与python并驾齐驱且是python的头号劲敌，但是Java实现爬虫代码较为臃肿，重构成本较大。
- c、c++：可以实现爬虫，但是使用这种方式实现爬虫纯粹是某些人能力的体现，却不是明智和合理的选择。
- python：可以实现爬虫，python实现和处理爬虫语法简单，代码优美，支持的模块繁多，学习成本低，具有非常强大的框架（scrapy等）。

爬虫的分类：

- 通用爬虫：通用爬虫是搜索引擎“抓取系统”的重要组成部分。主要的目的是将互联网上的页面下载到本地，形成一个互联网内容的镜像备份。

  ​	搜索引擎如何抓取互联网上的网站数据？

  - 门户网站主动向搜索引擎公司提供其网站的URL

  - 搜索引擎公司与DNS服务商合作，获取网站的URL

  - 门户网站主动挂靠在一些知名网站的友情链接中

- 聚焦爬虫：聚焦爬虫是根据指定的需求抓取网络上指定的数据。例如：获取豆瓣上电影的名称和影评，而不是获取整张页面中所有的数据值。

robots.txt协议：网站可以通过编写一个robots.txt的协议文件来约束爬虫程序的数据爬取。该协议只是相当于口头的协议。

反爬虫

- 门户网站通过响应的策略和技术手段，防止爬虫程序进行网站数据的抓取

反反爬虫

- 爬虫程序通过相应的策略和技术手段，破解门户网站的反爬虫手段，从而爬取到相因的数据

### 二、http和https

HTTP协议

- 官方概念：http协议是hyper text transfer protocol（超文本传输协议）的缩写，是用于从万维网服务器传输超文本到本地浏览器传输的协议。
- HTTP协议就是服务器（server）和客户端（client）之间进行数据交互（相互传输数据）的一种形式（双方相互遵循）。

HTTP工作原理

- HTTP协议工作于客户端-服务端架构以上，浏览器作为http客户端通过URL想http服务端即web服务器发送所有请求。web服务器根据接收到的请求后，向客户端发送响应信息。

HTTP协议的优点

- 效率高：限制每个连接只有一个请求的无连接状态，在服务器处理完客户的请求，并收到客户的反应，即断开，通过这种方式可以节省传输时间。
- 简单快速：当服务器客户端请求服务时，只需传送请求方法和路径。请求方法常用的GET，HEAD，POST。每种方法规定了客户端与服务器联系的是不同的类型。因为简单的 HTTP 协议，通信速度很快。
- 灵活：HTTP 允许任何类型的数据对象的传输，输入被传输的内容类型进行标记。
- 无状态：HTTP 协议是无状态的协议，没有一个国家是没有协议的事务处理和存储能力。如果该状态是指由于缺乏必要前述信息的后续处理中，它必须被重传，这可能导致在数据传输增加了每个连接。另一方面，当不需要在服务器上的快速响应的先验信息。

HTTP协议的缺点：

- 有被窃听的风险，Http通信使用明文,传输过程中没有任何的保证措施，可能会被窃听
- 在传输过过程中，不验证通信方的身份，这中间就有可能被遭遇伪装
- Http只是对报文进行了解析，并没有对其进行完整的校验，所以无法验证报文的完整形，可能被遭篡改

HTTP四点注意事项

- HTTP允许传输任意类型的数据对象。正在传输的类型由content-type加以标记
- HTTP是无连接：无连接的含义是限制每次连接只能处理一个请求，服务器处理完客户的请求，并受到客户的应答后，即断开连接。采用这种方式可以节省传输时间
- HTTP是媒体独立的：这意味着，只要客户端和服务器知道如何处理的数据内容，任何类型的数据都可以通过http发送，客户端及服务器指定使用适合的MIME-type内容类型
- HTTP无状态：HTTP协议是无状态协议，无状态是指协议对于事物处理没有记忆能力，缺少状态意味着如果后续处理需要前面的信息，则他必须重传，这样可能导致每次连接传送的数据量增加。另一方面，在服务器不需要先前信息时它的应答就较快。

HTTP之URL：

- HTTP使用统一资源标识符（*Uniform Resource Identifiers, URI*）来传输数据和建立连接。URL是一种特殊类型的URL，包含了用于查找某个资源的足够信息

  ```python
  URL,全称是UniformResourceLocator, 中文叫统一资源定位符,是互联网上用来标识某一处资源的地址。以下面这个URL为例，介绍下普通URL的各部分组成：http://www.aspxfans.com:8080/news/index.asp?boardID=5&ID=24618&page=1#name从上面的URL可以看出，一个完整的URL包括以下几部分：
  
  　　　　- 协议部分：该URL的协议部分为“http：”，这代表网页使用的是HTTP协议。在Internet中可以使用多种协议，如HTTP，FTP等等本例中使用的是HTTP协议。在"HTTP"后面的“//”为分隔符
  
  　　　　- 域名部分：该URL的域名部分为“www.aspxfans.com”。一个URL中，也可以使用IP地址作为域名使用
  
  　　　　- 端口部分：跟在域名后面的是端口，域名和端口之间使用“:”作为分隔符。端口不是一个URL必须的部分，如果省略端口部分，将采用默认端口
  
  　　　　- 虚拟目录部分：从域名后的第一个“/”开始到最后一个“/”为止，是虚拟目录部分。虚拟目录也不是一个URL必须的部分。本例中的虚拟目录是“/news/”
  
  　　　　- 文件名部分：从域名后的最后一个“/”开始到“？”为止，是文件名部分，如果没有“?”,则是从域名后的最后一个“/”开始到“#”为止，是文件部分，如果没有“？”和“#”，那么从域名后的最后一个“/”开始到结束，都是文件名部分。本例中的文件名是“index.asp”。文件名部分也不是一个URL必须的部分，如果省略该部分，则使用默认的文件名
  
  　　　　- 锚部分：从“#”开始到最后，都是锚部分。本例中的锚部分是“name”。锚部分也不是一个URL必须的部分
  
  　　　　- 参数部分：从“？”开始到“#”为止之间的部分为参数部分，又称搜索部分、查询部分。本例中的参数部分为“boardID=5&ID=24618&page=1”。参数可以允许有多个参数，参数与参数之间用“&”作为分隔符。
  ```

HTTP值request：

https://img2018.cnblogs.com/blog/1489694/201809/1489694-20180914105821455-775348403.png

- 客户端发送一个HTTP请求到服务器的请求消息包括一下组成部分

  ```python
  报文头：常被叫做请求头，请求头中存储的是该请求的一些主要说明（自我介绍）。服务器据此获取客户端的信息。
  常见的请求头：
  accept:浏览器通过这个头告诉服务器，它所支持的数据类型
  Accept-Charset: 浏览器通过这个头告诉服务器，它支持哪种字符集
  Accept-Encoding：浏览器通过这个头告诉服务器，支持的压缩格式
  Accept-Language：浏览器通过这个头告诉服务器，它的语言环境
  Host：浏览器通过这个头告诉服务器，想访问哪台主机
  If-Modified-Since: 浏览器通过这个头告诉服务器，缓存数据的时间
  Referer：浏览器通过这个头告诉服务器，客户机是哪个页面来的 防盗链
  Connection：浏览器通过这个头告诉服务器，请求完后是断开链接还是何持链接
  X-Requested-With: XMLHttpRequest 代表通过ajax方式进行访问
  User-Agent：请求载体的身份标识
  报文体：常被叫做请求体，请求体中存储的是将要传输/发送给服务器的数据信息。
  ```

HTTP值response

https://img2018.cnblogs.com/blog/1489694/201809/1489694-20180914112109546-492688431.png

- 服务器回传HTTP响应到客户端的响应消息包括一下组成部分

  ```python
  状态码：以“清晰明确”的语言告诉客户端本次请求的处理结果。
  HTTP的响应状态码由5段组成： 
  　　1xx 消息，一般是告诉客户端，请求已经收到了，正在处理，别急...
  　　2xx 处理成功，一般表示：请求收悉、我明白你要的、请求已受理、已经处理完成等信息.
  　　3xx 重定向到其它地方。它让客户端再发起一个请求以完成整个处理。
  　　4xx 处理发生错误，责任在客户端，如客户端的请求一个不存在的资源，客户端未被授权，禁止访问等。
  　　5xx 处理发生错误，责任在服务端，如服务端抛出异常，路由出错，HTTP版本不支持等。
  响应头：响应的详情展示
  常见的相应头信息：
  Location: 服务器通过这个头，来告诉浏览器跳到哪里
  Server：服务器通过这个头，告诉浏览器服务器的型号
  Content-Encoding：服务器通过这个头，告诉浏览器，数据的压缩格式
  Content-Length: 服务器通过这个头，告诉浏览器回送数据的长度
  Content-Language: 服务器通过这个头，告诉浏览器语言环境
  Content-Type：服务器通过这个头，告诉浏览器回送数据的类型
  Refresh：服务器通过这个头，告诉浏览器定时刷新
  Content-Disposition: 服务器通过这个头，告诉浏览器以下载方式打数据
  Transfer-Encoding：服务器通过这个头，告诉浏览器数据是以分块方式回送的
  Expires: -1 控制浏览器不要缓存
  Cache-Control: no-cache 
  Pragma: no-cache
  相应体：根据客户端指定的请求信息，发送给客户端的指定数据
  ```

HTTPS协议

- 官方概念：HTTPS (Secure Hypertext Transfer Protocol)安全超文本传输协议，HTTPS是在HTTP上建立SSL加密层，并对传输数据进行加密，是HTTP协议的安全版。

- 加密安全版的HTTP协议。

  https://img2018.cnblogs.com/blog/1489694/201809/1489694-20180914113539797-1056363389.png

HTTPS采用的加密技术

- SLL加密技术

  ```python
  SSL采用的加密技术叫做“共享密钥加密”，也叫作“对称密钥加密”，这种加密方法是这样的，比如客户端向服务器发送一条信息，首先客户端会采用已知的算法对信息进行加密，比如MD5或者Base64加密，接收端对加密的信息进行解密的时候需要用到密钥，中间会传递密钥，（加密和解密的密钥是同一个），密钥在传输中间是被加密的。这种方式看起来安全，但是仍有潜在的危险，一旦被窃听，或者信息被挟持，就有可能破解密钥，而破解其中的信息。因此“共享密钥加密”这种方式存在安全隐患：https://images2015.cnblogs.com/blog/1066538/201707/1066538-20170707004823440-1284340758.png
      https://images2015.cnblogs.com/blog/1066538/201707/1066538-20170707004834987-1723387229.png
  ```

- 非对称密钥加密技术

  ```python
  “非对称加密”使用的时候有两把锁，一把叫做“私有密钥”，一把是“公开密钥”，使用非对象加密的加密方式的时候，服务器首先告诉客户端按照自己给定的公开密钥进行加密处理，客户端按照公开密钥加密以后，服务器接受到信息再通过自己的私有密钥进行解密，这样做的好处就是解密的钥匙根本就不会进行传输，因此也就避免了被挟持的风险。就算公开密钥被窃听者拿到了，它也很难进行解密，因为解密过程是对离散对数求值，这可不是轻而易举就能做到的事。以下是非对称加密的原理图：
  ```

  https://images2015.cnblogs.com/blog/1066538/201707/1066538-20170707004854565-1261145539.png

  ```python
  但是非对称秘钥加密技术也存在如下缺点：
  第一个是：如何保证接收端向发送端发出公开秘钥的时候，发送端确保收到的是预先要发送的，而不会被挟持。只要是发送密钥，就有可能有被挟持的风险。
  第二个是：非对称加密的方式效率比较低，它处理起来更为复杂，通信过程中使用就有一定的效率问题而影响通信速度
  ```

- HTTPS的证书机制

  ```python
  公钥很可能存在被挟持的情况，无法保证客户端收到的公开密钥就是服务器发行的公开密钥。此时就引出了公开密钥证书机制。数字证书认证机构是客户端与服务器都可信赖的第三方机构。证书的具体传播过程如下：
  1：服务器的开发者携带公开密钥，向数字证书认证机构提出公开密钥的申请，数字证书认证机构在认清申请者的身份，审核通过以后，会对开发者申请的公开密钥做数字签名，然后分配这个已签名的公开密钥，并将密钥放在证书里面，绑定在一起
  2：服务器将这份数字证书发送给客户端，因为客户端也认可证书机构，客户端可以通过数字证书中的数字签名来验证公钥的真伪，来确保服务器传过来的公开密钥是真实的。一般情况下，证书的数字签名是很难被伪造的，这取决于认证机构的公信力。一旦确认信息无误之后，客户端就会通过公钥对报文进行加密发送，服务器接收到以后用自己的私钥进行解密。
  ```

为什么还有很多网站不使用Https?

```python
1：加密通信会消耗一定的cpu和服务器资源,如果每次通信都加密，就会消耗更多的资源
2:如果所有的信息都采用https加密，这无疑是一种浪费。非敏感信息就算被窃取了，也无伤大雅。可以在其传输敏感信息的时候，采用https协议进行加密
3：购买证书的开销也是一笔很大的费用。
```

三、Anaconda的使用

什么是Anaconda

- 集成环境：基于数据分析和机器学习的开发环境

jupyter：超级终端，Anaconda继承环境中提供的一种基于浏览器的可视化开发巩固

cell都是有两种常用的模式

- code：编写python程序
- Markdown：编写笔记

快捷键

- 插入cell：a,b
- 删除cell：x
- 切换cell的模式：y，m
- 执行：shift+enter
- tab
- shift+tab：打开帮助文档

### 三、requests模块基本使用

requests模块

- 概念：一个基于网络请求的模块，作用就是用来模拟浏览器发起请求
- 编码流程：
  - 指定URL
  - 进行请求的发送
  - 获取响应数据（爬取到的数据）
  - 持久化存储
    - 环境安装：pip install requests

获取页面源码数据

```python
#step_1
url = 'https://www.sogou.com'
#step_2:返回值是一个响应对象
response = requests.get(url=url)
#step_3:text返回的是字符串形式的响应数据
page_text = response.text
#step_4
with open('./sogou.html','w',encoding='utf-8') as fp:
    fp.write(page_text)
```

简单页面采集器

```python
wd = input('enter a key:')
url = 'https://www.sogou.com/web'
#存储的就是动态的请求参数
params = {
    'query':wd
}
#一定需要将params作用到请求中
#params参数表示的是对请求url参数的封装
response = requests.get(url=url,params=params)

page_text = response.text
fileName = wd+'.html'
with open(fileName,'w',encoding='utf-8') as fp:
    fp.write(page_text)
#上述程序出现了问题：
#1、爬取到的数据出现了乱码
#2、遇到了UA检测反扒机制


#解决中文乱码
wd = input('enter a key:')
url = 'https://www.sogou.com/web'
#存储的就是动态的请求参数
params = {
    'query':wd
}
#一定需要将params作用到请求中
#params参数表示的是对请求url参数的封装
response = requests.get(url=url,params=params)

#手动修改响应数据的编码
response.encoding = 'utf-8'

page_text = response.text
fileName = wd+'.html'
with open(fileName,'w',encoding='utf-8') as fp:
    fp.write(page_text)
    
#使用UA伪装解决UA检测反爬机制
#解决中文乱码&UA伪装
wd = input('enter a key:')
url = 'https://www.sogou.com/web'
#存储的就是动态的请求参数
params = {
    'query':wd
}

#即将发起请求对应的头信息
headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
}

#一定需要将params作用到请求中
#params参数表示的是对请求url参数的封装
#headers参数是用来实现UA伪装
response = requests.get(url=url,params=params,headers=headers)

#手动修改响应数据的编码
response.encoding = 'utf-8'

page_text = response.text
fileName = wd+'.html'
with open(fileName,'w',encoding='utf-8') as fp:
    fp.write(page_text)
```

爬取豆瓣

```python
headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
}
url = 'https://movie.douban.com/j/chart/top_list'
start = input('enter a start:')
limit = input('enter a limit')
#处理请求参数
params = {
    'type': '5',
    'interval_id': '100:90',
    'action': '',
    'start': start,
    'limit': limit,
}

response = requests.get(url=url,params=params,headers=headers)
#json返回的是序列化好的对象
data_list = response.json()

fp = open('douban.txt','w',encoding='utf-8')

for dic in data_list:
    name = dic['title']
    score = dic['score']
    
    fp.write(name+':'+score+'\n')
    print(name,'爬取成功')
 
fp.close()
```

爬取肯德基餐厅位置信息

```python
post_url = 'http://www.kfc.com.cn/kfccda/ashx/GetStoreList.ashx?op=keyword'
city = input('enter a city name:')
data = {
    'cname': '',
    'pid': '',
    'keyword': city,
    'pageIndex': '3',
    'pageSize': '2',
}
#data参数表示的就是get方法中的params
response = requests.post(url=post_url,data=data,headers=headers)

response.json()
```

爬取动态数据加载网站

- 分析

  ```python
  1.网站的首页和企业的详情页的数据都是动态加载出来的
  2.分析某一家企业的企业详情数据是怎么来的?
  企业详情数据时通过一个ajax请求(post)请求到的.
  请求对应的url:http://125.35.6.84:81/xk/itownet/portalAction.do?method=getXkzsById
  该请求携带了一个参数:id:xxdxxxx
  结论:
  1.每家企业详情页的数据都是通过一个post形式的ajax请求请求到的
  2.每家企业对应的ajax请求的url都一样,请求方式都是post,只有请求参数id的值不一样.
  3.只需要获取每一家企业对应的id值即可获取每一家企业对应的详情数据
  需要获取每一家企业的id值
  思路:每一家企业的id值应该存储在首页对应的相关请求或者响应中.
  结论:每一家企业的id值是存储在首页中的某一个ajax请求对应的响应数据中,只需要将该响应数据中企业的id提取/解析出来后即可.
  ```

- 代码

  ```python
  #要请求到每一家企业对应的id
  url = 'http://125.35.6.84:81/xk/itownet/portalAction.do?method=getXkzsList'
  data = {
      'on': 'true',
      'page': '1',
      'pageSize': '15',
      'productName': '',
      'conditionType': '1',
      'applyname': '',
      'applysn': '',
  }
  
  fp = open('./company_detail.txt','w',encoding='utf-8')
  
  #该json()的返回值中就有每一家企业的id
  data_dic = requests.post(url=url,data=data,headers=headers).json()
  #解析id
  for dic in data_dic['list']:
      _id = dic['ID']
  #     print(_id)
      #对每一个id对应的企业详情数据进行捕获(发起请求)
      post_url = 'http://125.35.6.84:81/xk/itownet/portalAction.do?method=getXkzsById'
      post_data = {
          'id':_id
      }
      #json的返回值是某一家企业的详情信息
      detail_dic = requests.post(url=post_url,data=post_data,headers=headers).json()
      company_title = detail_dic['epsName']
      address = detail_dic['epsProductAddress']
      
      fp.write(company_title+':'+address+'\n')
      print(company_title,'爬取成功!!!')
  fp.close()
  ```

  









