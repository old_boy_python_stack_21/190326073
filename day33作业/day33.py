# 1.进程间内存是否共享？如何实现通讯？
# 进程直接的内存是不共享的，通讯的话需要文件或者网络进程传输
# 2.请聊聊进程队列的特点和实现原理？
# 先进先出，当队列中数据已满时再放数据会阻塞，当使用q.get_nowait()时若队列为空会报错
# 3.请画出进程的三状态转换图
# 系统会创建一个进程开辟一个内存地址并分配一个pid，之后进入read就绪状态》》running
# 开始运行，过程中若遇到时间片轮转时间到则返回read状态，若遇到阻塞就会到blocking状态，
# 阻塞完后会返回到read状态。
# 4.从你的角度说说进程在计算机中扮演什么角色？
# 进程是计算机中最小的资源分配单位，是运行起来的程序，执行数据运算分析等任务的。
# 5.使用进程实现并发的socket的server端
import struct
import socket,random
from multiprocessing import Process
def server():
    sk = socket.socket()
    sk.bind(('127.0.0.1',6000))
    sk.listen()
    conn,_ = sk.accept()
    byte_len = conn.recv(4)
    size = struct.unpack('i',byte_len)[0]
    msg1 = conn.recv(size).decode('gbk')
    msg = conn.recv(1024)
    print(byte_len,msg1,msg)
    conn.close()
    sk.close()
if __name__ == '__main__':
    # v = []
    # for i in range(10):
    p = Process(target=server)
    p.start()
    # v.append(p)
    # for p in v:p.join()